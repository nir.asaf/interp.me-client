import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import { auth } from './firebase';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:3000';

// const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('authorization', `Token ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
  current: () =>
    requests.get('user'),
  login: (email, password) =>
    auth.doSignInWithEmailAndPassword(email, password).then((resp) => {
      return resp;
    }).catch((error) => {return {error: error} }),
  register: (email, password) =>
    auth.doCreateUserWithEmailAndPassword(email, password).then((resp) => {
        return resp;
    }).catch((error) => {return {error: error} }),
  save: user =>
    requests.put('/user', { user })
};

const Profile = {
  get: username =>
    requests.get(`/profiles/${username}`)
};

const TwilioToken = {
  getToken: () => {
    return;
  }
}

export default {
  Auth,
  Profile,
  TwilioToken,
  setToken: _token => { token = _token; }
};
