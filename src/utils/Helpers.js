export const IsEmptyObject = (obj) => {
  for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

export const findObjectByValue = (array, value) => {
  var findObject = {};
  array.map( object => {
    Object.values(object).map( itemValue => {
      if (itemValue === value) {
        findObject = object;
      }
      return true;
    })
    return true;
  })
  return findObject;
}

// Find a list of objects by there value in array
export const findObjectsByValue = (array, values) => {
  if (!values) return;
  var findObject = {};
  var objects = [];
  objects = values.map(value => {
    array.map( object => {
      Object.values(object).map( itemValue => {
        if (itemValue === value) {
          findObject = object;
        }
        return true;
      })
      return true;
    })
    return findObject;
  })
  return objects;
}

const getBrowserInfo = () => {
    var mobileDevice = '';
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    var mobile = navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/iPhone|iPad|iPod/i);
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'IE ',version:(tem[1]||'')};
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    if (mobile) {mobileDevice = mobile[0]}
    return {
      name: M[0],
      version: M[1],
      mobile: mobileDevice
    };
 }

/*
	       Chrome Edge Firefox Safari
Android	   ✓	   -	   ✓	    -
iOS	       *	   -	   *	    ✓
Linux	     ✓	   -	  ✓	      -
macOS	     ✓	   -	  ✓     	✓
Windows	   ✓	   ✘	  ✓     	-

* Chrome and Firefox for iOS do not have access to WebRTC APIs, unlike Safari for iOS.
  browserInfo object: {name: "Safari", version: "11", mobile: "iPhone"}

*/
const twilioBrowserSupport = () => {
   var browserInfo = getBrowserInfo();
   if (browserInfo.mobile && browserInfo.mobile.match(/iPhone|iPad|iPod/i)) {
     if (browserInfo.name === 'Firefox' || browserInfo.name === 'Chrome' ) return false;
   }else if(browserInfo.mobile && browserInfo.mobile.match(/Android/i) ) {
     if(browserInfo.name === 'Safari' || browserInfo.name === 'Edge') return false
   }
   return true;
 }

export default {twilioBrowserSupport, getBrowserInfo};
