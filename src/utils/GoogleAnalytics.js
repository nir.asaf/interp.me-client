import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactGA from 'react-ga'
import { Route } from 'react-router-dom'

const DEFAULT_CONFIG = {
  trackingId: 'UA-120061971-1', // production account // TODO: move configuration to a proper location
  // trackingId: 'UA-123015616-1', // dev account
  debug: true,
  gaOptions: {
  }
}; // TODO: make changes here if needed - see: https://github.com/react-ga/react-ga/blob/master/README.md

class GoogleAnalytics extends Component {
  componentDidMount () {
    this.logPageChange(
      this.props.location.pathname,
      this.props.location.search,
      this.props.location.hash,
    )
  }

  componentDidUpdate ({ location: prevLocation }) {
    const { location: { pathname, search, hash } } = this.props
    const isDifferentPathname = pathname !== prevLocation.pathname
    const isDifferentSearch = search !== prevLocation.search
    const isDifferentHash = hash !== prevLocation.hash

    if (isDifferentPathname || isDifferentSearch || isDifferentHash) {
      this.logPageChange(pathname, search, hash)
    }
  }

  logPageChange (pathname, search, hash = '') {
    const page = pathname + search + hash
    const { location } = window
    ReactGA.set({
      page,
      location: `${location.origin}${page}`,
      ...this.props.options
    })
    ReactGA.pageview(page)
  }

  render () {
    return null
  }
}

GoogleAnalytics.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
    search: PropTypes.string,
    hash: PropTypes.string
  }).isRequired,
  options: PropTypes.object
}

const RouteTracker = () =>
  <Route component={GoogleAnalytics} />

var initialized = false;
const init = (options = {}) => {
  if (initialized) {
    return true;
  }

  // if production
  const hostname = window && window.location && window.location.hostname;
  if (hostname === 'app.interp.me') {
  // if (true) { // - dev env
      ReactGA.initialize(
      DEFAULT_CONFIG
    );
    initialized = true;
    return true;
  }

  return false;
}

const setProperties = (props) => {
  if (initialized) {
    ReactGA.set(props);
  }
}

const logModalView = (modalName) => {
  if (initialized) {
    ReactGA.modalview(modalName);
  }
}

// args can have: (from: https://github.com/react-ga/react-ga/blob/master/README.md)
// args.label	String. Optional. More precise labelling of the related action. E.g. alongside the 'Added a component' action, we could add the name of a component as the label. E.g. 'Survey', 'Heading', 'Button', etc.
// args.value	Int. Optional. A means of recording a numerical value against an event. E.g. a rating, a score, etc.
// args.nonInteraction	Boolean. Optional. If an event is not triggered by a user interaction, but instead by our code (e.g. on page load, it should be flagged as a nonInteraction event to avoid skewing bounce rate data.
// args.transport	String. Optional. This specifies the transport mechanism with which hits will be sent. Valid values include 'beacon', 'xhr', or 'image'.
const logEvent = (category, action, args) => {
  var gaArgs = {
    category : category,
    action : action
  };
  if (args && args.label) {
    gaArgs.label = args.label;
  }
  if (args && args.value) {
    gaArgs.value = args.value;
  }
  if (args && args.nonInteraction) {
    gaArgs.nonInteraction = args.nonInteraction;
  }
  if (args && args.transport) {
    gaArgs.transport = args.transport;
  }

  ReactGA.event(gaArgs);
}

export default {
  GoogleAnalytics,
  RouteTracker,
  init,
  setProperties,
  logModalView,
  logEvent
}
