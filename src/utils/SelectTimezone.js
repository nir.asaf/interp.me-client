import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import 'moment-timezone';
import LanguageIcon from '@material-ui/icons/Language';
import SelectComponent from '../components/Common/SelectComponent';
import { TIME_ZONES } from '../constants/timeZone';

class SelectTimezone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: props.value,
    }
    this.timeZones= this.timeZones.bind(this);
  }

  timeZones() {
    const timeZones = moment.tz.names();
    const offsetTmz = [];
    for (const i in timeZones) {

      const tz = moment.tz(timeZones[i]).format('Z').replace(':00', '').replace(':30', '.5');
      let x = (tz === 0) ? 0 : parseInt(tz, 10).toFixed(2);
      const timeZone = {
        label: `(GMT${moment.tz(timeZones[i]).format('Z')})${timeZones[i]}`,
        value: `${timeZones[i]}`,
        time: `${x}`,
      };
      offsetTmz.push(timeZone);
    }

    return _.sortBy(offsetTmz, [function (el) { return -(el.time); }]);
  }

  handleChange(value) {
    this.setState({
      selectedValue: value
    });
    this.props.onChange(value);
  }

  render() {
    // const selectOptions = this.timeZones();
    const {label=null, placeholder="Time Zone"} = this.props;
    const selectOptions = TIME_ZONES.map(timezone => ({
      value: timezone,
      label: timezone,
    }));

    return (
      <SelectComponent
        suggestions={selectOptions}
        placeholder={placeholder}
        icon={<LanguageIcon/>}
        onChange={(value) => this.handleChange(value)}
        value={this.state.selectedValue}
        label={label}
        selectType={this.props.selectType}
        />
    )
  }
}

export default SelectTimezone;
