import * as actionTypes from '../constants/actionTypes';
import mixpanel from 'mixpanel-browser';

export function mixpanelInit() {
  mixpanel.init("7b4922101515e308f817ae9d51e2ab89")
  return mixpanel
}

export function mixpanelPerson(userPayload) {
  var mixpanel = mixpanelInit();
  mixpanel.identify(userPayload.uid)
  mixpanel.people.set({
    userType : userPayload.userType,
    $email : userPayload.email,
    gender : userPayload.gender,
    $name : userPayload.name,
    $phone : userPayload.phone,
    signLanguages : userPayload.signLanguages,
    spokenLanguages : userPayload.spokenLanguages,
    aslCertificates : userPayload.aslCertificates,
    bio : userPayload.bio,
    instant_fixed : (userPayload.callPreferences && userPayload.callPreferences.instant_fixed)
                      ? userPayload.callPreferences.instant_fixed.enabled : null,
    instant_free : (userPayload.callPreferences && userPayload.callPreferences.instant_free)
                      ? userPayload.callPreferences.instant_free.enabled : null,
    scheduled_physical : (userPayload.callPreferences && userPayload.callPreferences.scheduled_physical)
                      ? userPayload.callPreferences.scheduled_physical.enabled : null,
    scheduled_video : (userPayload.callPreferences && userPayload.callPreferences.scheduled_video)
                      ? userPayload.callPreferences.scheduled_video.enabled : null,
    country : userPayload.country,
    experience : userPayload.experience,
    specialized : userPayload.specialized,
    zipCode : userPayload.zipCode
  })
}

export function sendMixpanelEvent(eventName, mixpanelParams = {}) {
  return function(dispatch, getState) {
    let params = mixpanelParams;

    if (typeof(params) === 'string') {
      params = JSON.parse(params);
    }

    dispatch({
      type: actionTypes.MIXPANEL_EVENT,
      meta: {
        mixpanel: {
          event: eventName,
          props: Object.assign({}, params)
        }
      }
    });
  }
}
