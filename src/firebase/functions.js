import { functions } from './firebase';

export const configGetSignUpProperties = () => {
  var getSignUpProperties = functions.httpsCallable('configGetSignUpProperties');
  return getSignUpProperties().then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const configSendFeedback = (data) => {
  var getConfigSendFeedback= functions.httpsCallable('configSendFeedback');
  return getConfigSendFeedback(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error}});
}

export const configGetSpokenLanguages = () => {
  var getConfigGetSpokenLanguages = functions.httpsCallable('configGetSpokenLanguages');
  return getConfigGetSpokenLanguages().then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error}});
}

export const usersSignUp = (user) => {
  var getUsersSignUp = functions.httpsCallable('usersSignUp');
  return getUsersSignUp(user);
}

export const usersGetCurrent = () => {
  var getUsersGetCurrent = functions.httpsCallable('usersGetCurrent');
  return getUsersGetCurrent().then((resp) => {
      return resp.data;
  });
}

export const usersGet = (data) => {
  var getUsersGet = functions.httpsCallable('usersGet');
  return getUsersGet(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

// Payment
export const deafRemovePayment = (data) => {
  var getDeafRemovePayment = functions.httpsCallable('deafRemovePayment');
  return getDeafRemovePayment(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const deafAddPayment = (data) => {
  var getDeafAddPayment = functions.httpsCallable('deafAddPayment');
  return getDeafAddPayment(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const deafGetPaymentIds = () => {
  var getDeafGetPaymentIds = functions.httpsCallable('deafGetPaymentIds');
  return getDeafGetPaymentIds().then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const deafGetChargeTypes = () => {
  var getDeafGetChargeTypes = functions.httpsCallable('deafGetChargeTypes');
  return getDeafGetChargeTypes().then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const deafGetMinutesCredit = () => {
  var getDeafGetMinutesCredit = functions.httpsCallable('deafGetMinutesCredit');
  return getDeafGetMinutesCredit().then((resp) => {
    return resp;
  }).catch((error) => {return {error: error} });
}

export const deafBuyMinutesCredit = (data) => {
  var getDeafBuyMinutesCredit = functions.httpsCallable('deafBuyMinutesCredit');
  return getDeafBuyMinutesCredit(data).then((resp) => {
    return resp;
  }).catch((error) => {return {error: error} });
}

export const interpretersGetPayoutCredit = () => {
  var getInterpretersGetPayoutCredit = functions.httpsCallable('interpretersGetPayoutCredit');
  return getInterpretersGetPayoutCredit().then((resp) => {
    return resp;
  }).catch((error) => {return {error: error} });
}

// CALL FUNCTION

export const callsGet = (id) => {
  var getCallsGet = functions.httpsCallable('callsGet');
  return getCallsGet(id).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const callsFinish = (id) => {
  var getCallsFinish = functions.httpsCallable('callsFinish');
  return getCallsFinish(id).then((resp) => {
    return resp;
  }).catch((error) => {return {error: error} });
}

export const callsCreate = (obj) => {
  var getCallsCreate = functions.httpsCallable('callsCreate');
  return getCallsCreate(obj).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const callsStartSession = (data) => {
  var getCallsStartSession = functions.httpsCallable('callsStartSession');
  return getCallsStartSession(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const callsConnectInterpreter = (id) => {
  var getCallsConnectInterpreter = functions.httpsCallable('callsConnectInterpreter');
  return getCallsConnectInterpreter(id).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const callsPause = (id) => {
  var getCallsPause = functions.httpsCallable('callsPause');
  return getCallsPause(id).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const callsNewMatch = (data) => {
  var getCallsNewMatch = functions.httpsCallable('callsNewMatch');
  return getCallsNewMatch(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const interpretersWithdraw = (data) => {
  var getInterpretersWithdraw = functions.httpsCallable('interpretersWithdraw');
  return getInterpretersWithdraw(data).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const usersEdit = (user) => {
  var getUsersEdit = functions.httpsCallable('usersEdit');
  return getUsersEdit(user).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const interpretersSearch = (date) => {
  var getInterpretersSearch = functions.httpsCallable('interpretersSearch');
  return getInterpretersSearch(date).then((resp) => {
      return resp.data;
  }).catch((error) => {return {error: error} });
}

export const deafGetCallHistory = (data) => {
  var getDeafGetCallHistory = functions.httpsCallable('deafGetCallHistory');
  return getDeafGetCallHistory(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const verificationSend = (data) => {
  var getVerificationSend = functions.httpsCallable('verificationSend');
  return getVerificationSend(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const verificationVerify = (data) => {
  var getVerificationVerify = functions.httpsCallable('verificationVerify');
  return getVerificationVerify(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleCreate = (data) => {
  var getScheduleCreate = functions.httpsCallable('scheduleCreate');
  return getScheduleCreate(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleListDeaf = (data) => {
  var getScheduleListDeaf = functions.httpsCallable('scheduleListDeaf');
  return getScheduleListDeaf(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleListInterpreter = (data) => {
  var getscheduleListInterpreter = functions.httpsCallable('scheduleListInterpreter');
  return getscheduleListInterpreter(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleGet = (data) => {
  var getScheduleGet = functions.httpsCallable('scheduleGet');
  return getScheduleGet(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleGetOrCreateCall = (data) => {
  var getScheduleGetOrCreateCall = functions.httpsCallable('scheduleGetOrCreateCall');
  return getScheduleGetOrCreateCall(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleUpdate = (data) => {
  var getScheduleUpdate = functions.httpsCallable('scheduleUpdate');
  return getScheduleUpdate(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleReject = (data) => {
  var getScheduleReject = functions.httpsCallable('scheduleReject');
  return getScheduleReject(data).then((resp) => {
    console.log(resp);
    return resp.data;
  }).catch((error) => {return {error: error} });
}

export const scheduleAccept = (data) => {
  var getScheduleAccept = functions.httpsCallable('scheduleAccept');
  return getScheduleAccept(data).then((resp) => {
    return resp.data;
  }).catch((error) => {return {error: error} });
}

 // reviews
 export const reviewsCreate = (data) => {
   var getReviewsCreate = functions.httpsCallable('reviewsCreate');
   return getReviewsCreate(data).then((resp) => {
     return resp.data;
   }).catch((error) => {return {error: error} });
 }

 export const reviewsList = (data) => {
   var getReviewsList = functions.httpsCallable('reviewsList');
   return getReviewsList(data).then((resp) => {
     return resp.data;
   }).catch((error) => {return {error: error} });
 }
