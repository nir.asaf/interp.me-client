import { providers, auth } from './firebase';

// Sign Up - email+password
export const doCreateUserWithEmailAndPassword = (email, password) =>
  auth.createUserWithEmailAndPassword(email, password);

// Sign Up - facebook
export const doSignupWithFacebook = () =>
  auth.signInWithPopup(new providers.FacebookAuthProvider());

// Sign Up - google
export const doSignupWithGoogle = () =>
  auth.signInWithPopup(new providers.GoogleAuthProvider());

// Sign In
export const doSignInWithEmailAndPassword = (email, password) =>
  auth.signInWithEmailAndPassword(email, password);

// Sign out
export const doSignOut = () =>
  auth.signOut();

// Password Reset
export const doPasswordReset = (email) =>
  auth.sendPasswordResetEmail(email);

// Password Change
export const doPasswordUpdate = (password) =>
  auth.currentUser.updatePassword(password);

// delete current user. only used for error handling during signUp
export const doDeleteUser = () =>
  auth.currentUser.delete();

// send email verificaiton for current user.
export const doSendEmailVerification = () => {
  var env = process.env.NODE_ENV;
  var url;
  if (env === 'development') {url = 'http://localhost:4100/'}
  else if (env === 'staging') {url = 'https://staging.interp.me/'}
  else if (env === 'production') {url = 'https://app.interp.me/'}
  var actionCodeSettings = {
    url: url + '?email=' + auth.currentUser.email
  }
  return auth.currentUser.sendEmailVerification(actionCodeSettings);
}
