import * as auth from './auth';
import * as firebase from './firebase';
import * as functions from './functions';

export {
  auth,
  functions,
  firebase,
};
