import * as firebase from 'firebase';
require("firebase/functions");

const prodConfig = {
  apiKey: "AIzaSyDS6m_JhURlLbRutBoys60wBd1EN-hYLfs",
  authDomain: "interpme-39c4d.firebaseapp.com",
  databaseURL: "https://interpme-39c4d.firebaseio.com",
  projectId: "interpme-39c4d",
  storageBucket: "interpme-39c4d.appspot.com",
  messagingSenderId: "23082750199",
};

const stagingConfig = {
  apiKey: "AIzaSyD38q2QZqjvsI7HHQRQ7fPpzSHMFJCZLg4",
  authDomain: "interpme-staging.firebaseapp.com",
  databaseURL: "https://interpme-staging.firebaseio.com",
  projectId: "interpme-staging",
  storageBucket: "interpme-staging.appspot.com",
  messagingSenderId: "475488440712",
};

var config = stagingConfig;
const hostname = window && window.location && window.location.hostname;
if (hostname === 'app.interp.me') {
  config = prodConfig;
}

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();
const functions = firebase.functions();
const providers = firebase.auth;

export {
  db,
  auth,
  functions,
  providers
};
