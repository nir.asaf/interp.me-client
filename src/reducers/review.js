import {
  REVIEW_CREATE,
  REVIEWS_LIST,
} from '../constants/actionTypes';

const initialState = {
  reviews: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case REVIEW_CREATE:
      return {
        reviewId: action.error ? action.payload.errors : action.payload.id
      };
    case REVIEWS_LIST:
      return {
        ...state,
        reviews: action.error ? action.payload.errors : action.payload.reviews
      }
    default:
      return state;
  }
};
