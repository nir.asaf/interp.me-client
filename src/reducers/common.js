import helpers from '../utils/Helpers';

import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  SETTINGS_SAVED,
  LOGIN,
  HOME_PAGE_UNLOADED,
  PROFILE_PAGE_UNLOADED,
  SETTINGS_PAGE_UNLOADED,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  CONFIG_GET_SIGN_UP_PROPERTIES,
  SIGNUP_COMPLETED,
  SIGNUP_PAGE_UNLOADED,
  COMPLETE_DETAILS,
  OPEN_MODAL,
  CLOSE_MODAL,
  CALL_CREATE,
  ASYNC_START,
  GET_CURRENT_USER,
  ASYNC_END,
  LOGIN_WITH_PROVIDER,
  REDIRECT_TO,
  OPEN_USER_SNACKBAR,
  CLOSE_USER_SNACKBAR,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const defaultState = {
  inProgress: false,
  appName: 'Interp.Me',
  env: process.env.NODE_ENV,
  viewChangeCounter: 0,
  browserInfo: helpers.getBrowserInfo(),
  signLanguages: [],
  spokenLanguages: [],
  aslCertificates: [],
  experienceOptions: [],
  specializationOptions: [],
  countries: [],
  loadingScreen: false,
  error: null,
  openUserSnackbar: false,
  config: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        appLoaded: true,
        currentUser: action.payload && action.payload.currentUser ? action.payload.currentUser : action.payload,
        config: action.payload && action.payload.config ? action.payload.config : [],
        redirectTo: action.payload && action.payload.currentUser && action.payload.currentUser.registered === false  ? '/complete_details' : null,
      };
    case REDIRECT:
      return { ...state, redirectTo: null };
    case REDIRECT_TO:
      return { ...state, redirectTo: action.payload };
    case LOGOUT:
      console.log(' Commong logout called');
      return { ...state, redirectTo: '/login', currentUser: null, inProgress: false, loadingScreen: false };
    case SETTINGS_SAVED:
      return {
        ...state,
        redirectTo: action.error ? null : '/',
        currentUser: action.error ? null : action.payload.user
      };
    case LOGIN:
      return {
        ...state,
        redirectTo: action.payload.error && !action.payload.path ? null : action.payload.path,
        currentUser: action.payload.user,
        completeDetails: action.payload.error ? null : true,
        inProgress: false,
        loadingScreen: false,
      };
    case LOGIN_WITH_PROVIDER:
      return {
        ...state,
        redirectTo: action.payload.error || !action.payload.path ? '/dashboard' : action.payload.path,
        currentUser: action.payload.user,
        completeDetails: action.payload.error ? null : true,
        inProgress: false,
        loadingScreen: false,
      }
    case COMPLETE_DETAILS:
      return {
        ...state,
        redirectTo: action.payload.error ? null : "/dashboard",
        currentUser: action.payload.user,
        inProgress: action.payload.inProgress,
        completeDetails: action.payload.error ? null : true,
        errors: action.payload.error ? action.payload.error : null,
      }
    case SIGNUP_COMPLETED:
      return {
        ...state,
        redirectTo: action.payload.error ? null : '/complete_details#step=userType',
        currentUser: action.payload.error ? null : action.payload,
        completeDetails: action.payload.error ? null : false,
        inProgress: false,
        loadingScreen: false,
      };
    case HOME_PAGE_UNLOADED:
    case PROFILE_PAGE_UNLOADED:
    case SETTINGS_PAGE_UNLOADED:
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
    case SIGNUP_PAGE_UNLOADED:
      return { ...state, viewChangeCounter: state.viewChangeCounter + 1 };
    case CONFIG_GET_SIGN_UP_PROPERTIES:
      return {
        ...state,
        config: action.payload,
        signLanguages: action.payload.signLanguages,
        spokenLanguages: action.payload.spokenLanguages,
        countries: action.payload.countries,
        aslCertificates: action.payload.aslCertificates,
        specializationOptions: action.payload.specializationOptions,
        experienceOptions: action.payload.experienceOptions,
      };
    case OPEN_MODAL:
      return {
        ...state,
        model: action.payload,
      };
    case CALL_CREATE:
      if (!action.payload.error) {
        const redirectUrl = `/call/${action.payload.id}`;
        return { ...state, redirectTo: redirectUrl };
      }
     return state;
    case CLOSE_MODAL:
      return {
        ...state,
        model: null,
        inProgress: false,
      }
    case GET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload.user
      }
    case ASYNC_START:
      return {
        ...state,
        inProgress: true,
        loadingScreen: action.payload.loadingScreen ? true : false,
        text: action.payload.text ? action.payload.text : "",
      };
    case ASYNC_END:
      return { ...state, inProgress: false, loadingScreen: false };
    case OPEN_USER_SNACKBAR:
      return {
        ...state,
        message: action.payload.message ? action.payload.message : "" ,
        openUserSnackbar: action.payload.message ? true : false,
        variant: action.payload.variant ? action.payload.variant : "info",
    };
    case HANDLE_ERROR:
      return {
        ...state,
        message: action.error.message ? action.error.message : "" ,
        openUserSnackbar: true,
        variant: action.error.variant ? action.error.variant : "error",
    };
    case CLOSE_USER_SNACKBAR:
      return { ...state, openUserSnackbar: false };
    default:
      return state;
  }
};
