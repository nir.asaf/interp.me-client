import {
  SETTINGS_SAVED,
  SETTINGS_PAGE_UNLOADED,
  DEAF_GET_MINUTES_CREDIT,
  INTERPRETER_GET_PAYOUT_CREDIT,
  USER_EDIT,
  DEAF_GET_CALL_HISTORY,
} from '../constants/actionTypes';

const initialState = {
  inProgress: false,
  detailsEditStatus: null,
  callHistory: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_EDIT:
      return {
        ...state,
        detailsEditStatus: action.error ? action.payload.errors : {success: true}
      };
    case SETTINGS_SAVED:
      return {
        ...state,
        inProgress: false,
        errors: action.error ? action.payload.errors : null
      };
    case SETTINGS_PAGE_UNLOADED:
      return {};
    case DEAF_GET_MINUTES_CREDIT:
      return {
        ...state,
        errors: action.error ? action.payload.errors : null,
        credit: action.payload.credit,
      };
    case INTERPRETER_GET_PAYOUT_CREDIT:
      return {
        ...state,
        errors: action.error ? action.payload.errors : null,
        credit: action.error ? null : action.payload.credit / 100,
      };
    case DEAF_GET_CALL_HISTORY:
      return {
        ...state,
        callHistory: action.error ? action.payload.errors : action.payload,
      }
    default:
      return state;
  }
};
