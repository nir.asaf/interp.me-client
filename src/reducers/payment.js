import {
  DEAF_GET_CHARGE_TYPES,
  DEAF_BUY_MINUTES_CREDIT,
  INTERPRETERS_WITHDRAW,
  RESET_CREDIT,
  DEAF_ADD_PAYMENT,
  DEAF_GET_PAYMENT_IDS,
  DEAF_SELECT_PAYMENT_CARD,
  DEAF_REMOVE_PAYMENT,
} from '../constants/actionTypes';

const initialState = {
  credit: null,
  cardsDetails: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case DEAF_GET_CHARGE_TYPES:
      return {
        ...state,
        chargeTypes: action.payload.error  ? null : action.payload,
        errors: action.payload.error ? action.payload.error : null,
      };
    case DEAF_BUY_MINUTES_CREDIT:
      return {
        ...state,
        credit: action.payload.error ? action.payload.error : action.payload.data,
      }
    case INTERPRETERS_WITHDRAW:
     return {
       ...state,
     }
    case DEAF_ADD_PAYMENT:
      return {
        success: action.payload.error ? false : true,
      }
    case DEAF_GET_PAYMENT_IDS:
      return {
        cardsDetails: action.payload.error ? action.payload.error : action.payload.paymentIds,
      }
    case DEAF_SELECT_PAYMENT_CARD:
      return {
        ...state,
        selectedCard: action.data,
      }
    case DEAF_REMOVE_PAYMENT:
      return {
        paymentId: action.payload.error ? action.payload.error : action.payload,
        cardsDetails:  action.payload.error ? state.cardsDetails : state.cardsDetails.filter(el => el.id !== action.paymentId),
      }
    case RESET_CREDIT:
      return {
        credit: null,
      }
    default:
      return state;
  }
};
