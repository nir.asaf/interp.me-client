import {
  JOIN_ROOM
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case JOIN_ROOM:
      return {
        ...state
      };
    default:
      return state;
  }
};
