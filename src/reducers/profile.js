import {
  USERS_GET,
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case USERS_GET:
      return {
        profile: action.payload.error ? action.payload.error : action.payload,
      };
    default:
      return state;
  }
};
