import {
  LOGIN,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  UPDATE_FIELD_AUTH,
  RECOVER_PASSWORD,
  LOGIN_START,
  SEND_EMAIL_VERIFICATION,
  LOGIN_WITH_PROVIDER,
  SIGNUP_COMPLETED,
} from '../constants/actionTypes';

const initialState = {
  inProgress: false,
  email: '',
  password: '',
  errors: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_START:
      return {
        ...state,
        inProgress: action.payload.loading ? true : false,
      };
    case LOGIN:
      return {
        errors: action.payload.error ? action.payload.error : null,
        inProgress: false
      };
    case LOGIN_WITH_PROVIDER:
      return {
        inProgress: false,
      }
    case SIGNUP_COMPLETED:
      return {
        inProgress: false,
      }
    case SEND_EMAIL_VERIFICATION:
      return{
        ...state,
        errors: action.payload.error ? action.payload.error : null,
        emailSent: action.payload.error ? null : true,
      };
    case RECOVER_PASSWORD:
      return {
        ...state,
        inProgress: false,
        error: action.payload.error ? action.payload.error : null,
        emailSent: action.payload.email != null,
      };
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return {};
    case UPDATE_FIELD_AUTH:
      return {
        ...state,
        [action.key]: action.value,
        errors: null
      };
    default:
      return state;
  }
};
