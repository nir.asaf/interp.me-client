import {
  SIGNUP_COMPLETED,
  SIGNUP_PAGE_UNLOADED,
  SIGNUP_UPDATE_FIELD,
  COMPLETE_DETAILS_UNLOADED,
  SIGNUP_START,
} from '../constants/actionTypes';

const initialState = {
  inProgress: false,
  email: '',
  password: '',
  errors: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_START:
      return {
        inProgress: action.payload.loading ? true : false,
      };
    case SIGNUP_COMPLETED:
      return {
        ...state,
        inProgress: false,
        loadingScreen: false,
        errors: action.payload.error ? action.payload.error : null,
      };
    case COMPLETE_DETAILS_UNLOADED:
      return {};
    case SIGNUP_PAGE_UNLOADED:
      return {};
    case SIGNUP_UPDATE_FIELD:
      return {
        ...state,
        [action.key]: action.value,
        errors: null
      };
    default:
      return state;
  }
};
