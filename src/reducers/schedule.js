import {
  SCHEDULE_CREATE,
  SCHEDULE_LIST_DEAF,
  SCHEDULE_LIST_INTERPERTER,
  SCHEDULE_GET,
  SCHEDULE_GET_OR_CREATE_CALL,
  SCHEDULE_UPDATE,
  SCHEDULE_REJECT,
  SCHEDULE_ACCEPT,
} from '../constants/actionTypes';

const initialState = {
  inProgress: false,
  scheduleList: null,
  scheduleItem: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SCHEDULE_CREATE:
      return {
        ...state,
        id: action.error ? action.payload.errors : action.payload
      };
    case SCHEDULE_LIST_DEAF:
      return {
        ...state,
        scheduleList: action.error ? null : action.payload,
      }
    case SCHEDULE_LIST_INTERPERTER:
      return {
        ...state,
        scheduleList: action.error ? null : action.payload,
      }
    case SCHEDULE_GET:
      return {
        ...state,
        scheduleItem: action.error ? {} : action.payload,
      }
    case SCHEDULE_GET_OR_CREATE_CALL:
      return {
        ...state,
        callId: action.error ? {} : action.payload.callId,
      }
    case SCHEDULE_UPDATE:
      return {
        id: action.payload,
      }
    case SCHEDULE_REJECT:
      return {
        id: action.payload,
      }
    case SCHEDULE_ACCEPT:
      return {
        id: action.payload,
      }
    default:
      return state;
  }
};
