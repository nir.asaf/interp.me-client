import {
  CALL_CREATE,
  CALLS_GET,
  CALLS_CONNECT_INTERPRETER,
  CALLS_PAUSE,
  CALLS_START,
  CALLS_NEW_MATCH,
  CALLS_DATA_RESET,
  INTERPRETER_SEARCH,
  CALLS_FINISH,
  RESET_TIMER,
  START_TIMER,
  STOP_TIMER,
} from '../constants/actionTypes';

const initialState = {
  inProgress: false,
  callId: '',
  data: null,
  time: 0,
  listOfInterpreters: [],
  interpreterTotal: 0,
  callState: '',
  startedAt: undefined,
  stoppedAt: undefined,
  baseTime: undefined,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CALL_CREATE:
      return {
        ...state,
        callId: action.payload.error ? null : action.payload.id,
        errors: action.payload.error ? action.payload.error : null,
      };
    case CALLS_GET:
      return {
        ...state,
        data: action.payload.error ? null : action.payload
      };
    case CALLS_CONNECT_INTERPRETER:
      return {
        ...state,
        callState: action.payload.error ? null : 'connected',
      }
    case CALLS_START:
      return {
        ...state,
        callState: action.payload.error ? null : 'start',
      }
    case CALLS_PAUSE:
      return {
        ...state,
        callState: action.payload.error ? null : 'pause',
      }
    case CALLS_FINISH:
      return {
        ...state,
        callState: action.payload.error ? null : 'finish'
      }
    case CALLS_NEW_MATCH:
      return {
        ...state,
        callState: action.payload.error ? null : 'switch'
      };
    case INTERPRETER_SEARCH:
      return {
        listOfInterpreters: action.payload.error ? [] : action.payload.results,
        interpreterTotal: action.payload.error ? 0 : action.payload.total,
      }
    case CALLS_DATA_RESET:
      return {
        ...state,
        data: null,
        callState: null,
        callId: '',
      }
    case RESET_TIMER:
      return {
        ...state,
        baseTime: 0,
        startedAt: state.startedAt ? action.now : undefined,
        stoppedAt: state.stoppedAt ? action.now : undefined
      };
    case START_TIMER:
      return {
        ...state,
        baseTime: action.baseTime,
        startedAt: action.now,
        stoppedAt: undefined
      };
    case STOP_TIMER:
      return {
        ...state,
        stoppedAt: action.now
      }
    default:
      return state;
  }
};
