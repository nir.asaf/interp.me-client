import {
  CONFIG_GET_SPOKEN_LANGUAGES,
  CONFIG_SEND_FEEDBACK,
} from '../constants/actionTypes';

const initialState = {
  spokenLanguages: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CONFIG_GET_SPOKEN_LANGUAGES:
      return {
        ...state,
        spokenLanguages: action.payload,
      };
    case CONFIG_SEND_FEEDBACK:
      return {
        success: action.payload.error ? false : true,
      };
    default:
      return state;
  }
};
