import {
  VERIFICATION_SEND,
  VERIFICATION_VERIFY,
} from '../constants/actionTypes';

const initialState = {
  verificationSuccess: null,
  verificationId: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case VERIFICATION_SEND:
      return {
        ...state,
        verificationId: action.payload.error  ? null : action.payload.verificationId,
      };
    case VERIFICATION_VERIFY:
      return {
        ...state,
        verificationSuccess: action.payload.error  ? null : action.payload.success,
      }
    default:
      return state;
  }
};
