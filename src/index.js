import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import { store, history} from './store';

import 'font-awesome/css/font-awesome.min.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import 'mdbreact/dist/css/mdb.css';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';


import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import App from './views/App';
import { SnackbarProvider } from 'material-ui-snackbar-redux';

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
  overrides: {
    MuiButton: {
      root: {
        '&:focus': {
          boxShadow: 'none',
          outline: 'none',
        },
      }
    },
  },
});

ReactDOM.render((
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <SnackbarProvider SnackbarProps={{ autoHideDuration: 3500 }}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route path="/" component={App} />
          </Switch>
        </ConnectedRouter>
      </SnackbarProvider>
    </Provider>
  </MuiThemeProvider>
), document.getElementById('root'));
