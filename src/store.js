import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { promiseMiddleware, localStorageMiddleware, callMiddleware, configMiddleware, errorHandler } from './middleware';
import reducer from './reducer';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory';
import reduxCatch from 'redux-catch';

// mixpanel
import MixpanelMiddleware from 'redux-mixpanel-middleware'
import { mixpanelInit } from './utils/MixpanelUtil';

const mixpanel = mixpanelInit();
const mixpanelMiddleware = new MixpanelMiddleware(mixpanel)

export const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const myRouterMiddleware = routerMiddleware(history);
const myErrorHandler = reduxCatch(errorHandler);

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(myRouterMiddleware, promiseMiddleware, localStorageMiddleware, callMiddleware, configMiddleware, thunk, myErrorHandler, mixpanelMiddleware);
  } else {
    // Enable additional logging in non-production environments.
    return applyMiddleware(myRouterMiddleware, promiseMiddleware, localStorageMiddleware, callMiddleware, configMiddleware, thunk, createLogger(), myErrorHandler, mixpanelMiddleware);
  }
};



export const store = createStore(
  reducer, composeWithDevTools(getMiddleware()));
