import actions from './actions';

import {
  ASYNC_END,
  LOGIN,
  LOGOUT,
  APP_LOAD,
  SIGNUP_COMPLETED,
  COMPLETE_DETAILS,
  GET_CURRENT_USER,
  CALL_CREATE,
  CALLS_FINISH,
  LOGIN_WITH_PROVIDER,
  CONFIG_GET_SIGN_UP_PROPERTIES
} from './constants/actionTypes';
import GA from './utils/GoogleAnalytics';
import { snackbarActions as snackbar } from 'material-ui-snackbar-redux';

const promiseMiddleware = store => next => action => {
  if (isPromise(action.payload)) {
    // store.dispatch({ type: ASYNC_START, subtype: action.type });

    const currentView = store.getState().viewChangeCounter;
    const skipTracking = action.skipTracking;

    action.payload.then(
      res => {
        const currentState = store.getState()
        if (!skipTracking && currentState.viewChangeCounter !== currentView) {
          return
        }
        console.log('RESULT', res);
        action.payload = res;
        store.dispatch({ type: ASYNC_END, promise: action.payload });
        store.dispatch(action);
      },
      error => {
        const currentState = store.getState()
        if (!skipTracking && currentState.viewChangeCounter !== currentView) {
          return
        }
        console.log('ERROR', error);
        action.error = true;
        action.payload = error.response && error.response.body;
        if (!action.skipTracking) {
          store.dispatch({ type: ASYNC_END, promise: action.payload });
        }
        store.dispatch(action);
      }
    );

    return;
  }

  next(action);
};

const localStorageMiddleware = store => next => action => {
  if (action.type === SIGNUP_COMPLETED) {
    if (!action.payload.error) {
      window.localStorage.setItem('currentUser', JSON.stringify({email: action.payload.email, uid: action.payload.uid}));
    }
  } else if (action.type === COMPLETE_DETAILS || action.type === LOGIN || action.type === GET_CURRENT_USER) {
    window.localStorage.setItem('currentUser', JSON.stringify(action.payload.user));
    // set user properties in GoogleAnalytics
    GA.setProperties({
      userId : action.payload.user && action.payload.user.uid ? action.payload.user.uid : null,
      userRegistered : action.payload.user && action.payload.user.registered ? action.payload.user.registered : false
    });
  } else if (action.type === CONFIG_GET_SIGN_UP_PROPERTIES) {
    window.localStorage.setItem('config', JSON.stringify(action.payload));
  } else if (action.type === LOGOUT) {
    window.localStorage.removeItem('currentUser');

    // clear user properties in GoogleAnalytics
    GA.setProperties({
      userId : null,
      userRegistered : null
    });
  }
  next(action);
};

const callMiddleware = store => next => action => {
  if (action.type === CALL_CREATE || action.type === CALLS_FINISH) {
    actions.callsGet(action.payload);
  }
  next(action);
}

const configMiddleware = store => next => action => {
  if (action.type === APP_LOAD || action.type === LOGIN_WITH_PROVIDER) {
    var common = store.getState().common;
    if (common.config && (!common.config.signLanguages || !common.config.spokenLanguages || !common.config.countries)) {
      store.dispatch(actions.configGetSignUpProperties());
    }
  }
  next(action);
}

const errorHandler = (error, getState, lastAction, dispatch) => {
  dispatch(snackbar.show({
    message: 'Something went wrong. Please try again later.',
    action: 'close'
  }));
 
  console.debug('Redux catch error :: ', error);
  console.debug('current state', getState());
  console.debug('last action was', lastAction);
}

function isPromise(v) {
  return v && typeof v.then === 'function';
}


export { promiseMiddleware, localStorageMiddleware, callMiddleware, configMiddleware, errorHandler }
