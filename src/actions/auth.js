// Action for dealing with Auth fucntion: login, logout, forgot password

import { auth, functions } from '../firebase';
import { mixpanelPerson } from '../utils/MixpanelUtil';

import {
    UPDATE_FIELD_AUTH,
    LOGIN,
    LOGIN_PAGE_UNLOADED,
    LOGOUT,
    RECOVER_PASSWORD,
    LOGIN_START,
    SEND_EMAIL_VERIFICATION,
    GET_CURRENT_USER,
    CONFIG_GET_SIGN_UP_PROPERTIES,
    LOGIN_WITH_PROVIDER,
    SIGNUP_COMPLETED,
    ASYNC_START,
    ASYNC_END,
  } from '../constants/actionTypes';

import {
      FACEBOOK,
      GOOGLE,
    } from '../constants/signupProviders';

const updateAuthField = (key, value) => {
  return ({ type: UPDATE_FIELD_AUTH, key, value })
}

const login = (email, password, path) =>{
  return (dispatch) => {
    dispatch({ type: LOGIN_START, payload: {loading: true} , meta: {mixpanel: {event: "Login Start", props: {type: 'email'}}}});
    auth.doSignInWithEmailAndPassword(email, password).then( payload => {
      functions.configGetSignUpProperties().then(payload => {
        dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });
      });
      functions.usersGetCurrent().then(user => {
        var pathName = '/dashboard';
        if (user.registered) {
          mixpanelPerson(user);
          pathName = path.pathname === '/complete_details#step=userType' ? 'dashboard' : path.pathname;
        } else {
          pathName = '/complete_details#step=userType';
        }
        dispatch({
          type: LOGIN,
          payload: {user: user, loading: false, path: pathName} ,
          meta: {
            mixpanel: {
              event: "Login Succeed",
              props: {
                type: 'email',
                user: user
              }
            }
          }
        });
        dispatch({ type: GET_CURRENT_USER, payload: {user: user, loading: false} });
      })
    }).catch((error) => {
      var errorPayload = error
      switch (error.code) {
        case 'auth/network-request-failed':
          errorPayload = {code: 'auth/network-request-failed', message: 'There is problem with our server try again soon.'};
          break;
        case 'auth/user-not-found':
          errorPayload = {code: '400', message: `The email entered ${email} does not exist in our system. would you like to sign up as a new user with this email and password instead?`};
          break;
        default:
          break;
      }
      dispatch({
        type: LOGIN,
        payload: {error: errorPayload},
        meta: {
          mixpanel: {
            event: "Login Error",
            props: {
              type: 'password',
              error: errorPayload.message
            }
          }
        }
      });
    });
  }
}

const loginWithProvider = (user, path) => {
  var userPayload = {
    provider: user.provider,
    email: '',
    name: '',
    phone: '',
    zipCode: '',
    photoURL: '',
    gender: '',
    locale: '',
    registered: false,
  };
  return (dispatch) => {
    dispatch({ type: LOGIN_START, payload: {loading: true} , meta: {mixpanel: {event: "Login Start", props: {type: user.provider}}}});
    switch (user.provider) {
      case FACEBOOK : {
        auth.doSignupWithFacebook().then((result) => {

          functions.usersGetCurrent().then(user => {
            dispatch({ type: GET_CURRENT_USER, payload: {user: user, loading: false} });
            if (user.registered) {
              mixpanelPerson(user);
              dispatch({
                type: LOGIN_WITH_PROVIDER,
                payload: {user: user, loading: false, path: path.pathName},
                meta: {
                  mixpanel: {
                    event: "Login Succeed",
                    props: {
                      type: 'facebook',
                      user: user
                    }
                  }
                }
              });
            } else {
              // get data from facebook login
              userPayload.email = result.user.email;
              userPayload.name = result.user.displayName;
              userPayload.phone = result.user.phoneNumber;
              userPayload.photoURL = result.user.photoURL;
              userPayload.registered = false;
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.gender) {
                switch (result.additionalUserInfo.profile.gender) {
                  case 'male':
                    userPayload.gender = 'm';
                    break;
                  case 'female':
                    userPayload.gender = 'f';
                    break;
                  default:
                    // nothing to do
                }
              }
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.locale) {
                userPayload.locale = result.additionalUserInfo.profile.locale;
              }
              dispatch({
                type: SIGNUP_COMPLETED,
                payload: userPayload,
                meta: {
                  mixpanel: {
                    event: "SignUp Started",
                    props: {
                      type: 'facebook',
                      userPayload: userPayload
                    }
                  }
                }
              });
            }
          })
        }).catch((error) => {
          dispatch({
            type: LOGIN,
            payload: {error: error},
            meta: {
              mixpanel: {
                event: "Login Error",
                props: {
                  type: 'facebook',
                  error: error.message
                }
              }
            }
          });
        })
        break;
      }
      case GOOGLE : {
        auth.doSignupWithGoogle().then((result) => {
          functions.usersGetCurrent().then(user => {
            dispatch({ type: GET_CURRENT_USER, payload: {user: user, loading: false} });
            if (user.registered) {
              mixpanelPerson(user);
              dispatch({
                type: LOGIN_WITH_PROVIDER,
                payload: {user: user, loading: false, path: path.pathName} ,
                meta: {
                  mixpanel: {
                    event: "Login Succeed",
                    props: {
                      type: 'google',
                      user: user
                    }
                  }
                }
              });
            } else {
              // get data from google login
              userPayload.email = result.user.email;
              userPayload.name = result.user.displayName;
              userPayload.phone = result.user.phoneNumber;
              userPayload.photoURL = result.user.photoURL;
              userPayload.registered = false;
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.gender) {
                switch (result.additionalUserInfo.profile.gender) {
                  case 'male':
                    userPayload.gender = 'm';
                    break;
                  case 'female':
                    userPayload.gender = 'f';
                    break;
                  default:
                    // nothing to do
                }
              }
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.locale) {
                userPayload.locale = result.additionalUserInfo.profile.locale;
              }

              dispatch({
                type: SIGNUP_COMPLETED,
                payload: userPayload,
                meta: {
                  mixpanel: {
                    event: "SignUp Started",
                    props: {
                      type: 'google',
                      userPayload: userPayload
                    }
                  }
                }
              });            }
          })
        }).catch((error) => {
          dispatch({
            type: LOGIN,
            payload: {error: error},
            meta: {
              mixpanel: {
                event: "Login Error",
                props: {
                  type: 'google',
                  error: error.message
                }
              }
            }
          });
        })
        break;
      }
      default:

    }
  }
}

const onLoginUnload = () => {
    return ({ type: LOGIN_PAGE_UNLOADED })
}

const logout = () => {
  return ({ type: LOGOUT, payload: auth.doSignOut() });
}

const sendEmailVerification = () => {
  return (dispatch) => {

    auth.doSendEmailVerification().then(() => {
      dispatch({type: SEND_EMAIL_VERIFICATION, payload: true });
    }).catch(error => {
      dispatch({type: SEND_EMAIL_VERIFICATION, payload: {error : error} });
    })
  }
}

const forgotPassword = (email) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: {loadingScreen: false, text: ''} });
    auth.doPasswordReset(email).then(() => {
      dispatch({ type: ASYNC_END});
      dispatch({type: RECOVER_PASSWORD, payload: {email : email} });
    }).catch(error => {
      dispatch({ type: ASYNC_END});
      dispatch({type: RECOVER_PASSWORD, payload: {error: error} });
    })
  }
}

export default {
    updateAuthField,
    login,
    onLoginUnload,
    logout,
    forgotPassword,
    loginWithProvider,
    sendEmailVerification,
}
