// Common action

import {
  OPEN_MODAL,
  CLOSE_MODAL,
  ASYNC_START,
  ASYNC_END,
  REDIRECT_TO,
  OPEN_USER_SNACKBAR,
  CLOSE_USER_SNACKBAR,
} from '../constants/actionTypes';
import GA from '../utils/GoogleAnalytics'

const openModal = (modal) => {
  return (dispatch) => {
    GA.logModalView(modal);
    return dispatch({ type: OPEN_MODAL, payload: modal })
  }
}

const closeModal = () => {
  return (dispatch) => {
    return dispatch({ type: CLOSE_MODAL })
  }
}

const asyncStart = () => {
  return (dispatch) => {
    return dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }})
  }
}

const asyncEnd = () => {
  return (dispatch) => {
    return dispatch({ type: ASYNC_END })
  }
}

const redirectTo = (path) => {
  return (dispatch) => {
    return dispatch({ type: REDIRECT_TO, payload: path })
  }
}

const openUserSnackbar = (data) => {
  return (dispatch) => {
    return dispatch({ type: OPEN_USER_SNACKBAR, payload: {message: data.message, variant: data.variant} })
  }
}

const closeUserSnackbar = (data) => {
  return (dispatch) => {
      return dispatch({ type: CLOSE_USER_SNACKBAR})
  }
}

export default {
    openModal,
    closeModal,
    asyncStart,
    asyncEnd,
    redirectTo,
    openUserSnackbar,
    closeUserSnackbar,
}
