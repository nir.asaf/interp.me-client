// General configuration action

import { functions } from '../firebase';

import {
  REVIEW_CREATE,
  REVIEWS_LIST,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const reviewsCreate = (data) => {
  return (dispatch) => {
    functions.reviewsCreate(data).then(payload => {
      dispatch({ type: REVIEW_CREATE, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const reviewsList = (data) => {
  return (dispatch) => {
    functions.reviewsList(data).then(payload => {
      dispatch({ type: REVIEWS_LIST, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({type: REVIEWS_LIST, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

export default {
  reviewsCreate,
  reviewsList
}
