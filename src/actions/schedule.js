// General configuration action

import { functions } from '../firebase';

import {
  SCHEDULE_CREATE,
  SCHEDULE_GET,
  SCHEDULE_GET_OR_CREATE_CALL,
  SCHEDULE_ACCEPT,
  SCHEDULE_REJECT,
  SCHEDULE_UPDATE,
  SCHEDULE_LIST,
  ASYNC_START,
  SCHEDULE_LIST_DEAF,
  SCHEDULE_LIST_INTERPERTER,
  ASYNC_END,
  REDIRECT_TO,
  OPEN_USER_SNACKBAR,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const scheduleCreate = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }})
    functions.scheduleCreate(data).then(payload => {
      dispatch({ type: ASYNC_END })
      dispatch({ type: SCHEDULE_CREATE, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      } else {
        dispatch({ type: REDIRECT_TO, payload: `/schedule/${payload.id}` });
      }
    }).catch(error => {
      dispatch({type: SCHEDULE_CREATE, payload: {error : error} });
      dispatch({ type: ASYNC_END });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const scheduleGet = (data) => {
  console.log('callled')
  return (dispatch) => {
    functions.scheduleGet(data).then(payload => {
      dispatch({ type: SCHEDULE_GET, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleGetOrCreateCall = (data) => {
  return (dispatch) => {
    functions.scheduleGetOrCreateCall(data).then(payload => {
      dispatch({ type: SCHEDULE_GET_OR_CREATE_CALL, payload: payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      } 
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleAccept = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }})
    functions.scheduleAccept(data).then(payload => {
      dispatch({ type: ASYNC_END });
      dispatch({ type: SCHEDULE_ACCEPT, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      } else {
        functions.scheduleGet({scheduleId: payload.id}).then(payload => {
          dispatch({ type: SCHEDULE_GET, payload:  payload });
          if (payload.error) {
            dispatch({ type: HANDLE_ERROR, error: payload.error });
          }
        }).catch(error => {
          dispatch({ type: HANDLE_ERROR, error: error });
        })
      }
    }).catch(error => {
      dispatch({type: SCHEDULE_ACCEPT, payload: {error : error} });
      dispatch({ type: ASYNC_END });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const scheduleReject = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }})
    functions.scheduleReject(data).then(payload => {
      dispatch({ type: ASYNC_END });
      dispatch({ type: SCHEDULE_REJECT, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      } else {
        dispatch({ type: REDIRECT_TO, payload: '/dashbord' })
        dispatch({ type: OPEN_USER_SNACKBAR, payload: {message: 'You reject the schedule call', variant: 'warning'} })
      }
    }).catch(error => {
      dispatch({type: SCHEDULE_CREATE, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleUpdate = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }})
    functions.scheduleUpdate(data).then(payload => {
      dispatch({ type: ASYNC_END });
      dispatch({ type: SCHEDULE_UPDATE, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({type: SCHEDULE_CREATE, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleList = () => {
  return (dispatch) => {
    functions.scheduleUpdate().then(payload => {
      dispatch({ type: SCHEDULE_LIST, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleListDeaf = () => {
  return (dispatch) => {
    functions.scheduleListDeaf().then(payload => {
      dispatch({ type: SCHEDULE_LIST_DEAF, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const scheduleListInterpreter  = () => {
  return (dispatch) => {
    functions.scheduleListInterpreter().then(payload => {
      dispatch({ type: SCHEDULE_LIST_INTERPERTER, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

export default {
  scheduleUpdate,
  scheduleList,
  scheduleReject,
  scheduleGet,
  scheduleGetOrCreateCall,
  scheduleAccept,
  scheduleCreate,
  scheduleListDeaf,
  scheduleListInterpreter
}
