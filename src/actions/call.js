// Calls action
import { functions } from '../firebase';

import {
  CALL_CREATE,
  CALLS_GET,
  CALLS_CONNECT_INTERPRETER,
  CALLS_FINISH,
  CALLS_PAUSE,
  CALLS_START,
  CALLS_NEW_MATCH,
  CALLS_DATA_RESET,
  INTERPRETER_SEARCH,
  ASYNC_START,
  ASYNC_END,
  RESET_TIMER,
  START_TIMER,
  STOP_TIMER,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const createCall = (obj) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: {loadingScreen: true, text: 'Initiating call...'} });
    functions.callsCreate(obj).then((res) => {
      dispatch({ type: CALL_CREATE, payload: res, meta: {mixpanel: {event: "Call Create", props: { type: 'call', call: res}}}});
      dispatch({ type: ASYNC_END});
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: CALL_CREATE, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callsGet = (id) => {
  return (dispatch) => {
    functions.callsGet(id).then((res) => {
      dispatch({ type: CALLS_GET, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: CALLS_GET, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callsFinish = (id) => {
  const callId = {id: id.callId};
  return (dispatch) => {
    functions.callsFinish(id).then((res) => {
      dispatch({ type: CALLS_FINISH, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
      functions.callsGet(callId).then((obj) => {
        dispatch({ type: CALLS_GET, payload: obj });
        if (obj.error) {
          dispatch({ type: HANDLE_ERROR, error: obj.error });
        }
      }).catch(error => {
        dispatch({type: CALLS_GET, payload: {error : error} });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: CALLS_FINISH, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callsConnectInterpreter = (id) => {
  return (dispatch) => {
    functions.callsConnectInterpreter(id).then((res) => {
      dispatch({ type: CALLS_CONNECT_INTERPRETER, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
      functions.callsGet(res).then((obj) => {
        dispatch({ type: CALLS_GET, payload: obj });
        if (obj.error) {
          dispatch({ type: HANDLE_ERROR, error: obj.error });
        }
      }).catch(error => {
        dispatch({type: CALLS_GET, payload: {error : error} });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: CALLS_CONNECT_INTERPRETER, payload: {error : error} });
      dispatch({type: HANDLE_ERROR, error: error});
    })
  }
}

const callsPause = (id) => {
  const callId = {id: id.callId};
  return (dispatch) => {
    dispatch({ type: STOP_TIMER, now: new Date().getTime()});
    functions.callsPause(id).then((res) => {
      dispatch({ type: CALLS_PAUSE, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
      functions.callsGet(callId).then((obj) => {
        dispatch({ type: CALLS_GET, payload: obj });
        if (obj.error) {
          dispatch({ type: HANDLE_ERROR, error: obj.error });
        }
      }).catch(error => {
        dispatch({type: CALLS_GET, payload: {error : error} });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: CALLS_PAUSE, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callsStartSession = (data, baseTime = 0) => {
  const callId = {id: data.callId};
  return (dispatch) => {
    functions.callsStartSession(data).then((res) => {
      dispatch({ type: CALLS_START, payload: res });
      dispatch({ type: START_TIMER, now: new Date().getTime(), baseTime: baseTime});
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
      functions.callsGet(callId).then((obj) => {
        dispatch({ type: CALLS_GET, payload: obj });
        if (obj.error) {
          dispatch({ type: HANDLE_ERROR, error: obj.error });
        }
      }).catch(error => {
        dispatch({type: CALLS_GET, payload: {error : error} });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: CALLS_START, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callsNewMatch = (data) => {
  const callId = {id: data.callId};
  return (dispatch) => {
    dispatch({ type: RESET_TIMER, now: 0});
    functions.callsNewMatch(data).then((res) => {
      dispatch({ type: CALLS_NEW_MATCH, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
      functions.callsGet(callId).then((obj) => {
        dispatch({ type: CALLS_GET, payload: obj });
        if (obj.error) {
          dispatch({ type: HANDLE_ERROR, error: obj.error });
        }
      }).catch(error => {
        dispatch({type: CALLS_GET, payload: {error : error} });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: CALLS_NEW_MATCH, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const interpretersSearch = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: {loadingScreen: true, text: 'Loading interpreters...'} });
    functions.interpretersSearch(data).then((res) => {
      dispatch({type: INTERPRETER_SEARCH, payload: res});
      dispatch({ type: ASYNC_END });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: INTERPRETER_SEARCH, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const callDataReset = () => {
  return (dispatch) => {
    dispatch({ type: CALLS_DATA_RESET});
  }
}

const startTimer = (baseTime = 0) => {
  return (dispatch) => {
    dispatch({ type: START_TIMER, now: new Date().getTime(), baseTime: baseTime,});
  }
}

const stopTimer = () => {
  return (dispatch) => {
    dispatch({ type: STOP_TIMER, now: new Date().getTime()});
  }
}

const resetTimer = () => {
  return (dispatch) => {
    dispatch({ type: RESET_TIMER, now: new Date().getTime()});
  }
}

export default {
  startTimer,
  stopTimer,
  resetTimer,
  createCall,
  callDataReset,
  callsGet,
  callsConnectInterpreter,
  callsFinish,
  callsStartSession,
  callsPause,
  callsNewMatch,
  interpretersSearch,
}
