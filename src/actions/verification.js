// General configuration action

import { functions } from '../firebase';

import {
  VERIFICATION_SEND,
  VERIFICATION_VERIFY,
  ASYNC_START,
  ASYNC_END,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const verificationSend = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }, meta: {mixpanel: {event: "Contact Verification Requested", props: {type: data.type, value: data.value}}}})
    functions.verificationSend(data).then(payload => {
      dispatch({ type: ASYNC_END })
      dispatch({ type: VERIFICATION_SEND, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const verificationVerify = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' }, meta: {mixpanel: {event: "Contact Verification Code Sent"}}})
    functions.verificationVerify(data).then(payload => {
      dispatch({ type: ASYNC_END })
      dispatch({ type: VERIFICATION_VERIFY, payload:  payload , meta: {mixpanel: {event: "Contact Verification Verified Successfully"}}});
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error , meta: {mixpanel: {event: "Contact Verification Invalid!"}}});
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error , meta: {mixpanel: {event: "Contact Verification Invalid!"}}});
    });
  }
}

export default {
    verificationSend,
    verificationVerify,
}
