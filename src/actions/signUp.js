import { auth, functions } from '../firebase';

import {
    SIGNUP_COMPLETED,
    SIGNUP_PAGE_UNLOADED,
    SIGNUP_UPDATE_FIELD,
    SIGNUP_START,
    LOGIN_WITH_PROVIDER,
    GET_CURRENT_USER,
    ASYNC_START,
    ASYNC_END,
    CONFIG_GET_SIGN_UP_PROPERTIES
  } from '../constants/actionTypes';

import {
      PASSWORD,
      FACEBOOK,
      GOOGLE,
    } from '../constants/signupProviders';

const onSignupUnloaded = () => {
    return ({ type: SIGNUP_PAGE_UNLOADED })
}

const updateSignUpField = (key, value) => {
    return ({ type: SIGNUP_UPDATE_FIELD, key, value })
}

const signup = (user, path) => {
  var userPayload = {
    provider: user.provider,
    email: '',
    name: '',
    phone: '',
    zipCode: '',
    photoURL: '',
    gender: '',
    locale: '',
  };
  return (dispatch) => {
    switch (user.provider) {
      case PASSWORD : {
        dispatch({type: SIGNUP_START, payload: {loading: true}, meta: {mixpanel: {event: "SignUp Start", props: {type: 'email'}}}});
        dispatch({ type: ASYNC_START, payload: {loadingScreen: true, text: ''} });
        auth.doCreateUserWithEmailAndPassword(user.email, user.password).then((result) => {
          // all we can get is the email here
          userPayload.email = result.email;
          userPayload.registered = false;
          // send email verificaiton
          functions.configGetSignUpProperties().then(payload => {
            dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });
          });
          auth.doSendEmailVerification().then(() => {
            dispatch({ type: ASYNC_END });
            dispatch({
              type: SIGNUP_COMPLETED,
              payload: userPayload,
              meta: {
                mixpanel: {
                  event: "SignUp Started",
                  props: {
                    type: 'email',
                    userPayload: userPayload
                  }
                }
              }
            });
          }).catch((err) => { throw err }); // catch in next line
        }).catch((error) => {
          dispatch({
            type: SIGNUP_COMPLETED,
            payload: {error: error},
            meta: {
              mixpanel: {
                event: "SignUp Error",
                props: {
                  type: 'email',
                  error: error.message
                }
              }
            }
          });
        })
        break;
      }
      case FACEBOOK : {
        dispatch({type: SIGNUP_START, payload: {loading: false}, meta: {mixpanel: {event: "SignUp Start", props: {type: 'facebook'}}}});
        auth.doSignupWithFacebook().then((result) => {
          functions.usersGetCurrent().then(user => {
            dispatch({ type: GET_CURRENT_USER, payload: {user: user, loading: false} });
            functions.configGetSignUpProperties().then(payload => {
              dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });
            });
            if (user.registered) {
              dispatch({ type: LOGIN_WITH_PROVIDER, payload: {user: user, loading: false, path: path.pathname} });
            } else {
              // get data from facebook login
              userPayload.email = result.user.email;
              userPayload.name = result.user.displayName;
              userPayload.phone = result.user.phoneNumber;
              userPayload.photoURL = result.user.photoURL;
              userPayload.registered = false;
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.gender) {
                switch (result.additionalUserInfo.profile.gender) {
                  case 'male':
                    userPayload.gender = 'm';
                    break;
                  case 'female':
                    userPayload.gender = 'f';
                    break;
                  default:
                    // nothing to do
                }
              }
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.locale) {
                userPayload.locale = result.additionalUserInfo.profile.locale;
              }
              dispatch({
                type: SIGNUP_COMPLETED,
                payload: userPayload,
                meta: {
                  mixpanel: {
                    event: "SignUp Started",
                    props: {
                      type: 'facebook',
                      userPayload: userPayload
                    }
                  }
                }
              });            }
          })
        }).catch((error) => {
          dispatch({
            type: SIGNUP_COMPLETED,
            payload: {error: error},
            meta: {
              mixpanel: {
                event: "SignUp Error",
                props: {
                  type: 'facebook',
                  error: error.message
                }
              }
            }
          });
        })
        break;
      }
      case GOOGLE : {
        dispatch({type: SIGNUP_START, payload: {loading: false}, meta: {mixpanel: {event: "SignUp Start", props: {type: 'google'}}}});
        auth.doSignupWithGoogle().then((result) => {
          functions.usersGetCurrent().then(user => {
            dispatch({ type: GET_CURRENT_USER, payload: {user: user, loading: false} });
            functions.configGetSignUpProperties().then(payload => {
              dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });
            });
            if (user.registered) {
              dispatch({ type: LOGIN_WITH_PROVIDER, payload: {user: user, loading: false, path: path.pathname} });
              functions.configGetSignUpProperties().then(payload => {dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });});
            } else {
              // get data from google login
              userPayload.email = result.user.email;
              userPayload.name = result.user.displayName;
              userPayload.phone = result.user.phoneNumber;
              userPayload.photoURL = result.user.photoURL;
              userPayload.registered = false;
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.gender) {
                switch (result.additionalUserInfo.profile.gender) {
                  case 'male':
                    userPayload.gender = 'm';
                    break;
                  case 'female':
                    userPayload.gender = 'f';
                    break;
                  default:
                    // nothing to do
                }
              }
              if (result.additionalUserInfo && result.additionalUserInfo.profile && result.additionalUserInfo.profile.locale) {
                userPayload.locale = result.additionalUserInfo.profile.locale;
              }
              dispatch({
                type: SIGNUP_COMPLETED,
                payload: userPayload,
                meta: {
                  mixpanel: {
                    event: "SignUp Started",
                    props: {
                      type: 'google',
                      userPayload: userPayload
                    }
                  }
                }
              });
            }
          })
        }).catch((error) => {
          dispatch({
            type: SIGNUP_COMPLETED,
            payload: {error: error},
            meta: {
              mixpanel: {
                event: "SignUp Error",
                props: {
                  type: 'google',
                  error: error.message
                }
              }
            }
          });
        })
        break;
      }
      default:
        // nothing to do
    }
  }
}

export default {
    signup,
    onSignupUnloaded,
    updateSignUpField,
}
