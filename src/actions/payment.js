import { functions } from '../firebase';

import {
  DEAF_GET_CHARGE_TYPES,
  DEAF_BUY_MINUTES_CREDIT,
  INTERPRETERS_WITHDRAW,
  ASYNC_END,
  ASYNC_START,
  RESET_CREDIT,
  DEAF_ADD_PAYMENT,
  DEAF_GET_PAYMENT_IDS,
  DEAF_SELECT_PAYMENT_CARD,
  DEAF_REMOVE_PAYMENT,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const deafGetChargeTypes = () => {
  return (dispatch) => {
    functions.deafGetChargeTypes().then((res) => {
      const types = [];
      res.chargeTypes.map(type => {
        return types[type.order - 1] = type;
      });
      dispatch({ type: DEAF_GET_CHARGE_TYPES, payload: types });
    }).catch(error => {
      dispatch({type: DEAF_GET_CHARGE_TYPES, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const deafBuyMinutesCredit = (data) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' } })
    functions.deafBuyMinutesCredit(data).then((res) => {
      dispatch({ type: DEAF_BUY_MINUTES_CREDIT, payload: res });
      dispatch({ type: ASYNC_END })
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: DEAF_BUY_MINUTES_CREDIT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const interpretersWithdraw = (data) => {
  return (dispatch) => {
    functions.interpretersWithdraw(data).then((res) => {
      dispatch({ type: INTERPRETERS_WITHDRAW, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: INTERPRETERS_WITHDRAW, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const deafAddPayment = (data) => {
  return (dispatch) => {
    functions.deafAddPayment(data).then((res) => {
      dispatch({ type: ASYNC_END})
      dispatch({ type: DEAF_ADD_PAYMENT, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({ type: ASYNC_END})
      dispatch({type: DEAF_ADD_PAYMENT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const deafGetPaymentIds = () => {
  return (dispatch) => {
    functions.deafGetPaymentIds().then((res) => {
      dispatch({ type: DEAF_GET_PAYMENT_IDS, payload: res });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: DEAF_GET_PAYMENT_IDS, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const deafRemovePayment = (data) => {
  return (dispatch) => {
    functions.deafRemovePayment(data).then((res) => {
      dispatch({ type: DEAF_REMOVE_PAYMENT, payload: res, paymentId: data.paymentId });
      if (res.error) {
        dispatch({ type: HANDLE_ERROR, error: res.error });
      }
    }).catch(error => {
      dispatch({type: DEAF_REMOVE_PAYMENT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const resetCredit = () => {
  return (dispatch) => {
    dispatch({ type: RESET_CREDIT});
  }
}

const deafSelectPaymentCard = (data) => {
  return (dispatch) => {
    dispatch({ type: DEAF_SELECT_PAYMENT_CARD, data: data});
  }
}

export default {
    deafRemovePayment,
    deafGetChargeTypes,
    deafGetPaymentIds,
    deafBuyMinutesCredit,
    interpretersWithdraw,
    resetCredit,
    deafAddPayment,
    deafSelectPaymentCard
}
