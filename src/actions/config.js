// General configuration action

import { functions } from '../firebase';

import {
  CONFIG_GET_SIGN_UP_PROPERTIES,
  CONFIG_GET_SPOKEN_LANGUAGES,
  CONFIG_SEND_FEEDBACK,
  HANDLE_ERROR,
} from '../constants/actionTypes';

const configGetSignUpProperties = () => {
  return (dispatch) => {
    functions.configGetSignUpProperties().then(payload => {
      dispatch({ type: CONFIG_GET_SIGN_UP_PROPERTIES, payload:  payload });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const configGetSpokenLanguages = () => {
  return (dispatch) => {
    functions.configGetSpokenLanguages().then(payload => {
      dispatch({ type: CONFIG_GET_SPOKEN_LANGUAGES, payload: payload.data });
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

const configSendFeedback = (data) => {
  return (dispatch) => {
    functions.configSendFeedback(data).then(payload => {
      dispatch({type: CONFIG_SEND_FEEDBACK, payload})
      if (payload.error) {
        dispatch({ type: HANDLE_ERROR, error: payload.error });
      }
    }).catch(error => {
      dispatch({ type: HANDLE_ERROR, error: error });
    });
  }
}

export default {
    configGetSignUpProperties,
    configGetSpokenLanguages,
    configSendFeedback,
}
