// User action including update user, user detiels, show user

import { functions } from '../firebase';
import { mixpanelPerson } from '../utils/MixpanelUtil';

import {
  COMPLETE_DETAILS,
  COMPLETE_DETAILS_UNLOADED,
  UPDATE_FIELD_AUTH,
  GET_CURRENT_USER,
  DEAF_GET_MINUTES_CREDIT,
  INTERPRETER_GET_PAYOUT_CREDIT,
  USER_EDIT,
  ASYNC_START,
  ASYNC_END,
  DEAF_GET_CALL_HISTORY,
  USERS_GET,
  HANDLE_ERROR,
  } from '../constants/actionTypes';

const updateDetailsField = (key, value) => {
  return ({ type: UPDATE_FIELD_AUTH, key, value })
}

const onCompleteDetailsUnloaded = () => {
    return ({ type: COMPLETE_DETAILS_UNLOADED })
}

const completeDetails = (user) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' } , meta: {mixpanel: {event: "SignUp Submitted", props: {
      userType: user.userType,
      email: user.email,
      name: user.name,
      gender: user.gender,
      phone: user.phone,
      signLanguages : user.signLanguages,
      spokenLanguages : user.spokenLanguages
    }}}});
    functions.usersSignUp(user).then( res => {
      console.log(res);
      dispatch({ type: COMPLETE_DETAILS, payload: {inProgress: true} , meta: {mixpanel: {event: "SignUp Completed Successfully"}}});
      functions.usersGetCurrent().then( user => {
        mixpanelPerson(user);
        dispatch({ type: COMPLETE_DETAILS, payload: {user: user, inProgress: false} });
      }).catch((error) => {
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch((error) => {
      dispatch({ type: COMPLETE_DETAILS, payload: {error: error, inProgress: false} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const usersGetCurrent = () => async (dispatch) =>{
  functions.usersGetCurrent().then((res) => {
    mixpanelPerson(res);
    dispatch({ type: GET_CURRENT_USER, payload: {user: res} });
  }).catch(error => {
    dispatch({ type: HANDLE_ERROR, error: error });
  })
}

const deafGetMinutesCredit = () => {
  return (dispatch) => {
    functions.deafGetMinutesCredit().then((res) => {
      dispatch({ type: DEAF_GET_MINUTES_CREDIT, payload: res.data });
    }).catch(error => {
      dispatch({type: DEAF_GET_MINUTES_CREDIT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const interpretersGetPayoutCredit = () => {
  return (dispatch) => {
    functions.interpretersGetPayoutCredit().then((res) => {
      dispatch({ type: INTERPRETER_GET_PAYOUT_CREDIT, payload: res.data });
    }).catch(error => {
      dispatch({type: INTERPRETER_GET_PAYOUT_CREDIT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const usersEdit = (user) => {
  return (dispatch) => {
    dispatch({ type: ASYNC_START, payload: { loadingScreen: false, text: '' } })
    functions.usersEdit(user).then((res) => {
      dispatch({ type: USER_EDIT, payload: res });
      dispatch({ type: ASYNC_END })
      functions.usersGetCurrent().then((res) => {
        mixpanelPerson(res);
        dispatch({ type: GET_CURRENT_USER, payload: {user: res} });
      }).catch(error => {
        dispatch({type: GET_CURRENT_USER });
        dispatch({ type: HANDLE_ERROR, error: error });
      })
    }).catch(error => {
      dispatch({type: USER_EDIT, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const deafGetCallHistory = (data) => {
  return (dispatch) => {
    functions.deafGetCallHistory(data).then((res) => {
      dispatch({ type: DEAF_GET_CALL_HISTORY, payload: res });
    }).catch(error => {
      dispatch({type: DEAF_GET_CALL_HISTORY, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

const usersGet = (data) => {
  return (dispatch) => {
    functions.usersGet(data).then((res) => {
      dispatch({ type: USERS_GET, payload: res });
    }).catch(error => {
      dispatch({type: USERS_GET, payload: {error : error} });
      dispatch({ type: HANDLE_ERROR, error: error });
    })
  }
}

export default {
    onCompleteDetailsUnloaded,
    updateDetailsField,
    completeDetails,
    usersGetCurrent,
    deafGetMinutesCredit,
    interpretersGetPayoutCredit,
    usersEdit,
    deafGetCallHistory,
    usersGet,
}
