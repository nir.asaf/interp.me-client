import auth from './auth';
import config from './config';
import signUp from './signUp';
import user from './user';
import common from './common';
import call from './call';
import payment from './payment';
import verification from './verification';
import schedule from './schedule';
import review from './review';

export default {
    ...auth,
    ...config,
    ...signUp,
    ...user,
    ...common,
    ...call,
    ...payment,
    ...config,
    ...verification,
    ...schedule,
    ...review,
}
