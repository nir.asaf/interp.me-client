import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import withWidth from '@material-ui/core/withWidth';
import Hidden from '@material-ui/core/Hidden';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Countdown from '../components/Common/Countdown';
import Typography from '@material-ui/core/Typography';
import AddToCalendar from 'react-add-to-calendar';
import moment from 'moment';
import withAuthorization from '../components/Session/withAuthorization';
import actions from '../actions';
import ScheduleViewDeaf from '../components/Schedule/ScheduleViewDeaf';
import ScheduleInfo from '../components/Schedule/ScheduleInfo';
import ScheduleViewInterpreter from '../components/Schedule/ScheduleViewInterpreter';

const styles = theme => ({
  root: {
    flexGrow: 1,
    maxWidth: '960px',
    justifyContent: 'center',
    display: 'flex',
    margin: '0 auto',
    [theme.breakpoints.down('sm')]: {
      padding: '1rem',
    },
  },
  wrapper: {
    marginTop: '2rem',
    marginBottom: '2rem',
  },
  text: {
    textAlign: 'center',
    marginTop: '1rem',
  },
  paper: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  enable: {
    pointerEvents: 'none',
    opacity: '0.4',
  },
  statusActionWrap: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
    },
  },
  detailsWrap: {
    [theme.breakpoints.down('sm')]: {
      order: 2,
    },
  },
  countdown: {
    [theme.breakpoints.down('sm')]: {
      marginTop: '0',
    },
  },
  addToCalendar: {
    position: 'fixed',
    bottom: '0.5rem',
    right: '0.5rem',
    zIndex: '9'
  },
  disabled: {
    color: "#333",
  },
});

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  authUser: state.session.authUser,
  schedule: state.schedule.scheduleItem ? state.schedule.scheduleItem : null,
  inProgress: state.common.inProgress,
  signLanguages: state.common.config && state.common.config.signLanguages ? state.common.config.signLanguages.map(suggestion => ({
    value: suggestion.id,
    label: suggestion.name,
  })) : [],
  spokenLanguages: state.common.config && state.common.config.spokenLanguages ? state.common.config.spokenLanguages.map(suggestion => ({
    value: suggestion.id,
    label: suggestion.name,
  })) : [],
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const LinkTo = props => <Link to={props.to} {...props} />

class ScheduleCallStatusPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: null,
      time: null,
      date: null,
      err: '',
      gender: '',
      eventToCalendar: '',
      spokenLanguage: null,
      datePassed: false,
      enableStartCall: false,
    }
  }

  componentDidMount() {
    var scheduleId = this.props.match.params.id;
    this.props.dispatch(actions.scheduleGet({scheduleId: scheduleId}));
  }

  componentWillUnmount() {
    this.stop();
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedule && !nextProps.schedule.error) {
      var schedule = nextProps.schedule;
      const minutes = moment(schedule.startTime.timestamp).diff(moment(), 'minutes');
      if ( minutes <= 10) {
        this.setState({enableStartCall : true});
        if (!schedule.callId) this.props.dispatch(actions.scheduleGetOrCreateCall({scheduleId: schedule.scheduleId}));
      }
      var dateAndTime = schedule.startTime && schedule.startTime.timestamp ? new Date(schedule.startTime.timestamp) : new Date();
      if (moment(dateAndTime).isBefore(moment())) {
        this.setState({datePassed: true});
      }
      if (this.props.currentUser.userType === 'interpreter') {
        this.setState({spokenLanguage: schedule.spokenLanguage})
      } else {
        this.setState({spokenLanguage: schedule.interpreterMatch.spokenLanguage})
      }
      this.setState({
        schedule: schedule,
        time: moment(schedule.startTime.timestamp).format("HH:mm"),
        date: moment(schedule.startTime.timestamp).format("Y-MM-D"),
        eventToCalendar: {
          title: 'Interp.me',
          location: 'Video',
          description: '',
          startTime: schedule.startTime.timestamp ? dateAndTime : new Date(),
          endTime: schedule.startTime.timestamp ? dateAndTime.setMinutes(dateAndTime.getMinutes() + schedule.durationMins) : new Date(),
        }
      })
    } else if(nextProps.schedule && nextProps.schedule.error){
      this.setState({err: "Something went wrong, try to create a new schedule call"})
    }
  }

  handleChangeDateAndTime = data => {
    this.setState({schedule: data.schedule, time: data.time, date: data.date})
  }

  handleSchedule = data => {
    this.setState({schedule: data})
  }

  updateScheduleCall = data => {
    this.props.dispatch(actions.scheduleUpdate(data));
  }

  reject = data => {
    this.props.dispatch(actions.scheduleReject(data));
  }

  accept = data => {
    this.props.dispatch(actions.scheduleAccept(data));
  }

  render() {
    const { classes, spokenLanguages, signLanguages, width, currentUser, inProgress } = this.props;
    const { schedule, enableStartCall, eventToCalendar, err, time, spokenLanguage, datePassed} = this.state;

    return (
      <Grid container className={classes.root}>
        { schedule ?
          <Grid container spacing={24}>
            <Hidden smUp>
              <Button variant="fab" className={classes.addToCalendar}>
                <AddToCalendar buttonLabel={""} event={eventToCalendar} displayItemIcons={false} buttonTemplate={{ 'calendar-plus-o': 'left' }}/>
              </Button>
            </Hidden>
            <Grid item xs={12} sm={12}>
              <Typography variant='headline' align="center" className={classes.text}>Call Schedule</Typography>
            </Grid>
            <Grid item xs={12} sm={12}><h5 align="center">{ moment(schedule.startTime.timestamp).calendar() }</h5></Grid>
            {enableStartCall && schedule.callId &&
              <Grid item xs={12} sm={12} style={{textAlign: 'center'}}>
                <Button  variant="contained" color="primary" component={LinkTo} to={`/call/${schedule.callId}`}>Start Call</Button>
              </Grid>
            }

            {/* Schedule infomration */}
            <ScheduleInfo spokenLanguages={spokenLanguages} signLanguages={signLanguages} inProgress={inProgress}
              currentUser={currentUser} schedule={schedule} time={time} spokenLanguage={spokenLanguage} datePassed={datePassed}
              updateScheduleCall={this.updateScheduleCall} handleSchedule={this.handleSchedule}
              handleChangeDateAndTime={this.handleChangeDateAndTime} />

            <Grid item xs={12} sm={4} className={classes.statusActionWrap}>
              <Grid container spacing={16}>
                <Grid item xs={12} sm={12}>
                  {currentUser && currentUser.userType === 'interpreter' ?
                    <ScheduleViewInterpreter schedule={schedule} currentUser={currentUser} inProgress={inProgress}
                      accept={this.accept} reject={this.reject}/>
                    :
                    <ScheduleViewDeaf schedule={schedule} />
                  }
                </Grid>
                <Grid item xs={12} sm={12} className={classes.countdown}>
                  <Paper elevation={1} className={classes.paper}>
                    <h3>Call will start in</h3>
                    <Countdown date={schedule.startTime.timestamp}/>
                  </Paper>
                </Grid>
                <Hidden smDown>
                  <Grid item xs={12} sm={12}>
                    <Button variant="contained" style={{width: '100%'}}>
                      <AddToCalendar buttonLabel={width === 'xs' ? "" : "Add To Calendar"} event={eventToCalendar} displayItemIcons={false} buttonTemplate={{ 'calendar-plus-o': 'left' }}/>
                    </Button>
                  </Grid>
                </Hidden>
              </Grid>
            </Grid>
          </Grid>
          :
          <Grid item xs={12} sm={12} style={{textAlign: 'center', marginTop: '1rem'}}>
            {err ?
              <h3>{err}</h3>
              :
              <div><CircularProgress/><h6>Loading Schedule Data</h6></div>
            }
          </Grid>
        }
      </Grid>
    );
  }
}

ScheduleCallStatusPage.propTypes = {
  classes: PropTypes.object.isRequired,
  match: PropTypes.object,
  width: PropTypes.string.isRequired,
};

const authCondition = (authUser) => !!authUser;

export default compose(withAuthorization(authCondition), withRouter, connect(mapStateToProps, mapDispatchToProps), withStyles(styles), withWidth())(ScheduleCallStatusPage);
