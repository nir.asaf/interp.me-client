import React from 'react';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import withAuthorization from '../components/Session/withAuthorization';
import { compose } from 'recompose';
import {Link} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import CallViewDeaf from '../components/Call/CallViewDeaf';
import CallViewInterpreter from '../components/Call/CallViewInterpreter';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import actions from '../actions';

const styles = theme => ({
  callPage: {
    height: '100%',
    width: '100%',
    backgroundColor: '#000',
    position: 'fixed',
    top: '0',
  },
  logo: {
    position: 'absolute',
    bottom: '0rem',
    left: '0.4rem',
    color: '#fff',
  },
  error: {
    color: '#fff',
  },
  textCenter: {
    textAlign: 'center',
  },
  loading: {
    overflow: 'hidden',
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    textAlign: 'center',
  }
})

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  authUser: state.session.authUser,
  callData: state.call.data,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class Call extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      roomName: '',
      readyForCall: false,
      err: '',
    };
  }

  async getReadyForCall() {
    if (!this.props.callData) {
      const id = this.props.match.params.id ? this.props.match.params.id : null;
      await this.props.dispatch(actions.callsGet({id: id}));
    }
  }

  componentDidMount () {
    this.getReadyForCall()
  }
  componentWillUnmount() {
    // if (this.props.callData && this.props.currentUser.userType === 'deaf') {
    //   // Pause the call if call state isn't finish in case deaf user get out of the room
    //   if (this.props.callData.state !== "finished") this.props.dispatch(actions.callsPause({id: this.props.callData.id}));
    // }
    // if (this.props.callData && this.props.currentUser.userType === 'interpreter') {
    //   const date = {callId: this.props.callData.id, interpreterMatch: this.props.callData.interpreterMatch}
    //   if (this.props.callData.state !== "finished") this.props.dispatch(actions.callsNewMatch(date));
    // }
    this.props.dispatch(actions.resetTimer());
    this.props.dispatch(actions.callDataReset());
    this.setState({err: '', readyForCall: false, roomName: ''});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.callData) {
      this.setState({readyForCall: true})
    }
    if (nextProps.callData === null && this.state.readyForCall ) {
      this.setState({err: "Oops! something went wrong, try to create a new call"})
    }
  }

  render() {
    const {currentUser, callData, classes} = this.props;
    const {readyForCall, err} = this.state;

    return (
      <div className={classes.callPage}>
        <div className={classes.logo}>interp.<span style={{color: '#0f7f9c'}}>me</span> </div>

          { readyForCall && callData  && currentUser ?
              currentUser.userType === 'deaf' ?
                <div>
                  <CallViewDeaf currentUser={currentUser} callData={callData}/>
                </div>
              :
              currentUser.userType === 'interpreter' ?
                <CallViewInterpreter currentUser={currentUser} callData={callData}/>
              :
                <h1 className={classes.textCenter}>Something wrong .....</h1>
            :
            <div className={classes.textCenter}>
              {this.state.err ?
                <div className={classes.error}>
                  <h5>{err}</h5>
                  <Button><Link to="/dashboard">Go To Dashboard</Link></Button>
                </div>
                :
                <div className={classes.loading}><CircularProgress/></div>
              }
            </div>
          }
      </div>

    );
  }
}

Call.propTypes = {
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser) => !!authUser;
export default compose(withAuthorization(authCondition), withStyles(styles), withRouter, connect(mapStateToProps, mapDispatchToProps))(Call);
