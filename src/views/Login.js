import { Link } from 'react-router-dom';
import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import LockIcon from '@material-ui/icons/Lock';
import TextField from '@material-ui/core/TextField';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import LoadingButton from '../components/Common/LoadingButton';
import actions from '../actions';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import EmailIcon from '@material-ui/icons/Email';
import { compose } from 'recompose';
import { FACEBOOK, GOOGLE, PASSWORD, } from '../constants/signupProviders';
import { sendMixpanelEvent } from '../utils/MixpanelUtil';

const styles = theme => ({
  authPage: {
    height: '100%',
    padding: '1rem',
    background: 'linear-gradient(45deg, #307f9c 30%, #c3dff9 90%)',
  },
  paper: {
    height: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '450px',
    width: '100%',
    padding: '1rem',
    marginTop: '1rem',
  },
  modalFooter: {
    paddingTop: '1rem',
    float: 'right',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3,
  },
  textField: {
    flexBasis: 200,
    marginBottom: '1.4rem',
  },
  loginButton: {
    [theme.breakpoints.up('sm')]: {
      width: '100%',
    }
  }
});

const mapStateToProps = state => ({
  ...state.auth,
  ...state,
  authUser: state.session.authUser,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      err: '',
      provider: '',
      openDialog: false,
      showPassword: false,
    }
  }

  componentDidMount() {
    this.props.dispatch(sendMixpanelEvent('Login Page showed'));
  }

  componentWillMount() {
    console.log('current user  :: ', this.props.currentUser);
    if (this.props.currentUser) {
      let redirect = this.props.currentUser.registered  ? '/dashboard' : '/complete_details';
      this.props.history.push(redirect);
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors && nextProps.errors !== this.props.errors) {
      if (nextProps.errors.code === '400') {
        this.setState({openDialog: true, provider: PASSWORD});
      } else {
        this.setState({
          err: nextProps.errors.message
        })
      }
    }
  }

  signUp = user => ev => {
    const pathName = this.props.location.state ? this.props.location.state.from : {pathname: '/dashboard'};
    ev.preventDefault();
    if(this.validate(user)) {
      this.props.dispatch(actions.signup(user, pathName));
      this.setState({openDialog: false, provider: '', err: ''});
    }
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({[name] : value, err: ''})
    this.props.dispatch(actions.updateAuthField(name, value))
  }

  submitForm = (user) => ev => {
    ev.preventDefault();
    const pathName = this.props.location.state ? this.props.location.state.from : {pathname: '/dashboard'};
    if (this.state.provider === '') {
      if(this.validate(user)) {
        this.props.dispatch(actions.login(user.email, user.password, pathName));
      }
    } else {
      this.props.dispatch(actions.loginWithProvider(user, pathName));
    }
  };

  setError = (err) => {
    this.setState({err})
  }

  validate = (user) => {
    const {  password='', email='', provider='' } = user;
    if (provider === '') {
      if(!email || !email.trim()) {
        this.setError('Email is Required');
        return false;
      }
      if(email) {
	      let emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	      let res = emailReg.test(email);
	      if(!res){
          this.setError('Please enter correct email');
	      	return false;
	      }
	    }
      if(!password || !password.trim()){
        this.setError('Password is Required');
        return false;
      }
    }
    return true;
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  render() {
    const { email='', password='', err='', provider='' } = this.state;
    const user = {email, password, provider};
    const { classes } = this.props;
    return (
      <div className={classes.authPage}>
        <Grid container spacing={8}>
          {this.props.inProgress && (<Grid item xs={12} sm={12} style={{textAlign: 'center'}}><CircularProgress size={25} thickness={3} /><h4>Logging in, please wait...</h4></Grid>)}
          <Grid item xs={12} sm={12}><h1 style={{textAlign: 'center'}}>Sign In</h1></Grid>
          <Grid item xs={12} sm={12} style={{height: '100%', display: 'flex'}}>
            <Paper elevation={2} className={classes.paper} style={this.props.inProgress ? {opacity: '.5', pointerEvents: 'none'} : null}>
              {err && (<div className="col-sm-12 alert alert-danger" role="alert" style={{marginTop: 10}}>{err}</div>)}
              <form onSubmit={this.submitForm(user)}>
                <Grid item xs={12} sm={12}>
                  <TextField type="email" className={classes.textField} label="Email" value={email} onChange={this.onChange} name="email" fullWidth
                  InputProps={{startAdornment: (<InputAdornment position="start"><EmailIcon /></InputAdornment>),}}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth className={classes.textField}>
                    <InputLabel htmlFor="adornment-password">Password</InputLabel>
                    <Input
                      id="adornment-password"
                      type={this.state.showPassword ? 'text' : 'password'}
                      value={password}
                      name="password"
                      onChange={this.onChange}
                      startAdornment={
                        <InputAdornment position="start">
                          <LockIcon />
                        </InputAdornment>
                      }
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12} style={{textAlign: 'right'}}>
                  <LoadingButton type="submit" style={{margin: 0}} variant={"contained"} disabled={this.props.inProgress} onClick={() => this.setState({ provider: ''})} label='Login'/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <p> or Login with:</p>
                  <div>
                    <Button type="submit" id="facebookSubmit" variant="outlined" disabled={this.props.inProgress} style={{marginRight: '0.2rem'}} onClick={() => this.setState({ provider: FACEBOOK})}>
                      <i className="fa fa-facebook blue-text text-center"></i>
                    </Button>
                    <Button type="submit" id="googleSubmit" variant="outlined" disabled={this.props.inProgress} onClick={() => this.setState({ provider: GOOGLE})}>
                      <i className="fa fa-google-plus blue-text text-center"></i>
                    </Button>
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <Divider style={{marginTop: '2rem', backgroundColor: '#2996f3'}}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div className={classes.modalFooter}>
                    <p>Not a member? <Link to="/signup">Sign Up</Link></p>
                    <p>Forgot <Link to="/forgot">Password?</Link></p>
                  </div>
                </Grid>
            </form>
          </Paper>
        </Grid>
      </Grid>
        <Dialog modal="true" open={this.state.openDialog}>
          <DialogContent>
            <DialogContentText>
              The email entered {this.state.email} does not exist in our system. would you like to sign up as a new user with this email and password instead?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={() => this.setState({ openDialog: false, err: '', provider: ''})}>No</Button>
            <Button color="primary" onClick={this.signUp(user)}>Yes</Button>
          </DialogActions>
        </Dialog>

      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(Login);
//
