import Header from '../components/Header/Header';
import Footer from '../components/Footer';
import React from 'react';
import { connect } from 'react-redux';
import { APP_LOAD, REDIRECT } from '../constants/actionTypes';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Login from './Login';
import Business from './Business';
import SignUp from './SignUp';
import Dashboard from './Dashboard';
import Admin from './Admin';
import Interpreters from './Interpreters';
import Settings from './Settings';
import ScheduleCall from './ScheduleCall';
import ScheduleCallStatusPage from './ScheduleCallStatusPage';
import Call from './Call';
import CompleteDetails from './CompleteDetails';
import Forgot from './Forgot';
import Profile from './Profile';
import UserAlert from '../components/Common/UserAlert'
import UserSnackbar from '../components/Common/UserSnackbar'
import NotFound from '../components/Common/NotFound'
import withAuthentication from '../components/Session/withAuthentication';
import { store } from '../store';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import CircularProgress from '@material-ui/core/CircularProgress';
import actions from '../actions';
import LoadingScreen from '../components/Common/LoadingScreen';

import GA from '../utils/GoogleAnalytics'

import './css/style.css';
import './css/react-rater.css';

const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo,
    env: state.common.env,
    authUser: state.session.authUser,
    loadingScreen: state.common.loadingScreen,
    text: state.common.text,
  }};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload) =>
    dispatch({ type: APP_LOAD, payload, skipTracking: true,}),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
  dispatch
});


class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      // this.context.router.replace(nextProps.redirectTo);
      console.log('app.js', nextProps.redirectTo);
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
    // if (nextProps.currentUser === undefined && nextProps.authUser) {
    //   this.props.dispatch(actions.usersGetCurrent());
    // }
  }

  componentDidMount() {
    var currentUser = window.localStorage.getItem('currentUser');
    var config = window.localStorage.getItem('config');
    // load currentUser if not loaded
    if (currentUser === "undefined" && this.props.authUser){
      this.props.dispatch(actions.usersGetCurrent());
      if (config) {
        config = JSON.parse(config);
      } else {config = []}
      this.props.onLoad({config: config});
      return;
    }

    // parse currentUser if exist
    if (currentUser && currentUser !== "undefined") {
      const user = JSON.parse(currentUser);
      if (config) {
        config = JSON.parse(config);
      } else {
        config = [];
        this.props.dispatch(actions.configGetSignUpProperties());
      }
      this.props.onLoad({currentUser: user, config: config});
      return;
    }

    this.props.onLoad();
    if (this.props.authUser === null) {
      return;
    }

    // invalid state - force logout
    this.props.dispatch(actions.logout());
  }

  onLogout = () => {
    this.props.dispatch(actions.logout());
  }

  render() {
    // let { currentUser } = this.props;
    const isCall = window.location.pathname.split('/')[1] === 'call';
    // const PrivateRoute = ({component: Component, ...rest }) => (
    //   <Route {...rest} render={(props) => (
    //     currentUser
    //       ? <Component {...props} />
    //       : <Redirect to='/login' />
    //   )} />
    // );
    return (
      <React.Fragment>

      { this.props.appLoaded ?
        <React.Fragment>
          <UserSnackbar/>
          {this.props.loadingScreen && <LoadingScreen text={this.props.text}/>}
          { !isCall && <Header appName={this.props.appName} currentUser={this.props.currentUser} onLogout={this.onLogout} authUser={this.props.authUser}/>}
          <UserAlert/>
          { GA.init() && <GA.RouteTracker /> }
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/complete_details" component={CompleteDetails} />
            <Route path="/signup" component={SignUp} />
            <Route path="/settings" component={Settings} />
            <Route path="/forgot" component={Forgot} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/interpreters" component={Interpreters} />
            <Route path="/schedule" exact component={ScheduleCall} />
            <Route path="/schedule/:id" exact component={ScheduleCallStatusPage}/>
            <Route path="/call/:id" component={Call} />
            <Route path="/profile/:uid" component={Profile} />
            <Route path="/business" component={Business} />
            <Route path="/admin" component={Admin} />
            <Redirect from="*" to="/login" handler={NotFound}/>
          </Switch>
          <Footer/>
        </React.Fragment>
        :
        <div>
          <Header
            appName={this.props.appName}
            currentUser={this.props.currentUser}
            onLogout={this.onLogout}
          />
          <div><CircularProgress/></div>
        </div>

      }

    </React.Fragment>
  );
  }
}

// App.contextTypes = {
//   router: PropTypes.object.isRequired
// };

export default compose(withAuthentication ,withRouter, connect(mapStateToProps, mapDispatchToProps))(App);
