import React from 'react';
import { connect } from 'react-redux';
import TableComponent from '../components/Common/TableComponent';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import withAuthorization from '../components/Session/withAuthorization';
import StartACallDialog from '../components/Call/StartACallDialog';
import PaymentDialog from '../components/Payment/PaymentDialog';
import WithdrawDialog from '../components/Payment/WithdrawDialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import CurrentBalance from '../components/User/CurrentBalance';
import UserRating from '../components/Common/UserRating';
import MessageDialog from '../components/Common/MessageDialog';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import helpers from '../utils/Helpers';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import { compose } from 'recompose';
import actions from '../actions';

const styles = theme => ({
  dashboardPage: {
    maxWidth: '970px',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  card: {
    padding: 30,
  },
  button: {
    width: '100%',
  },
  interpreterMessage: {
    marginTop: '1rem',
    background: '#e6f0f9',
    padding: '16px',
    lineHeight: '22px',
  },
  browserPanel: {
    background: 'transparent',
    boxShadow: 'none',
  },
  browserPanelEpanded: {
    padding: 0,
    flexDirection: 'column',
  },
  browserPanelSummary: {
    padding: 0,
    [theme.breakpoints.down('sm')]: {
      paddingRight: 4,
    },
  },
  gridWrap: {
    marginTop: '2rem',
    marginBottom: '2rem',
    [theme.breakpoints.down('sm')]: {
      padding: '10px',
    }
  },
});

const mapStateToProps = state => ({
  ...state,
  appName: state.common.appName,
  browserInfo: state.common.browserInfo,
  env: state.common.env,
  authUser: state.session.authUser,
  currentUser: state.common.currentUser,
  model: state.common.model,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const LinkTo = props => <Link to={props.to} {...props} />;

const callHistoryTableCells = [
  {id: 'time', label: 'Time'},
  {id: 'interpreter', label: 'Interpreter'},
  {id: 'duration', label: 'Duration'}
];
const scheduleListTableCells = [
  {id: 'startTime', label: 'Date & Time'},
  {id: 'duration', label: 'Duration in Mins'},
  {id: 'actions', label: 'Actions'}
];

const DeafView = props => {
  const {classes, currentUser} = props;

  return (
    <Grid container className={classes.deaf}>
      <Grid item xs={12} sm={12}>
        <Card className={classes.card}>
          <Grid container>
            <Grid item xs={12} sm={8}>
              <h2>Hello {currentUser.name ? currentUser.name : null}</h2>
              {/*<h2>Your Balance: <CurrentBalance currentUser={currentUser}/><span className="pl-1">minutes</span></h2>
                <Button className="float-left"  variant="outlined"  onClick={() => props.dispatch(actions.openModal('PaymentDialog'))}>Buy More Minutes</Button>*/}
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button style={{width: '100%', marginBottom: '1rem', marginTop: '1rem'}} variant="contained" onClick={() => props.dispatch(actions.openModal('StartACallDialog'))}>Start a Call</Button>
              <Button component={LinkTo} style={{width: '100%'}} variant="contained" to="/schedule">Schedule a Call</Button>
            </Grid>
          </Grid>
        </Card>
        <Grid item xs={12} sm={12} className={classes.gridWrap}>
          <TableComponent cells={scheduleListTableCells} type="scheduleList"/>
        </Grid>
        <Grid item xs={12} sm={12} className={classes.gridWrap}>
          <TableComponent cells={callHistoryTableCells} type="callHistory"/>
        </Grid>
      </Grid>
      {props.model && <PaymentDialog/> }
      {props.model &&  <StartACallDialog  currentUser={currentUser}/>}
    </Grid>
  )
}

const InterpreterView = props => {
  const {currentUser, classes} = props;
  // const tableCells = ['Time', 'Deaf', 'Duration', 'Payment', 'actions'];
  return (
    <Grid container className={classes.interpreter}>
      <Grid item xs={12} sm={12}>
        <Card className={classes.card}>
          <Grid container>
            <Grid item xs={12} sm={8}>
              <div>
                <h2 style={{margin: 0}}>Hello {currentUser.name ? currentUser.name : null}</h2>
                <UserRating showReview={true} reviewCount={currentUser.reviewCount} rating={currentUser.avgRating} style={{display: 'flex'}}/>
              </div>
              <h3>Your Balance: $<CurrentBalance currentUser={currentUser}/></h3>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button variant="contained" color="primary" className={classes.button} onClick={() => props.dispatch(actions.openModal('WithdrawDialog'))}>Withdraw</Button>
            </Grid>
          </Grid>
        </Card>
        <Grid container className={classes.interpreterMessage}>
          <Grid item xs={12} sm={12}>
            <div>
              We will notify you about scheduled and on-demand interpretation requests via Email and SMS to: {currentUser.email} and {currentUser.phone}
              <ExpansionPanel classes={{root: classes.browserPanel,}}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.browserPanelSummary}>
                  <h4>What Browser should I use  when accepting a call?</h4>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.browserPanelEpanded}>
                  <h4 style={{textDecoration: 'underline'}}>Mobile IOS users</h4>
                  <p>To accept interpretation requests please make sure to open the link you receive in Safari.
                  If it doesn't open automatically in Safari Copy the link and Paste it in the Safari browser URL field</p>
                  <h4 style={{textDecoration: 'underline'}}>Android/Windows/Mac users</h4>
                  <p>Please use Chrome or Firefox browsers</p>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </div>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} className={classes.gridWrap}>
          <TableComponent cells={scheduleListTableCells} type="scheduleList"/>
        </Grid>
      </Grid>
      {props.model ? <WithdrawDialog currentUser={currentUser} /> : null }
    </Grid>
  )
}

class Dashboard extends React.Component {

  componentDidMount() {
    if (!helpers.twilioBrowserSupport()) {
      var data = {
        message: `Sorry, at the moment we don't support ${this.props.browserInfo.name} ${this.props.browserInfo.mobile} for video call`,
        variant: "warning",
      }
      this.props.dispatch(actions.openUserSnackbar(data));
    }
  }

  render() {
    const { model, currentUser, classes } = this.props;

    return (
      <div className={classes.dashboardPage}>
        <MessageDialog open={false}/>

          { currentUser ?
              currentUser.userType === 'deaf' ?
                <DeafView model={model} currentUser={currentUser} classes={classes} dispatch={this.props.dispatch} env={this.props.env}/>
              :
              currentUser.userType === 'interpreter' ?
                <InterpreterView model={model} currentUser={currentUser} dispatch={this.props.dispatch} classes={classes}/>
              :
                null
            :
              <div className="text-center" style={{marginTop: '3rem'}}>
                <CircularProgress/>
              </div>
          }
      </div>
    )
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser, currentUser) => !!authUser && currentUser && currentUser.registered;

export default compose(withRouter, withAuthorization(authCondition), connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(Dashboard);
