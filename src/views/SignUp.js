import { Link } from 'react-router-dom';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import LoadingButton from '../components/Common/LoadingButton';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import EmailIcon from '@material-ui/icons/Email';
import actions from '../actions';
import { compose } from 'recompose';
import Paper from '@material-ui/core/Paper';
import { PASSWORD, FACEBOOK, GOOGLE, } from '../constants/signupProviders';
import { sendMixpanelEvent } from '../utils/MixpanelUtil';

const styles = theme => ({
    authPage: {
      height: '100%',
      padding: '1rem',
      background: 'linear-gradient(45deg, #c3dff9 30%, #307f9c 90%)',
    },
    paper: {
      padding: '2rem',
      marginLeft: 'auto',
      marginRight: 'auto',
      maxWidth: '450px',
      marginTop: '1rem',
    },
    modalFooter: {
      paddingTop: '1rem',
    },
    margin: {
      margin: theme.spacing.unit,
    },
    withoutLabel: {
      marginTop: theme.spacing.unit * 3,
    },
    textField: {
      flexBasis: 200,
      marginBottom: '1.4rem',
    },
    loginButton: {
      [theme.breakpoints.up('sm')]: {
        width: '100%',
      }
    }
  });

const mapStateToProps = state => ({
  ...state.signUp,
  ...state,
  authUser: state.session.authUser,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      provider: '',
      err: '',
      isConfirmPassword: false,
    }
  }

  componentDidMount() {
    this.props.dispatch(sendMixpanelEvent('SignUp Page showed'));
  }

  componentWillMount() {
    if (this.props.common && this.props.common.currentUser) {
      console.log('Signup push called');
      this.props.history.push('/');
    }
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
      err: ''
    });
    this.props.dispatch(actions.updateSignUpField(name, value))
    if (name === 'confirmPassword'){
      value !== this.state.password ? this.setState({isConfirmPassword: true}) : this.setState({isConfirmPassword: false});
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors && nextProps.errors !== this.props.errors) {
      this.setState({
        err: nextProps.errors.message
      })
    }
  }

  setError = (err) => {
    this.setState({
      err
    })
  }

  validate = (user) => {
    const { email='', password='', provider='' } = user;
    if (provider === 'password') {
      if(!email || !email.trim()) {
        this.setError('Email is Required');
        return false;
      }
      if(email) {
        let emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        if(!emailReg.test(email)){
          this.setError('Please enter correct email');
          return false;
        }
      }
      if(!password || !password.trim()){
        this.setError('Password is Required');
        return false;
      }
      if(password !== this.state.confirmPassword){
        this.setError('Passwords Do Not Match!');
        return false;
      }
    }
    return true;
  }

  submitForm = (user) => ev => {
    const pathName = this.props.location.state ? this.props.location.state.from : {pathname: '/dashboard'};
    ev.preventDefault();
    if(this.validate(user)) {
      this.props.dispatch(actions.signup(user, pathName));
    }
  }

  render() {
    const { email, password, provider, err='', confirmPassword, isConfirmPassword } = this.state;
    const user = {email, password, provider};
    const { classes } = this.props;

    return (
      <div className={classes.authPage}>
        <Grid container>
          <Grid item xs={12} sm={12}><h1 style={{textAlign: 'center'}}>Sign Up</h1></Grid>
          <Grid item xs={12} sm={12}>
            <Paper elevation={1} className={classes.paper}>
              {err && (<div className="col-sm-12  alert alert-danger" role="alert" style={{marginTop: 20}}>{err}</div>)}
              <form onSubmit={this.submitForm(user)}>
                <Grid item xs={12} sm={12}>
                  <TextField type="email" className={classes.textField} label="Email" value={email} onChange={this.onChange} name="email" fullWidth
                  InputProps={{startAdornment: (<InputAdornment position="start"><EmailIcon /></InputAdornment>),}}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth className={classes.textField}>
                    <InputLabel htmlFor="Password">Password</InputLabel>
                    <Input
                      id="Password"
                      type={this.state.showPassword ? 'text' : 'password'}
                      value={password}
                      name="password"
                      onChange={this.onChange}
                      startAdornment={
                        <InputAdornment position="start">
                          <LockIcon />
                        </InputAdornment>
                      }
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.handleClickShowPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth className={classes.textField}>
                    <InputLabel htmlFor="ConfirmPassword">Confirm Password</InputLabel>
                    <Input
                      id="ConfirmPassword"
                      type='password'
                      error={isConfirmPassword}
                      value={confirmPassword}
                      name="confirmPassword"
                      onChange={this.onChange}
                      startAdornment={
                        <InputAdornment position="start">
                          {confirmPassword === password ? <LockIcon /> : <LockOpenIcon />}
                        </InputAdornment>
                      }
                    />
                  <FormHelperText>{!isConfirmPassword ? '' : 'Passwords Do Not Match!' }</FormHelperText>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12} style={{textAlign: 'right'}}>
                  <LoadingButton type="submit" variant={"contained"} loading={this.props.inProgress} label="Sign up" onClick={() => this.setState({ provider: PASSWORD})}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <p> or Sign up with:</p>
                  <div>
                    <Button type="submit" id="facebookSubmit" variant="outlined" disabled={this.props.inProgress} style={{marginRight: '0.2rem'}}onClick={() => this.setState({ provider: FACEBOOK})}>
                      <i className="fa fa-facebook blue-text text-center"></i>
                    </Button>
                    <Button type="submit" variant="outlined" id="googleSubmit" disabled={this.props.inProgress} onClick={() => this.setState({ provider: GOOGLE})}>
                      <i className="fa fa-google-plus blue-text text-center"></i>
                    </Button>
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <Divider style={{marginTop: '2rem', backgroundColor: '#2996f3'}}/>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div className={classes.modalFooter}>
                    <p>Have a user? <Link to="/login">Login</Link></p>
                  </div>
                </Grid>
              </form>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(SignUp);
