import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import CircularProgress from '@material-ui/core/CircularProgress';
import withAuthorization from '../components/Session/withAuthorization';
import actions from '../actions';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import UserRating from '../components/Common/UserRating';
import Reviews from '../components/User/Reviews';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import {findObjectByValue, findObjectsByValue} from '../utils/Helpers';
import Avatar from '@material-ui/core/Avatar';
import ScheduleDialog from '../components/Schedule/ScheduleDialog';
import Icon from '@material-ui/core/Icon';
import WorkIcon from '@material-ui/icons/Work';
import LanguageIcon from '@material-ui/icons/Language';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import SchoolIcon from '@material-ui/icons/School';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

const styles = theme => ({
  root: {
    flexGrow: 1,
    justifyContent: 'center',
    display: 'flex',
  },
  avatar: {
    width: '100px',
    height: '100px',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  content: {
    flex: '1 0 auto',
    marginBottom: '1rem',
    paddingBottom: 0,
  },
  header: {
    background: 'linear-gradient(45deg, #c3dff9 30%, #307f9c 90%)',
    paddingTop: '1rem',
  },
  headerTop: {
    width: '100%',
  },
  wrapper: {
    width: '100%',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {

    },
  },
  tabContent: {
    maxWidth: '600px',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '1rem',
  },
})

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  profile: state.profile.profile,
  config: state.common.config,
  model: state.common.model,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const DetailsTab = (props) => {
  const {config, profile} = props;
  var experience = config && profile.experience ? findObjectByValue(config.experienceOptions, profile.experience) : null;
  var specialized = config && profile.experience ? findObjectByValue(config.specializationOptions, profile.specialized) : null;
  var spokenLanguages = config && profile.spokenLanguages ? findObjectsByValue(config.spokenLanguages, profile.spokenLanguages) : null;
  var signLanguages = config && profile.signLanguages ? findObjectsByValue(config.signLanguages, profile.signLanguages) : null;
  var aslCertificates = config && profile.aslCertificates ? findObjectsByValue(config.aslCertificates, profile.aslCertificates) : null;
  return(
    <div style={{padding: 10}}>
      <Paper>
        <List>
          {experience && <ListItem><ListItemIcon><SchoolIcon/></ListItemIcon><ListItemText primary="Experience" secondary={experience.name}/></ListItem>}
          {specialized && <ListItem><ListItemIcon><WorkIcon/></ListItemIcon><ListItemText primary="Specialize in" secondary={specialized.name}/></ListItem>}
          {spokenLanguages && <ListItem><ListItemIcon><LanguageIcon/></ListItemIcon><ListItemText primary="Spoken languages" secondary={spokenLanguages.map(spokenLanguage => spokenLanguage.name)}/></ListItem>}
          {signLanguages && <ListItem><ListItemIcon><Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/></ListItemIcon><ListItemText primary="Sign languages" secondary={signLanguages.map(signLanguage => `${signLanguage.name} `)}/></ListItem>}
          {aslCertificates && <ListItem><ListItemIcon><VerifiedUserIcon/></ListItemIcon><ListItemText inset primary="ASL Certificates" secondary={aslCertificates.map(aslCertificate => aslCertificate.name)}/></ListItem>}
        </List>
      </Paper>
    </div>
  )
}

const ReviewsTab = (props) => {

  return (
    <div style={{padding: 10}}>
      <Reviews uid={props.uid}/>
    </div>
  )
}

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      targetUserId: null
    };
  }

  componentDidMount() {
    const targetUserId = this.props.match.params ? this.props.match.params.uid : null
    this.setState({targetUserId: targetUserId});
    this.props.dispatch(actions.usersGet({targetUserId: targetUserId}));
  }

  componentWillMount() {
  }

  componentWillUnmount() {
  }

  handleTabChange = (event, selectedTab) => {
    this.setState({ selectedTab });
  }

  render() {
    const {profile, classes, config, model} = this.props;
    const {selectedTab, targetUserId} = this.state;

    return (
      <div>
      {model && <ScheduleDialog /> }
      {profile ?
        <Grid container>
          <Grid container className={classes.header}>
            <Grid item xs={12} sm={12}>
              <div className={classes.wrapper}>
                <Avatar className={classes.avatar}> {profile.name[0]} </Avatar>
                <CardContent className={classes.content}>
                  <Typography variant="headline">{profile.name}</Typography>
                  <Typography variant="subheading" color="textSecondary"><UserRating value={profile.avgRating} starsSize="12" /></Typography>
                  {profile.bio && <Typography variant="subheading" color="textSecondary">{profile.bio}</Typography>}
                </CardContent>
                <div className={classes.content}>
                  <Button variant="contained" onClick={() => this.props.dispatch(actions.openModal('ScheduleDialog'))}>Schedule a call</Button>
                </div>
              </div>
            </Grid>
            <Grid item xs={12} sm={12} style={{padding: 0}}>
              <Tabs value={selectedTab} onChange={this.handleTabChange} indicatorColor="primary" textColor="primary" centered>
                <Tab label="Details" />
                <Tab label="Reviews" />
              </Tabs>
            </Grid>
          </Grid>
          <Grid container className={classes.tabContent}>
            <Grid item xs={12} sm={12}>
              {selectedTab === 0 && <DetailsTab config={config} profile={profile}/>}
              {selectedTab === 1 && <ReviewsTab uid={targetUserId}/>}
            </Grid>
          </Grid>
        </Grid>
      :
        <Grid item xs={12} sm={12} style={{textAlign: 'center', marginTop: '1rem'}}>
          <div><CircularProgress/><h6>Loading profile</h6></div>
        </Grid>
      }
      </div>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser) => !!authUser;

export default compose(withAuthorization(authCondition), withStyles(styles), withRouter, connect(mapStateToProps, mapDispatchToProps))(Profile);
