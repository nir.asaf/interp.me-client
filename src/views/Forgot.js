import { Link } from 'react-router-dom';
import React from 'react';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import EmailIcon from '@material-ui/icons/Email';
import Grid from '@material-ui/core/Grid';
import LoadingButton from '../components/Common/LoadingButton';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import actions from '../actions';

const styles = theme => ({
 forgetPage: {
   height: '100%',
   padding: '1rem',
   background: 'linear-gradient(20deg, #307f9c 30%, #c3dff9 90%)',
 },
 paper: {
   padding: '2rem',
   marginLeft: 'auto',
   marginRight: 'auto',
   maxWidth: '450px',
   width: '100%',
   marginTop: '1rem',
 },
 gridWrap: {
   marginTop: '1rem',
   marginBottom: '1rem',
 }
})

const mapStateToProps = state => ({
  ...state.auth,
  inProgress: state.common.inProgress,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      err: '',
      emailSent: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.error) {
      if (nextProps.error.code === '"auth/user-not-found"' );
      this.setState({
        err: 'The email does not exist in the system'
      })
    }
    if (nextProps.emailSent) {
      this.setState({
        emailSent: true,
        err: '',
      })
    }
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name] : value,
      err: ''
    })
    this.props.dispatch(actions.updateAuthField(name, value))
  }

  submitForm = (email) => ev => {
    ev.preventDefault();
    if(this.validate({email})) {
      this.props.dispatch(actions.forgotPassword(email));
    }
  };

  validate = (user) => {
    const { email } = user;
    if(!email || !email.trim()) {
      this.setState({err: 'Email is Required'});
      return false;
    }
    if(email) {
      let emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      let res = emailReg.test(email);
      if(!res){
        this.setState({err: 'Please enter correct email'});
      	return false;
      }
    }
    return true;
  }

  render()  {
    const { email='', err='', emailSent } = this.state;
    const {classes} = this.props;
    return (
      <div className={classes.forgetPage}>
        <Grid container>
          <Grid item xs={12} sm={12}><h2 style={{textAlign: 'center'}}>Recover your password</h2></Grid>
          <Paper elevation={2} className={classes.paper}>
            {err && <div className="alert alert-danger" role="alert" style={{marginTop: 10}}>{err}</div>}
            { emailSent ?
              <Grid item xs={12} sm={12}>
                <p>Email sent, please check your Inbox.</p>
                <p>If you don''t see our email, check your Spam folder.</p>
              </Grid>
            :
              <form onSubmit={this.submitForm(email)}>
                <Grid item xs={12} sm={12} className={classes.gridWrap}>
                  <TextField type="email"
                             className={classes.textField}
                             label="Email"
                             value={email}
                             onChange={this.onChange}
                             name="email"
                             fullWidth
                             InputProps={{startAdornment: (<InputAdornment position="start"><EmailIcon /></InputAdornment>),}}/>
                </Grid>
                <Grid item xs={12} sm={12} className={classes.gridWrap}>
                  <LoadingButton type="submit" style={{margin: 0}} variant={"contained"} disabled={this.props.inProgress} loading={this.props.inProgress} label='Recover Password'/>
                </Grid>
                <Grid item xs={12} sm={12} className={classes.gridWrap} style={{textAlign: 'right'}}>
                  <p>Not a member? <Link to="/signup">Sign Up</Link></p>
                </Grid>
              </form>
            }
          </Paper>
        </Grid>
      </div>
    );
  }
}

Forgot.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(Forgot);
