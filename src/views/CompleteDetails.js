import React from 'react';
import { connect } from 'react-redux';
import SelectComponent from '../components/Common/SelectComponent';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import green from '@material-ui/core/colors/green';
import Switch from '@material-ui/core/Switch';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import ReactPhoneInput from 'react-phone-input-2';
import actions from '../actions';
import moment from 'moment';
import LoadingButton from '../components/Common/LoadingButton';
import Countdown from '../components/Common/Countdown';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Countries from '../components/User/Countries'
import SpokenLanguageSelect from '../components/User/SpokenLanguageSelect'
import SignLanguageSelect from '../components/User/SignLanguageSelect'
import Grid from '@material-ui/core/Grid';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import withAuthorization from '../components/Session/withAuthorization';
import withWidth from '@material-ui/core/withWidth';
import { compose } from 'recompose';
import { sendMixpanelEvent } from '../utils/MixpanelUtil';

const mapStateToProps = state => ({
  ...state,
  experienceOptions: state.common.config && !state.common.config.experienceOptions ? [] :
    state.common.experienceOptions.map(experience => {
      return {
      label: experience.name,
      value: experience.id
    }}),
  specializationOptions: state.common.config && !state.common.config.specializationOptions ? [] :
    state.common.specializationOptions.map(specialization => {
      return {
      label: specialization.name,
      value: specialization.id
    }}),
  aslCertificatesList: state.common.config && state.common.config.aslCertificates ? state.common.config.aslCertificates : [],
  authUser: state.session.authUser,
  inProgress: state.common.inProgress,
  verificationId: state.verification.verificationId,
  verificationSuccess: state.verification.verificationSuccess,
  errors: state.common.errors ? state.common.errors : null,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

const styles = theme => ({
  root: {
    flexGrow: 1,
    justifyContent: 'center',
    display: 'flex',
    maxWidth: '600px',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '18px',
    width: '100%',
    marginTop: '1rem',
    [theme.breakpoints.down('sm')]: {
      padding: 0,
    },
  },
  stepper: {
    backgroundColor: 'transparent',
  },
  buttonWrap: {
    textAlign: 'right',
    marginTop: '2rem',
  },
  gridWrap: {
    marginTop: '2rem',
  },
  radioButton: {
    width: 'auto',
    display: 'inline-block',
    marginRight: '2rem',
  },
  radioButtonGroup: {
    textAlign: 'left',
    marginTop: '1.5rem',
  },
  gender: {
    marginTop: '1.5rem',
  },
  autoCompleteWrapper: {
    display: 'inline-block',
    width: '100%',
    position: 'relative',
    marginTop: '1.5rem',
  },
  autoComplete: {
    paddingLeft: '0.6rem',
    width: 'calc(100% - 2rem)',
    float: 'left',
  },
  chip: {
    margin: 4,
    display: 'inline-block',
  },
  chipWrapper: {
    float: 'left',
  },
  verifyButton: {
    fontSize: '12px',
  },
  languagesWrap: {
    marginTop: '1.5rem',
  },
  error: {
    color: '#f44336',
    textAlign: 'center',
    background: '#f8f8f8',
    maxWidth: '400px',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '0.2rem',
    borderRadius: '2%',
  },
  userTypeButton: {
    margin: theme.spacing.unit,
    padding: '10px',
    fontSize: '1.2rem',
    fontWeight: '300',
    marginTop: '1rem',
    textTransform: 'capitalize',
    [theme.breakpoints.only('xs')]: {
      width: '100%',
    },
  },
  check: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: green[500],
  },
});

class CompleteDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: props.authUser && props.authUser.email ? props.authUser.email : '',
      name: props.authUser && props.authUser.displayName ? props.authUser.displayName : '',
      phone: props.authUser && props.authUser.phoneNumber ? props.authUser.phoneNumber : '',
      zipCode: props.authUser && props.authUser.zipCode ? props.authUser.zipCode : '',
      gender: props.authUser && props.authUser.gender ? props.authUser.gender : '',
      bio: '',
      country: null,
      signLanguages: null,
      spokenLanguages: null,
      userType: null,
      selectedFile: null,
      specialized: null,
      experience: null,
      imgSrc: null,
      spokenLanguagesList: props.spokenLanguagesList,
      signLanguagesList: props.signLanguagesList,
      interpreterView: false,
      err: '',
      finished: false,
      callPreferences: {
        instant_free: {enabled: true},
        instant_fixed: {enabled: true},
        scheduled_video: {enabled: true},
      },
      instant_free: true,
      instant_fixed: true,
      scheduled_video: true,
      stepIndex: 0,
      verificationCode: '',
      verificationCodeSent: false,
      phoneVerified: false,
      checkCode: false,
      zipCodeValidator: null,
      aslCertificates: [],
      showAslCertificates: false,
      step: 'userType'
    };

  }

  selectUser = (userType) => {
    var step = (userType === 'deaf') ? 'deaf0' : 'interp0'
    this.changeStep(step, {userType: userType})

    this.props.dispatch(sendMixpanelEvent('SignUp UserType Pressed', {userType: userType}));
  }

  changeStep = (step, stateChange) => {
    window.location.hash = "step=" + step;

    if (stateChange) {
      stateChange.step = step;
      this.setState(stateChange);
    }
    else {
      this.setState({step: step});
    }
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
      err: ''
    });
    this.props.dispatch(actions.updateDetailsField(name, value))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user && nextProps.user.email) {
      this.setState({
        email: nextProps.user.email
      });
    }
    if(nextProps.errors && nextProps.errors !== this.props.errors) {
      this.setError(nextProps.errors.message)
    }
    if(nextProps.verificationSuccess) {
      this.setState({
        err: ''
      })
    }
    if (nextProps.verificationSuccess === false) {
      this.setState({checkCode: false, err: 'The code is incorrect or expired, make sure you enter a valid number and try to resend'})
    }
  }

  setError = (err, state) => {
    this.props.dispatch(sendMixpanelEvent('SignUp Error',
    {
      error : err
    }));
    if (state) {
      state.err = err
      this.setState(state)
    }
    else {
      this.setState({
        err : err
      })
    }
  }

  validate = (user) => {
    if (!user.userType) {
      this.setError('You must select the type of user');
      return false;
    }
    if(!user.name || !user.name.trim()) {
      this.setError('Name is Required');
      return false;
    }

    if(user.signLanguages === undefined || user.signLanguages.length === 0) {
      this.setError('Please select sign languages');
      return false;
    }
    if (user.userType === 'interpreter') {
      if(user.spokenLanguages === undefined || user.spokenLanguages.length === 0) {
        this.setError('Please select spoken languages');
        return false;
      }
    }
    if (user.zipCode && user.country) {
      var regexPattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
      if (this.state.zipCodeValidator) {
        regexPattern = new RegExp(this.state.zipCodeValidator);
      }
      if (!regexPattern.test(user.zipCode)) {
        this.setError('Please enter a valid zip code');
        return false;
      }
    }
    return true;
  }

  submitForm = (user) => ev => {
    ev.preventDefault();
    if (!this.validateByStep(this.state.stepIndex)){return}
    if(this.validate(user)) {
      var notifications = [
        { type: "email", value: this.state.email, verificationId: 'REGISTERED_EMAIL'},
        { type: "phone", value: this.state.phone, verificationId: this.props.verificationId},
      ]
      user["notifications"] = notifications;
      this.props.dispatch(actions.completeDetails(user));
    }
  }

  componentDidMount() {
    try {
      const currentUser = JSON.parse(window.localStorage.getItem('currentUser'));
      if (currentUser) {
        if (currentUser.email) {
          this.setState({email: currentUser.email});
        }
      }
    } catch(error) {
      console.log(error);
      window.location.reload();
    }

  }

  fileSelectedHandler = ev => {
    this.setState({selectedFile: ev.target.files[0]})
    const reader  = new FileReader();
    reader.onloadend = () => {
        this.setState({
            imgSrc: reader.result
        })
    }
    if (ev.target.files[0]) {
        reader.readAsDataURL(ev.target.files[0]);
        this.setState({
            imgSrc :reader.result
        })
    }
    else {
        this.setState({
            imgSrc: null
        })
    }
  }

  validateByStep = (step) => {
    const {name, spokenLanguages, signLanguages, userType, zipCode, country} = this.state;
    if (step === 0 && !name.length) {
      this.setError('Name is Required');
      return false;
    }else if (step === 1 && userType === 'interpreter' && !spokenLanguages){
      this.setError('Spoken Language is Required');
      return false;
    }else if (step === 1 && !signLanguages){
      this.setError('Sign Language is Required');
      return false;
    }
    else if (step === 2 && !this.props.verificationSuccess){
      this.setError('You need to Verify your phone');
      return false;
    }else if (step === 2 && country && !zipCode){
      this.setError('Zip Code is Required');
      return false;
    } else if (step === 2 && country && zipCode){
      var regexPattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
      if (this.state.zipCodeValidator) {
        regexPattern = new RegExp(this.state.zipCodeValidator);
      }
      if (!regexPattern.test(zipCode)) {
        this.setError('Please enter a valid zip code');
        return false;
      }
    }
    return true;
  }

  handleNext = () => {
    const {stepIndex, userType} = this.state;
    if (!this.validateByStep(stepIndex)) return;
    this.setState({err: ''});
    var finishedStep = {interpreter: 3, deaf: 2}
    var newStepIndex = stepIndex + 1;
    var newStep = userType + newStepIndex;
    this.changeStep(newStep, {
      stepIndex: newStepIndex,
      finished: newStepIndex === finishedStep[userType],
    });

    this.props.dispatch(sendMixpanelEvent('SignUp Next Pressed',
    {
      userType: userType,
      newStep: newStep,
      name: this.state.name,
      gender: this.state.gender,
      phone: this.state.phone,
      signLanguages : this.state.signLanguages,
      spokenLanguages : this.state.spokenLanguages
    }));
  };

  handlePrev = () => {
    const {stepIndex, userType} = this.state;
    var newStepIndex = stepIndex - 1;
    var newStep = userType + newStepIndex;
    var finishedStap = {interpreter: 3, deaf: 2}
    var newState = {};
    if (stepIndex === finishedStap[userType]) {
      newState.finished = false;
    }
    if (stepIndex > 0) {
      newState.stepIndex = newStepIndex;
    }
    this.changeStep(newStep, newState);
    this.props.dispatch(sendMixpanelEvent('SignUp Back Pressed',
    {
      userType: userType,
      newStep: newStep,
      name: this.state.name,
      gender: this.state.gender,
      phone: this.state.phone,
      signLanguages : this.state.signLanguages,
      spokenLanguages : this.state.spokenLanguages
    }));
  };

  verificationSend = (phone) => ev => {
    const {userType} = this.state;

    ev.preventDefault();
    if (!this.validatePhone(phone)){
      this.setError('Enter valid number');
      return false;
    }
    this.props.dispatch(actions.verificationSend({type: 'phone', value: phone}));
    this.changeStep(userType + '_validatePhone', {verificationCodeSent: true});
  }

  checkVerificationCode = (code) => ev => {
    const {userType} = this.state;
    this.changeStep(userType + '_verifyCode', {checkCode: true});
    ev.preventDefault();
    if (code && this.props.verificationId) {
      this.props.dispatch(actions.verificationVerify({verificationId: this.props.verificationId, code: code}));
      return;
    }
    return false;
  }

  validatePhone = (phone) => {
    var regexPattern = /^\+(?:[0-9]●?){6,14}[0-9]$/;
    if (!regexPattern.test(phone)) {
      return false;
    }
    return true;
  }

  onAslCertificatesChange = value => event => {
    if (event.target.checked) {
      if (!this.state.aslCertificates.length) {
        this.setState({aslCertificates: [value]})
      } else {
        this.setState({aslCertificates: this.state.aslCertificates.concat([value])})
      }
    } else {
      this.setState({ aslCertificates: this.state.aslCertificates.filter(val => val !== value )})
    }
  }

  callPreferencesChange = name => event => {
    let newCallPreferences = Object.assign({}, this.state.callPreferences);
    newCallPreferences[name].enabled = event.target.checked;
    this.setState({ [name]: event.target.checked, callPreferences:  newCallPreferences});
  };

  getStepContent(stepIndex) {
    const {err, name, userType, showAslCertificates, instant_free, instant_fixed, scheduled_video, spokenLanguages, signLanguages, experience} = this.state;
    const {classes} = this.props;
    switch (stepIndex) {
      case 0:
        return (
          <Grid container spacing={16}>
            <Grid item xs={12} sm={8}>
                <TextField required error={Boolean(err) && !name} fullWidth label="What is your full name" name="name" value={name} onChange={this.onChange}/>
            </Grid>
              {/* <div className="col"><input type = "file" style = {{display: 'none'}} onChange = {this.fileSelectedHandler} ref = {fileInput => this.fileInput = fileInput}/><div>
                  <a onClick={ () => this.fileInput.click()}>{ this.state.selectedFile ? (<img src={this.state.imgSrc} className="z-depth-1" alt="user" style={{border: '1px solid #c9c9c9', height: '120px', padding: '6px'}}></img>) : (<i className="fas fa-user-circle fa-8x"></i>)}</a></div>
                  </div>
              */}
            <Grid item xs={12} sm={12} className={classes.gridWrap} style={{marginTop: '2rem'}}>
              <FormLabel style={{textAlign: 'left'}} component="legend">Gender</FormLabel>
              <RadioGroup name="gender" onChange={this.onChange} value={this.state.gender}>
                <FormControlLabel value="" control={<Radio color="primary"/>} label="Not Specified / Other" />
                <FormControlLabel value="f" control={<Radio color="primary"/>} label="Female" />
                <FormControlLabel value="m" control={<Radio color="primary"/>} label="Male" />

              </RadioGroup>
            </Grid>
            <Grid item xs={12} sm={12} className={this.props.classes.buttonWrap}>
              <Button variant="contained" disabled={stepIndex === 0} style={{marginRight: 12}} onClick={this.handlePrev}>Back</Button>
              <Button variant="contained" color="primary" onClick={this.handleNext}>Next</Button>
            </Grid>
          </Grid>
        )
      case 1:
        return (
          <div className={this.props.classes.languagesWrap}>
            { userType === 'interpreter' && (
              <Grid container spacing={16} style={{marginBottom: '1rem'}}>
                <Grid item xs={12} sm={12}>
                  <SpokenLanguageSelect value={spokenLanguages ? spokenLanguages : []} onChange={val => {
                      let newSpokenLanguages = Object.assign([], spokenLanguages);
                      newSpokenLanguages = val;
                      this.setState({spokenLanguages: newSpokenLanguages});
                    }}/>
                </Grid>
              </Grid>)}
            <Grid container spacing={16}>
              <Grid item xs={12} sm={12} className={classes.gridWrap}>
                <SignLanguageSelect value={signLanguages ? signLanguages : []} onChange={val => {
                    let newSignLanguages = Object.assign([], signLanguages);
                    newSignLanguages = val;
                    this.setState({signLanguages: newSignLanguages});
                    if (userType === 'interpreter') {
                      if (newSignLanguages && !newSignLanguages.includes("mQPtRQBhlg09sH7fvzI2") && !newSignLanguages.includes("eQTaxbYvtNU1fDncJ4ie")){
                          this.setState({showAslCertificates: false})
                      }
                      if (newSignLanguages.includes("mQPtRQBhlg09sH7fvzI2") || newSignLanguages.includes("eQTaxbYvtNU1fDncJ4ie")){
                          this.setState({showAslCertificates: true})
                      }
                    }
                  }}/>
              </Grid>
            </Grid>

            { userType === 'interpreter' && showAslCertificates ?
              <FormGroup row className={this.props.classes.gridWrap}>
                <FormLabel style={{textAlign: 'left'}} component="legend">ASL Certificates:</FormLabel>
                <div>
                  { this.props.aslCertificatesList.map((aslCertificate, index) => {
                    return (<FormControlLabel key={index} control={<Checkbox color="primary" checked={this.state.aslCertificates.includes(aslCertificate.id)} value={aslCertificate.id} onChange={this.onAslCertificatesChange(aslCertificate.id)}/>} label={aslCertificate.name}/> )
                  })}
                </div>
              </FormGroup> : null
            }
            <Grid container spacing={16} justify="flex-end" style={{marginTop: '3rem', textAlign: 'right'}}>
              <Grid item xs={12} sm={12}>
                <Button  disabled={stepIndex === 0} style={{marginRight: 12}} onClick={this.handlePrev}>Back</Button>
                <Button variant="contained" color="primary" onClick={this.handleNext}>Next</Button>
              </Grid>
            </Grid>
          </div>
        )
      case 2:
        return (
          <div className="mt-4">
            <Grid container>
              {this.state.verificationCodeSent && !this.props.verificationSuccess && (
                <Grid item xs={12} sm={12}>
                  <h6>
                    A verification code sent to your phone, check your phone for a code, please wait <Countdown justSec={true} sec={30} date={String(new moment().add(31, 'seconds'))}/> before resend
                  </h6>
              </Grid>)}
              <Grid item xs={12} sm={12}>
                <Grid container spacing={8}>
                 <Grid item xs={8} sm={9}>
                    <ReactPhoneInput placeholder="Enter phone number" name="phone" inputStyle={{width: '100%'}} value={this.state.phone} required={true} defaultCountry={'us'}
                      disabled={this.props.verificationSuccess ? true : false}
                      onChange={phone => { this.setState({phone: phone.replace(/[^0-9.+]/g, ""), err: ''});
                          this.props.dispatch(actions.updateDetailsField("phone", phone.replace(/[^0-9.+]/g, "")));}}
                    />
                 </Grid>
                 <Grid item xs={4} sm={3}>
                    {!(this.props.verificationSuccess) ?
                      <LoadingButton loading={this.props.inProgress && this.props.verificationSuccess}
                                     label={this.state.verificationCodeSent ? "Resend" : "Verify"}
                                     disabled={this.state.phone && this.state.phone.length > 8 ? false : true}
                                     onClick={this.verificationSend(this.state.phone)}
                                     className={this.props.classes.verifyButton}
                                     size="medium"
                                     /> : <div className={classes.check}><CheckCircleIcon/></div>}

                 </Grid>
               </Grid>
              </Grid>
              <Grid item xs={12} sm={12} className={classes.gridWrap}>
                { this.state.verificationCodeSent && !(this.props.verificationSuccess) ?
                    <Grid container spacing={8}>
                      <Grid item xs={12} sm={9}>
                        <TextField
                          label="Enter verification code"
                          name="verificationCode"
                          value={this.state.verificationCode}
                          onChange={this.onChange}
                          style={{float: 'left', width: '100%'}}
                          />
                      </Grid>
                      <Grid item xs={12} sm={3}>
                        <LoadingButton
                          label="Check"
                          size="medium"
                          style={{fontSize: '12px', minwidth: '100%'}}
                          loading={this.state.checkCode}
                          onClick={this.checkVerificationCode(this.state.verificationCode)}
                          disabled={(this.props.verificationId && this.state.verificationCode) ? false : true}/>
                      </Grid>
                    </Grid> : null}
              </Grid>
            </Grid>

            <Grid container spacing={16} className={classes.gridWrap}>
              <Grid item xs={8} sm={8}>
                <Countries value={''} onChange={ obj => this.setState({country: obj.value, zipCodeValidator: obj.zipCodeValidator})}/>
              </Grid>
              <Grid item xs={4} sm={4} style={{paddingTop: '1.3rem'}}>
                <TextField label="Zip code" name="zipCode" disabled={this.state.country ? false : true} style={{width: '100%'}} value={this.state.zipCode} onChange={this.onChange}/>
              </Grid>
            </Grid>
            {this.state.userType === 'interpreter' && (<Grid container spacing={16} justify="flex-end" style={{marginTop: '3rem', textAlign: 'right'}}>
              <Grid item xs={12} sm={12}>
                <Button  disabled={stepIndex === 0} style={{marginRight: 12}} onClick={this.handlePrev}>Back</Button>
                <Button variant="contained" color="primary" onClick={this.handleNext}>Next</Button>
              </Grid>
            </Grid>)}
          </div>
        )
      case 3:
        return (
          <Grid container spacing={16}>
            <Grid item xs={6} sm={6}>
              <h6>Experience</h6>
              <SelectComponent value={experience} isLoading={this.props.experienceOptions ? false : true} suggestions={this.props.experienceOptions} onChange={obj => this.setState({experience: obj.value})} selectType={'single'}/>
            </Grid>
            <Grid item xs={6} sm={6}>
              <h6>Specialized in</h6>
              <SelectComponent value={this.state.specialized} isLoading={this.props.specializationOptions ? false : true} suggestions={this.props.specializationOptions} onChange={obj => this.setState({specialized: obj.value})} selectType={'single'}/>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField fullWidth value={this.state.bio} label="Say a few things about yourself..." multiline rows="5" margin="normal"
                onChange={event => {
                  if (event.target.value.length <= 200) this.setState({bio: event.target.value});
                }}/>
              <FormHelperText style={{textAlign: 'right'}}>{this.state.bio.length}/200</FormHelperText>
            </Grid>
            <Grid item xs={12} sm={12}>
              <h6>Notifications (SMS & Email)</h6>
              <p>You will always have the choice whether to accept or decline a call</p>
              <FormControlLabel control={<Switch color="primary" checked={instant_free} value={"instant_free"} onChange={this.callPreferencesChange("instant_free")}/>} label="Notify me of Pro-Bono calls"/>
              <FormControlLabel disabled={experience === 's0mbzg83b9w8Ed294Ih0' || experience === 'kTa6AtsUqLydPeUJcFTO'} control={<Switch color="primary" checked={instant_fixed} value={"instant_fixed"} onChange={this.callPreferencesChange("instant_fixed")}/>} label="Notify me of On-demand calls"/>
              <FormControlLabel disabled={experience === 's0mbzg83b9w8Ed294Ih0' || experience === 'kTa6AtsUqLydPeUJcFTO'} control={<Switch color="primary" checked={scheduled_video} value={"scheduled_video"} onChange={this.callPreferencesChange("scheduled_video")}/>} label="Notify me of Scheduled calls"/>
            </Grid>
          </Grid>
        )
      default:
      return;
    }
  }

  render() {
    const {classes} = this.props;
    const { finished, callPreferences, bio, experience, specialized, signLanguages, spokenLanguages, stepIndex, country, name, email, phone, aslCertificates, zipCode, userType, gender, err=''} = this.state;
    const user = {
      signLanguages,
      bio,
      experience,
      specialized,
      spokenLanguages,
      name,
      email,
      phone,
      zipCode,
      userType,
      gender,
      country,
      aslCertificates,
      callPreferences,
    };
    return (
      <Grid container spacing={16} className={this.props.classes.root}>
        { !this.state.userType ?
          <div>
            <Grid item xs={12} sm={12}>
              <h2>I am</h2>
            </Grid>
            <Grid item xs={12} sm={12} style={{marginTop: '2rem'}}>
              <Grid justify="center" alignItems="center" direction="row" container spacing={16}>
                <Button variant="outlined" color="primary" className={this.props.classes.userTypeButton} onClick={()=> this.selectUser('deaf')}>Deaf</Button>
                <Button variant="outlined" color="primary" className={this.props.classes.userTypeButton} onClick={()=> this.selectUser('interpreter')}>Interpreter</Button>
              </Grid>
            </Grid>
          </div>
        :
        <div style={{width: '100%'}}>
          <Grid item xs={12} sm={12} className="details-steps">
            <Stepper
              activeStep={stepIndex}
              alternativeLabel
              classes={{
                root: classes.stepper,
              }}>
              <Step>
                <StepLabel>Full name / gender</StepLabel>
              </Step>
              <Step>
                <StepLabel>Languages</StepLabel>
              </Step>
              <Step>
                <StepLabel>Address / Phone</StepLabel>
              </Step>
              { userType === 'interpreter' && (
                <Step>
                  <StepLabel>Preferences</StepLabel>
                </Step>)}
            </Stepper>
          </Grid>
          <Grid item xs={12} sm={12}>
            {err ? <div className={classes.error}>{err}</div> : null}
          </Grid>
          <Grid item xs={12} sm={12} style={{padding: '1.2rem', maxWidth: '450px', marginLeft: 'auto', marginRight: 'auto'}}>
            {this.getStepContent(stepIndex)}
          </Grid>

          {finished && (
            <Grid container spacing={16} justify="flex-end" style={{marginTop: '3rem', textAlign: 'right', padding: '1.2rem'}}>
              <Grid item xs={12} sm={12}>
                <Button  disabled={stepIndex === 0} style={{marginRight: 12}} onClick={this.handlePrev}>Back</Button>
                <LoadingButton size="medium" label='Finish' primary={true} onClick={this.submitForm(user)} loading={this.props.inProgress && this.props.verificationSuccess} />
              </Grid>
            </Grid>
          )}
        </div>
        }
      </Grid>
    );
  }
}

CompleteDetails.propTypes = {
  width: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser, currentUser) => !!authUser && currentUser && !currentUser.registered;

export default compose(withAuthorization(authCondition),connect(mapStateToProps, mapDispatchToProps),withWidth(), withStyles(styles))(CompleteDetails);
