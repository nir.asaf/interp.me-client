import React from 'react';
import { connect } from 'react-redux';

import Hero from '../components/Home/sections/hero';
import Features from '../components/Home/sections/features';
import Team from '../components/Home/sections/team';
import Pricing from '../components/Home/sections/pricing';
import Contact from '../components/Home/sections/contact';
import Banner from '../components/Home/sections/banner';

import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
} from '../constants/actionTypes';

// const Promise = global.Promise;

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName
});

const mapDispatchToProps = dispatch => ({
  onLoad: (pager, payload) =>
    dispatch({ type: HOME_PAGE_LOADED, pager, payload }),
  onUnload: () =>
    dispatch({  type: HOME_PAGE_UNLOADED })
});

class Home extends React.Component {
  componentWillMount() {

  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div className="home-page">
          <Hero/>
          <Features/>
          <Pricing/>
          <Team/>
          <Banner/>
          <Contact/>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
