import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import withAuthorization from '../components/Session/withAuthorization';
import Schedule from '../components/Schedule/Schedule';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';
import { sendMixpanelEvent } from '../utils/MixpanelUtil';

const styles = theme => ({
  root: {
    flexGrow: 1,
    justifyContent: 'center',
    display: 'flex',
  },
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing.unit * 2,
    },
  },
});

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  signLanguages: state.common.config.signLanguages,
  authUser: state.session.authUser,
  inProgress: state.common.inProgress,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});


class ScheduleCall extends React.Component {
  componentDidMount() {
    this.props.dispatch(sendMixpanelEvent('Open Start A Call Dialog'));
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Schedule />
        </Paper>
      </div>
    );
  }
}

ScheduleCall.propTypes = {
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser, currentUser) => !!authUser && currentUser.userType === 'deaf';

export default compose(withAuthorization(authCondition), connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(ScheduleCall);
