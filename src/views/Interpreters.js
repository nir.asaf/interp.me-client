import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import classnames from 'classnames';
import UserRating from '../components/Common/UserRating';
import actions from '../actions';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import withAuthorization from '../components/Session/withAuthorization';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import withWidth from '@material-ui/core/withWidth';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import InterpreterSearch from '../components/InterpreterSearch/InterpreterSearch';
import loadingImage from './../images/02.png';

const styles = theme => ({
  interpretersView: {
    maxWidth: 1100,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: 40,
  },
  card: {
    maxWidth: 400,
    minHeight: 348,
    position: 'relative',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      minHeight: 240,
    }
  },
  actions: {
    display: 'flex',
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  chip: {
    margin: theme.spacing.unit,
    marginLeft: 0,
    fontSize: 10,
    height: 'auto',
    width: 'auto',
  },
  heading: {
    marginBottom: '1.4rem',
  },
  interpreterLoadingImg:{
    width: '100%'
  }


})

const LinkTo = props => <Link to={props.to} {...props} />

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  config: state.common.config,
  inProgress: state.common.inProgress,
  loadingScreen: state.common.loadingScreen,
  interpreters:  state.call.listOfInterpreters ? state.call.listOfInterpreters : [],
  total: state.call.interpreterTotal ? state.call.interpreterTotal : 0,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class Interpreters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      offset: 0,
      limit: 20,
      defaultOffset: 0,
      defaultLimit: 20,
      filters: '',
      interpreters: [],
      expanded: true,
    };
    this.filterInterpreter = this.filterInterpreter.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.toogleSearchPanel = this.toogleSearchPanel.bind(this);
  }

  componentDidMount() {
    this.filterInterpreter('');
    window.addEventListener('scroll', this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  componentWillReceiveProps(nextProps) {
    let { interpreters } = this.state;
    if (!nextProps.inProgress && nextProps.interpreters && nextProps.interpreters.length && nextProps.total > interpreters.length) {
      this.setState({interpreters: interpreters.concat(nextProps.interpreters)});
    }
  }

  onScroll = () => {
    const { offset, limit, filters } = this.state;
    const { total, inProgress } = this.props;

    // calculate page height and window height, detect scroll and fetch next records when reached bottom of page
    const windowHeight = window.innerHeight ? window.innerHeight : document.documentElement.offsetHeight
    const docHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight)
    const windowBottom = windowHeight + window.pageYOffset
    if (!inProgress && (windowBottom >= docHeight) && total > 0) {
      let newOffset = offset + 1;
      if (total > ((offset * limit) + limit)) {
        this.setState({ offset: newOffset });
        this.filterInterpreter('', filters, newOffset, limit);
      }
    }
  }

  filterInterpreter (query, filters, offset, limit) {
    var data = {
      query: query
    }

    if (filters) {
      data.filters = filters;
    }

    if (offset) {
      data.offset = offset;
    }

    if (limit) {
      data.limit = limit;
    }

    this.props.dispatch(actions.interpretersSearch(data));
  }

  handleFilter (filters) {
    const { defaultOffset, defaultLimit } = this.state;
    this.setState({ filters: filters, interpreters: [], offset: defaultOffset, limit: defaultLimit});
    this.filterInterpreter ('', filters, defaultOffset, defaultLimit);
  }

  randomBackground = () => {
    return ['red', 'purple', 'blue', 'orange', 'teal', 'green'][Math.floor(Math.random() * 6)]
  }

  toogleSearchPanel () {
    this.setState({expanded: !this.state.expanded})
  }

  getNameFromId = (id, array) => {
    const match = array.find((item) => item.id === id);
    return match ? match.name : '';
  }

  render() {
    const {classes, config, loadingScreen, width} = this.props;
    const { interpreters } = this.state;
    return (
      <div className={classes.interpretersView}>
        <Grid container>

          <Grid item xs={12} sm={3} style={{padding: '1.2rem'}}>
            {width === 'xs' ?
              <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.heading}>Search Filters</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <InterpreterSearch config={config} handleFilter={this.handleFilter} />
                </ExpansionPanelDetails>
              </ExpansionPanel>
              :
              <React.Fragment>
                <Typography className={classes.heading} variant="title" gutterBottom>Search</Typography>
                <InterpreterSearch config={config} handleFilter={this.handleFilter} />
              </React.Fragment>
            }
          </Grid>

          <Grid item xs={12} sm={9}>
            <Grid container>
              {interpreters && interpreters.map((interpreter, index) =>
                <Grid item xs={12} sm={4} key={index} style={{padding: '1rem'}}>
                  <Card className={classes.card}>
                    <CardHeader style={{paddingBottom: 0}} avatar={<Avatar aria-label="Recipe"
                                className={classnames(classes.avatar, this.randomBackground())}>{interpreter.name[0]}</Avatar>}
                      action={
                        <IconButton component={LinkTo} to={`/profile/${interpreter.uid}`}>
                          <MoreVertIcon />
                        </IconButton>
                      }
                      title={interpreter.name}
                      subheader={<UserRating rating={interpreter.avgRating}/>}
                    />
                  <CardContent style={{paddingBottom: 0, paddingTop: 8,     minHeight: 'inherit'}}>
                      <div>
                        <Typography component="p">Sign Languages</Typography>
                        {interpreter.signLanguages && interpreter.signLanguages.map((signLanguage, singlIndex) =>
                          <Chip label={this.getNameFromId(signLanguage, config.signLanguages)} key={singlIndex} className={classes.chip} />
                        )}
                      </div>
                      <div>
                        <Typography component="p">Spoken Languages Languages</Typography>
                        {interpreter.spokenLanguages && interpreter.spokenLanguages.map((spokenLanguage, spokenlIndex) =>
                          <Chip label={this.getNameFromId(spokenLanguage, config.spokenLanguages)} className={classes.chip} key={spokenlIndex} />
                        )}
                      </div>
                    </CardContent>
                    <CardActions className={classes.actions} disableActionSpacing>
                      <IconButton aria-label="Add to favorites">
                        <FavoriteIcon />
                      </IconButton>
                      <div style={{textAlign: 'right'}}>
                        {interpreter.experience && <Chip label={this.getNameFromId(interpreter.experience, config.experienceOptions)} className={classes.chip} />}
                        {interpreter.specialized && <Chip label={this.getNameFromId(interpreter.specialized, config.specializationOptions)} className={classes.chip} />}
                      </div>
                    </CardActions>
                  </Card>
                </Grid>
                )}


              {loadingScreen && [1,2,3,4,5,6,7,8,9].map((interpreter, index) =>
                <Grid item xs={12} sm={4} key={index}>
                  <Card className={classes.card}>
                    <img src={loadingImage} alt={interpreter} className={classes.interpreterLoadingImg} />
                  </Card>
                </Grid>
                )}

              {!interpreters.length && !loadingScreen && <div style={{textAlign: 'center'}}><h6>No Interpreter(s) Found</h6></div>}
           </Grid>
         </Grid>

      </Grid>
    </div>
    );
  }
}

Interpreters.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
};

const authCondition = (authUser, currentUser) => !!authUser && currentUser && currentUser.userType === 'deaf';

export default compose(withAuthorization(authCondition), withWidth(), withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Interpreters);
