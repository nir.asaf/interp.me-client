import React from 'react';
import {connect} from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import withWidth from '@material-ui/core/withWidth';
import {compose} from 'recompose';
import withAuthorization from '../components/Session/withAuthorization';
import DetailsForm from '../components/User/DetailsForm';
import Languages from '../components/User/Languages';
import Reviews from '../components/User/Reviews';
import actions from '../actions';
import PropTypes from 'prop-types';
import Notifications from '../components/User/Notifications';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Payment from '../components/Payment/Payment';
import GA from '../utils/GoogleAnalytics'

const styles = theme => ({
  settingsPage: {
    flexGrow: 1,
    width: '100%',
    flex: '1 1 100%',
    margin: '0 auto',
  },
  tabContainer: {
    maxWidth: '490px',
    margin: '1rem auto',
    width: '100%',
    background: '#fff',
    border: '1px solid #ddd',
  },
})

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  authUser: state.session.authUser,
  detailsEditStatus: state.settings.detailsEditStatus,
  inProgress: state.common.inProgress,
  signLanguagesList: state.common.config && !state.common.config.signLanguages ? [] :
    state.common.config.signLanguages.map(language => {
      return {
      label: language.name,
      value: language.id
    }}),
  spokenLanguagesList: state.common.config && !state.common.config.spokenLanguages ? [] :
    state.common.config.spokenLanguages.map(language => {
      return {
      label: language.name,
      value: language.id
    }}),
  aslCertificatesList: state.common.config && state.common.config.aslCertificates ? state.common.config.aslCertificates : [],
  countries: state.common.config.countries ? state.common.config.countries.map(country => ({
    value: country.code,
    label: country.name,
    zipCodeValidator: country.zipCodeValidator,
  })) : [],
  experienceOptions: state.common.config && !state.common.config.experienceOptions ? [] :
    state.common.config.experienceOptions.map(experience => {
      return {
      label: experience.name,
      value: experience.id
    }}),
  specializationOptions: state.common.config && !state.common.config.specializationOptions ? [] :
    state.common.config.specializationOptions.map(specialization => {
      return {
      label: specialization.name,
      value: specialization.id
    }}),
});

const mapDispatchToProps = dispatch => ({
    dispatch
});

class Settings extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        activeTab: 'details'
      };
      this.onSubmitDetailsForm = this.onSubmitDetailsForm.bind(this);
      this.updateUserCurrent = this.updateUserCurrent.bind(this);
  }

  componentDidMount() {
    if (this.props.router.location.hash) {
      this.setState({activeTab: this.props.router.location.hash.replace("#","")})
    }
  }

  componentWillUnmount() {
    this.setState({activeTab: null})
  }

  onSubmitDetailsForm(user) {
    GA.logEvent("Settings", "Submit Clicked", {"label": "User Details"});
    console.log(user);
    this.props.dispatch(actions.usersEdit(user));
  }

  updateUserCurrent() {
    if (this.props.authUser) this.props.dispatch(actions.usersGetCurrent());
  }

  handleTabChange = (event, value) => {
      this.setState({activeTab: value})
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.detailsEditStatus && this.props.detailsEditStatus !== nextProps.detailsEditStatus) {
      var data;
      if (nextProps.detailsEditStatus.success ){
        data = {message: 'Your changes have been saved', variant: 'success'};
        this.props.dispatch(actions.openUserSnackbar(data));
      }else {
        data = {message: 'Sorry, Something Went Wrong: Please try again later.',variant: "warning",};
        this.props.dispatch(actions.openUserSnackbar(data));
      }
    }
  }

  render() {
    const { activeTab } = this.state;
    const { currentUser, aslCertificatesList, inProgress, countries, signLanguagesList, spokenLanguagesList, classes, specializationOptions, experienceOptions, width } = this.props;

    return (
      <div className={classes.settingsPage}>
        <AppBar position="static" color="default">
          <Tabs value={activeTab} onChange={this.handleTabChange} centered={width === 'xs' ? false : true } indicatorColor="primary" scrollable={width === 'xs' ? true : false } scrollButtons="off">
            <Tab value="details" label="Details" href="#details"/>
            <Tab value="languages" label="Languages" href="#languages"/>
            {currentUser && currentUser.userType === 'deaf' && <Tab value="payment" label="Payment" href="#payment"/>}
            {currentUser && currentUser.userType === 'interpreter' && <Tab value="reviews" label="Reviews" href="#reviews"/>}
            {currentUser && currentUser.userType === 'interpreter' && <Tab value="notifications" label="Notifications" href="#notifications"/>}
            Notifications
          </Tabs>
        </AppBar>
        <div className={classes.tabContainer}>
          {activeTab === 'details' && <DetailsForm inProgress={inProgress}
                                                   id="details"
                                                   updateUserCurrent={this.updateUserCurrent}
                                                   detailsEditStatus={this.props.detailsEditStatus}
                                                   countries={countries}
                                                   currentUser={currentUser}
                                                   experienceOptions={experienceOptions}
                                                   specializationOptions={specializationOptions}
                                                   onSubmitDetailsForm={this.onSubmitDetailsForm}/>}
          {activeTab === 'languages' && <Languages currentUser={currentUser}
                                                   detailsEditStatus={this.props.detailsEditStatus}
                                                   signLanguagesList={signLanguagesList}
                                                   spokenLanguagesList={spokenLanguagesList}
                                                   dispatch={this.props.dispatch}
                                                   aslCertificatesList={aslCertificatesList}
                                                   inProgress={inProgress}
                                                   updateUserCurrent={this.updateUserCurrent}
                                                   onSubmitDetailsForm={this.onSubmitDetailsForm}/>}
          {activeTab === 'reviews' && currentUser && currentUser.userType === 'interpreter' && <Reviews currentUser={currentUser} uid={currentUser.uid}/>}
          {activeTab === 'notifications' && currentUser && currentUser.userType === 'interpreter' && <Notifications inProgress={inProgress} onSubmitDetailsForm={this.onSubmitDetailsForm} currentUser={currentUser}/>}
          {activeTab === 'payment' && currentUser && currentUser.userType === 'deaf' && <Payment inProgress={inProgress} currentUser={currentUser}/>}
        </div>
      </div>
    );
  }
}

Settings.propTypes = {
  classes: PropTypes.object.isRequired,
};

const authCondition = (authUser) => !!authUser;

export default compose(withAuthorization(authCondition), withRouter, connect(mapStateToProps, mapDispatchToProps), withWidth(), withStyles(styles))(Settings);
