import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import SelectComponent from '../Common/SelectComponent';
import FormHelperText from '@material-ui/core/FormHelperText';
import { compose } from 'recompose';

const styles = theme => ({
  notifications: {
    padding: 30,
  },
})

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  signLanguageList: state.common.config && !state.common.config.signLanguages ? [] : state.common.config.signLanguages,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class SignLanguageSelect extends React.Component {
  constructor(props) {
    super();
    this.state = {
      err: '',
      signLanguages: props.currentUser.signLanguages ? props.currentUser.signLanguages : props.value,
      showAslCertificates: props.currentUser.aslCertificates ? props.currentUser.aslCertificates : [],
    }
  }

  render() {
    const {signLanguageList, type='multi'} = this.props;
    const {signLanguages, err} = this.state;
    const signLanguagesFilterList = signLanguageList.map(language => {
      return {
        label: language.name,
        value: language.id
      }
    });
    return (
      <React.Fragment>
        <SelectComponent label="Sign Language" restriction={true} key="signLanguage" value={signLanguages} suggestions={signLanguagesFilterList} placeholder="Sign Languages"
          selectType={type}
          isLoading={signLanguagesFilterList.length ? false : true}
          icon={<Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/>}
          callback={() => this.setState({err: "You must have a least one Sign Languages "})}
          onChange={(obj) => {
            let newSignLanguages = Object.assign([], signLanguages);
            if (type === 'multi'){
              newSignLanguages = obj.map(signLanguage => {return signLanguage.value});
              if (!obj.length) {
                newSignLanguages = null
                this.setState({showAslCertificates: false});
              }
              this.setState({signLanguages: newSignLanguages, err: ''});
            }else {
              newSignLanguages = obj.value;
            }
            this.props.onChange(newSignLanguages);
          }}
        />
        {err && <FormHelperText>*{err}</FormHelperText>}
      </React.Fragment>

    );
  }
}

SignLanguageSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

SignLanguageSelect.defaultProps = {
  value: []
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(SignLanguageSelect);
