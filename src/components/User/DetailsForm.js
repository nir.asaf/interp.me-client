import React from 'react';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import { withStyles } from '@material-ui/core/styles';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import SelectComponent from '../Common/SelectComponent';
import Grid from '@material-ui/core/Grid';
import LoadingButton from '../Common/LoadingButton';

const styles = theme => ({
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 4,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  gridWrap: {
    marginTop: '1rem',
  }
})

class DetailsForm extends React.Component {
  constructor(props) {
    super();

    this.state = {
      name: props.currentUser.name ? props.currentUser.name : '',
      image: props.currentUser.image ? props.currentUser.image : '',
      email: props.currentUser.email ? props.currentUser.email : '',
      selectedFile: '',
      signLanguages: '',
      country: props.currentUser.country ? props.currentUser.country : '',
      gender: props.currentUser.gender ? props.currentUser.gender : '',
      zipCode: props.currentUser.zipCode ? props.currentUser.zipCode : '',
      phone: props.currentUser.phone ? props.currentUser.phone : '',
      bio: props.currentUser.userType === 'interpreter' && props.currentUser.bio ? props.currentUser.bio : '',
      experience: props.currentUser.userType === 'interpreter' && props.currentUser.experience ? props.currentUser.experience : null,
      specialized: props.currentUser.userType === 'interpreter' && props.currentUser.specialized ? props.currentUser.specialized : null,
      zipCodeValidator: '',
      err: ''
    };
    this.updateState = this.updateState.bind(this);
  }

  updateState = name => event => {
   this.setState({
     [name]: event.target.value,
     err: ''
   });
 };


 validate = (user) => {
   if (user.zipCode) {
     var regexPattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
     if (this.state.zipCodeValidator) {
       regexPattern = new RegExp(this.state.zipCodeValidator);
     }
     if (!regexPattern.test(user.zipCode)) {
       this.setState({
         err: 'Please enter a valid zip code'
       })
       return false;
     }
   }
   return true;
 }

 update = user => ev => {
    ev.preventDefault();
    if(this.validate(user)) {
      this.props.onSubmitDetailsForm(user);
    }
  }

  render() {
    const {countries, currentUser, classes} = this.props;
    const {zipCode, gender, name, phone, country, err, bio, experience, specialized} = this.state;
    const user = {zipCode, gender, name, phone, country, experience, specialized, bio};
    return (
      <Grid container style={{padding: 30}}>
        <Grid container spacing={24}>
          {err && (
            <Grid item xs={12} sm={12}>
              <div className="alert alert-danger" role="alert" style={{fontSize: "12px"}}>{err}</div>
            </Grid>)}
          <Grid item xs={12} sm={12}>
            <h4>Personal Details</h4>
          </Grid>
          { /*<input type = "file" style = {{display: 'none'}} onChange = {this.fileSelectedHandler} ref = {fileInput => this.fileInput = fileInput}/> <div><a> { this.state.selectedFile ? (<img src={this.state.imgSrc} className="z-depth-1" alt="user" style={{border: '1px solid #c9c9c9', height: '120px', padding: '6px'}}></img>) :(<i className="fas fa-user-circle fa-8x"></i>)}</a></div>*/ }
          <Grid item xs={6} sm={6}>
            <TextField fullWidth value={this.state.name} label="Name" onChange={this.updateState("name")}/>
          </Grid>
          <Grid item xs={6} sm={6}>
            <TextField fullWidth value={this.state.phone} label="Phone" disabled={true} onChange={this.updateState("phone")}/>
          </Grid>
          <Grid item xs={12} sm={8}>
            <SelectComponent fullWidth suggestions={countries} value={country} placeholder="Select country" label="Country" selectType={'single'} onChange={(obj) => {
                if (obj.value) this.setState({country: obj.value});
                if (obj.zipCodeValidator) this.setState({zipCodeValidator: obj.zipCodeValidator});
              }}/>
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField fullWidth value={this.state.zipCode} label="Zip code" disabled={country ? false : true} onChange={this.updateState("zipCode")}/>
          </Grid>
        </Grid>
        { currentUser && currentUser.userType === 'interpreter' && (
          <Grid container spacing={16} style={{marginTop: '2rem'}}>
            <Grid item xs={6} sm={6}>
              <h6>Experience</h6>
              <SelectComponent value={experience} suggestions={this.props.experienceOptions} onChange={obj => this.setState({experience: obj.value})} selectType={'single'}/>
            </Grid>
            <Grid item xs={6} sm={6}>
              <h6>Specialized in</h6>
              <SelectComponent value={specialized} suggestions={this.props.specializationOptions} onChange={obj => this.setState({specialized: obj.value})} selectType={'single'}/>
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField fullWidth value={bio} label="Say a few things about yourself..." multiline rows="5" margin="normal" onChange={event => {if (event.target.value.length <= 200) this.setState({bio: event.target.value});}}/>
              <FormHelperText style={{textAlign: 'right'}}>{bio.length}/200</FormHelperText>
            </Grid>
          </Grid>)}

        <Grid container spacing={16} className={classes.gridWrap}>
          <Grid item xs={12} sm={12}>
            <FormLabel style={{textAlign: 'left'}} component="legend">Gender</FormLabel>
            <RadioGroup style={{flexDirection: 'row'}} aria-label="gender" name="gender" value={this.state.gender}onChange={this.updateState("gender")}>
              <FormControlLabel value="f" control={<Radio color="primary"/>} label="Female" />
              <FormControlLabel value="m" control={<Radio color="primary"/>} label="Male" />
              <FormControlLabel value="" control={<Radio color="primary"/>} label="Not Specified / Other" />
            </RadioGroup>
          </Grid>
        </Grid>
        <Grid container spacing={16}>
          <Grid item xs={12} sm={12}>
            <LoadingButton loading={this.props.inProgress} disabled={this.props.inProgress} onClick={this.update(user)} label="Update Settings"/>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

DetailsForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailsForm);
