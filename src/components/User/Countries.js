import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SelectComponent from '../Common/SelectComponent';
import { compose } from 'recompose';

const styles = theme => ({

})

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  countries: state.common.config && !state.common.config.countries ? [] : state.common.config.countries,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class Countries extends React.Component {
  constructor(props) {
    super();
    this.state = {
      err: '',
      country: props.currentUser && props.currentUser.country ? props.currentUser.country : props.value,
    }
  }

  componentDidMount() {
  }

  render() {
    // const {} = this.props;
    const {country} = this.state;
    const {countries} = this.props;
    const countriesFilterList= countries.map(country => {
      return {
        label: country.name,
        value: country.code,
        zipCodeValidator: country.zipCodeValidator,
    }});
    return (
      <React.Fragment>
        <SelectComponent label="Country"
                         key="country"
                         suggestions={countriesFilterList}
                         value={country}
                         placeholder="Select your country"
                         selectType="single"
                         onChange={(obj) => {
                           this.setState({country: obj.value, zipCodeValidator: obj.zipCodeValidator});
                           this.props.onChange(obj);
                         }}
                         />
      </React.Fragment>

    );
  }
}

Countries.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Countries);
