import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const styles = theme => ({
  radioGroup: {
    width: '100%',
  },
})

class GenderSelect extends React.Component {
  state = {
    gender: this.props.value,
  }

  handleGenderChange = event => {
    this.setState({ gender: event.target.value });
  };

  componentWillReceiveProps (nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ gender: nextProps.value });
    }
  }

  render() {
    const {classes, row=true, label=null} = this.props;
    const {gender} = this.state;
    return (
      <div>
        { label && label.length ? <InputLabel style={{transform: 'translate(0, 1.5px) scale(0.75)', transformOrigin: 'top left', float: 'left'}} htmlFor="selectComponent">{label}</InputLabel> : null }
        <RadioGroup aria-label="gender"
                    name="gender"
                    className={classes.radioGroup}
                    row={row}
                    value={gender}
                    onChange={this.props.onChange}>
          <FormControlLabel value="f" control={<Radio color="primary"/>} label="Female only" />
          <FormControlLabel value="m" control={<Radio color="primary"/>} label="Male only" />
          <FormControlLabel value="" control={<Radio color="primary"/>} label="Any" />
        </RadioGroup>
      </div>
    )
  }
}

GenderSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GenderSelect);
