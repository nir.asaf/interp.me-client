import React from 'react';
// import actions from '../../actions';
import { connect } from 'react-redux';


const mapStateToProps = state => ({
  currentBalance: state.settings.credit
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class CurrentBalance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentBalance: null
    }
  }

  componentDidMount() {
    if (this.props.currentUser && this.props.currentUser.userType === 'deaf') {
      this.setState({currentBalance: this.props.currentUser.minutesCredit})
      // this.props.dispatch(actions.deafGetMinutesCredit())
    } else if (this.props.currentUser && this.props.currentUser.userType === 'interpreter') {
      this.setState({currentBalance: this.props.currentUser.payoutCredit / 100})
      // this.props.dispatch(actions.interpretersGetPayoutCredit())
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentBalance && nextProps.currentBalance !== this.state.currentBalance) {
      this.setState({currentBalance: nextProps.currentBalance})
    }
  }

  render() {
    const currentBalance = this.state.currentBalance;
    return (
      <span>
        { currentBalance  ?
          currentBalance
          :
          0
        }
      </span>
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentBalance);
