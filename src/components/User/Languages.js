import React from 'react';
import Grid from '@material-ui/core/Grid';
import LoadingButton from '../../components/Common/LoadingButton';
import SelectComponent from '../Common/SelectComponent';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import SpokenLanguageSelect from './SpokenLanguageSelect'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import { compose } from 'recompose';

const styles = theme => ({
  gridWrap: {
    marginTop: '1rem',
  },
  error: {
    color: '#f44336',
    textAlign: 'center',
    background: '#f8f8f8',
    maxWidth: '400px',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '0.2rem',
    borderRadius: '2%',
  },
  languages: {
    padding: 30,
  },
})

class Languages extends React.Component {
  constructor(props) {
    super();
    this.state = {
      err: '',
      spokenLanguages: props.currentUser ? props.currentUser.spokenLanguages : [],
      signLanguages: props.currentUser ? props.currentUser.signLanguages : [],
      aslCertificates: props.currentUser ? props.currentUser.aslCertificates : [],
      showAslCertificates: props.currentUser && props.currentUser.aslCertificates && props.currentUser.aslCertificates.length ? true : false,
    };
  }

  updateLanguages = () => ev => {
    ev.preventDefault();
    var data = {
        spokenLanguages: this.state.spokenLanguages,
        signLanguages: this.state.signLanguages,
        aslCertificates: this.state.aslCertificates,
    }
    if (this.validate()) this.props.onSubmitDetailsForm(data);
  }

  validate = () => {
    const {signLanguages, spokenLanguages} = this.state;
    if (!signLanguages) {
      this.setState({err: "You must have a least one Sign Language "})
      return false;
    }
    if (this.props.currentUser.userType === 'interpreter' && !spokenLanguages) {
      this.setState({err: "You must have a least one Spoken Language "})
      return false;
    }
    return true
  }

  onAslCertificatesChange = value => event => {
    if (event.target.checked) {
      if (!this.state.aslCertificates.length) {
        this.setState({aslCertificates: [value]})
      } else {
        this.setState({aslCertificates: this.state.aslCertificates.concat([value])})
      }
    } else {
      this.setState({ aslCertificates: this.state.aslCertificates.filter(val => val !== value )})
    }
  }

  render() {
      const {err, signLanguages, spokenLanguages, showAslCertificates, aslCertificates} = this.state;
      const {classes, signLanguagesList, currentUser, aslCertificatesList} = this.props;

      return (
        <Grid container className={classes.languages}>
          <Grid item xs={12} sm={12}>
            <Grid container spacing={16} justify="center">
              {err ? <div className={classes.error}>{err}</div> : null}
            </Grid>
          </Grid>
          <Grid item xs={12} sm={12}>
            <h4>Languages</h4>
          </Grid>
          <Grid container spacing={16}>
            {currentUser && currentUser.userType === 'interpreter' && (
            <Grid item xs={12} sm={12} className={classes.gridWrap}>
              <SpokenLanguageSelect onChange={val => {
                  let newSpokenLanguages = Object.assign([], spokenLanguages);
                  newSpokenLanguages = val;
                  this.setState({spokenLanguages: newSpokenLanguages});
                }}/>
            </Grid>)}
            <Grid item xs={12} sm={12} className={classes.gridWrap}>
                <SelectComponent label="Sign Languages" restriction={true} key="signLanguages" value={signLanguages} suggestions={signLanguagesList} placeholder="Sign Languages" selectType='multi'
                    icon={<Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/>}
                    callback={() => this.setState({err: "You must have a least one Sign Language "})}
                    onChange={(obj) => {
                      let newSignLanguages = Object.assign([], signLanguages);
                      newSignLanguages = obj.map(signLanguage => {
                        if (signLanguage.value === 'mQPtRQBhlg09sH7fvzI2' || signLanguage.value === 'eQTaxbYvtNU1fDncJ4ie'){
                          this.setState({showAslCertificates: true})
                        }
                        return signLanguage.value
                      });
                      if (!newSignLanguages.includes("mQPtRQBhlg09sH7fvzI2") && !newSignLanguages.includes("eQTaxbYvtNU1fDncJ4ie")){
                          this.setState({showAslCertificates: false, aslCertificates: []})
                      }
                      if (!obj.length) {
                        newSignLanguages = null;
                        this.setState({showAslCertificates: false});
                      }
                      this.setState({signLanguages: newSignLanguages, err: ''});

                    }}
                  />
            </Grid>
            <Grid item xs={12} sm={12} className={classes.gridWrap}>
                { currentUser && currentUser.userType === 'interpreter' && showAslCertificates ?
                  <FormGroup row className={classes.gridWrap}>
                    <FormLabel style={{textAlign: 'left', width: '100%'}} component="legend">ASL Certificates:</FormLabel>
                    { aslCertificatesList.map((aslCertificate, index) => {
                      return (
                        <FormControlLabel key={index}
                                          control={<Checkbox key={index} color="primary" checked={aslCertificates && aslCertificates.includes(aslCertificate.id)}
                                          value={aslCertificate.id} onChange={this.onAslCertificatesChange(aslCertificate.id)}/>}
                                          label={aslCertificate.name}/> )
                    })}
                  </FormGroup> : null
                }
            </Grid>
          </Grid>
          <Grid container spacing={16} className={classes.gridWrap}>
            <Grid item xs={12} sm={12}>
              <LoadingButton loading={this.props.inProgress} disabled={this.props.inProgress} onClick={this.updateLanguages()} label="Update Settings"/>
            </Grid>
          </Grid>
        </Grid>

      );
  }
}

Languages.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles))(Languages);
