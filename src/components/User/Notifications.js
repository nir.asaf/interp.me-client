import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import LoadingButton from '../../components/Common/LoadingButton';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { compose } from 'recompose';

const styles = theme => ({
  notifications: {
    padding: 30,
  },
})

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class Notifications extends React.Component {
  constructor(props) {
    super();
    this.state = {
      callPreferences: {
        instant_free: {enabled: true},
        instant_fixed: {enabled: true},
        scheduled_video: {enabled: true},
      },
      instant_free: props.currentUser.callPreferences && props.currentUser.callPreferences.instant_free ? props.currentUser.callPreferences.instant_free.enabled : false,
      instant_fixed: props.currentUser.callPreferences && props.currentUser.callPreferences.instant_fixed ? props.currentUser.callPreferences.instant_fixed.enabled : false,
      scheduled_video: props.currentUser.callPreferences &&  props.currentUser.callPreferences.scheduled_video ? props.currentUser.callPreferences.scheduled_video.enabled : false,
    };
  }

  componentDidMount() {
  }

  updateLanguages = () => ev => {
    ev.preventDefault();
    var data = {
      callPreferences: this.state.callPreferences
    }
    this.props.onSubmitDetailsForm(data);
  }

  callPreferencesChange = name => event => {
    let newCallPreferences = Object.assign({}, this.state.callPreferences);
    newCallPreferences[name].enabled = event.target.checked;
    this.setState({ [name]: event.target.checked, callPreferences:  newCallPreferences});
  };

  render() {
    const {classes} = this.props;
    const {instant_free, instant_fixed, scheduled_video} = this.state;
    return (
      <Grid container spacing={16} className={classes.notifications}>
        <Grid item xs={12} sm={12}>
          <h4>Notifications (SMS & Email)</h4>
          <p>You will always have the choice whether to accept or decline a call</p>
          <FormControlLabel control={<Switch color="primary" checked={instant_free} value={"instant_free"} onChange={this.callPreferencesChange("instant_free")}/>} label="Notify me of Pro-Bono calls"/>
          <FormControlLabel control={<Switch color="primary" checked={instant_fixed} value={"instant_fixed"} onChange={this.callPreferencesChange("instant_fixed")}/>} label="Notify me of On-demand calls"/>
          <FormControlLabel control={<Switch color="primary" checked={scheduled_video} value={"scheduled_video"} onChange={this.callPreferencesChange("scheduled_video")}/>} label="Notify me of Scheduled calls"/>
        </Grid>
        <Grid item xs={12} sm={12}>
          <LoadingButton loading={this.props.inProgress} disabled={this.props.inProgress} onClick={this.updateLanguages()} label="Update Settings"/>
        </Grid>
      </Grid>
    );
  }
}

Notifications.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Notifications);
