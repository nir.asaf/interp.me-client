import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import actions from '../../actions';
import SelectComponent from '../Common/SelectComponent';
import FormHelperText from '@material-ui/core/FormHelperText';
import { compose } from 'recompose';

const styles = theme => ({
  notifications: {
    padding: 30,
  },
})

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  spokenLanguagesList: state.common.config && !state.common.config.spokenLanguages ? [] : state.common.config.spokenLanguages,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class SpokenLanguageSelect extends React.Component {
  constructor(props) {
    super();
    this.state = {
      err: '',
      spokenLanguages: props.currentUser.spokenLanguages ? props.currentUser.spokenLanguages : props.value,
      spokenLanguagesList: props.spokenLanguagesList,
    }
  }

  componentDidMount() {
    if (this.props.spokenLanguagesList && !this.props.spokenLanguagesList.length) {
      this.props.dispatch(actions.configGetSignUpProperties());
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.spokenLanguagesList && !this.props.spokenLanguagesList.length) {
      this.setState({
        spokenLanguagesList: nextProps.spokenLanguagesList
      })
    }
  }

  render() {
    const {type='multi', label="Spoken Language", placeholder="Spoken Language"} = this.props;
    const {spokenLanguages, err, spokenLanguagesList} = this.state;
    const spokenLanguagesFilterList = spokenLanguagesList.map(language => {
      return {
        label: language.name,
        value: language.id
      }
    });
    return (
      <React.Fragment>
        <SelectComponent label={label}
          restriction={true} key="spokenLanguages"
          value={spokenLanguages}
          suggestions={spokenLanguagesFilterList}
          placeholder={placeholder}
          selectType={type}
          icon={<Icon style={{ fontSize: 24 }} className={'fa fa-language'}/>}
          isLoading={spokenLanguagesFilterList.length ? false : true}
          callback={() => this.setState({err: "You must have a least one Spoken Languages "})}
          onChange={(obj) => {
            let newSpokenLanguages = Object.assign([], spokenLanguages);
            if (type === 'multi'){
              newSpokenLanguages = obj.map(spokenLanguage => {return spokenLanguage.value});
              if (!obj.length) newSpokenLanguages = null;
              this.setState({spokenLanguages: newSpokenLanguages, err: ''});
            }else {
              newSpokenLanguages = obj.value;
            }
            this.props.onChange(newSpokenLanguages);
          }}
        />
        {err && <FormHelperText>*{err}</FormHelperText>}
      </React.Fragment>

    );
  }
}

SpokenLanguageSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

SpokenLanguageSelect.defaultProps = {
  value: []
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(SpokenLanguageSelect);
