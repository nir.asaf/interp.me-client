import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import Typography from '@material-ui/core/Typography';
import UserRating from '../Common/UserRating';
import { compose } from 'recompose';
import Avatar from '@material-ui/core/Avatar';
import actions from '../../actions';

const styles = theme => ({
  review: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    width: '100%',
    boxSizing: 'border-box',
    borderBottom: '1px solid #ddd',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  image: {
    display: 'flex',
  },
  avatar: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
})

const mapStateToProps = state => ({
  reviews: state.review.reviews,
  currentUser: state.common.currentUser ? state.common.currentUser : null,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class Reviews extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  componentDidMount() {
    var data = {
      targetUserId: this.props.uid
    }
    if (!this.props.reviews) {
      this.props.dispatch(actions.reviewsList(data));
    }
  }

  render() {
    const {classes, reviews, currentUser} = this.props;
    return (
      <div>
        { reviews && reviews.length ?
            reviews.map((review, index) => {
              return (
                <Grid container spacing={16} className={classes.review} key={index}>
                  <Grid item>
                    <div className={classes.image}>
                      <Avatar aria-label="review" className={classes.avatar}></Avatar>
                    </div>
                  </Grid>
                  <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={16}>
                      <Grid item xs>
                        <Typography variant="subheading"><UserRating rating={review.ratingStars}/></Typography>
                        <Typography variant="subheading" gutterBottom>{review.reviewText}</Typography>
                        <Typography variant="caption" color="textSecondary">{moment(review.creationTime).startOf('day').fromNow()}</Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              )
            })
          :
          <Grid item xs={12} sm={12} style={{padding: '1rem'}}>
            <h5>{reviews ?
                  currentUser && currentUser.uid === this.props.uid && "You don't have any user reviews yet. invite your deaf friends to interp.me and ask them to review you."
                  :
                  "Loading reviews ..."
                }
            </h5>
          </Grid>
        }
      </div>
    );
  }
}

Reviews.propTypes = {
  classes: PropTypes.object.isRequired,
  uid: PropTypes.string.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Reviews);
