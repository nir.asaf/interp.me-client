import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from '../../actions';
import Video from 'twilio-video';
import { LocalDataTrack, createLocalTracks }  from 'twilio-video';
import CallEnd from '@material-ui/icons/CallEnd';
import IconButton from '@material-ui/core/IconButton';
import './Video.css';

const localDataTrack = new LocalDataTrack();

const style = {
  endCallButton: {
    background: '#ed4c7f',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px',
    borderRadius: '100%',
    position: 'fixed',
    left: '50%',
    bottom: '40px',
    transform: 'translate(-50%, -50%)',
    margin: '0 auto',
    width: 'auto',
    height: 'auto',
  },
  endCallIcon: {
    height: '35px',
    width: '35px',
    color: '#fff',
  }
}

const mapStateToProps = state => ({
  callData: state.call.data
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class VideoComponent extends Component {
 constructor(props) {
   super();
   this.state = {
      identity: props.currentUser.uid,  /* Will hold the name assigned to the client. */
      roomName: props.callData.id,     /* Will store the room name */
      roomNameErr: false,  /* Track error for room name TextField. This will    enable us to show an error message when this variable is true */
      previewTracks: null,
      localMediaAvailable: false, /* Represents the availability of a LocalAudioTrack(microphone) and a LocalVideoTrack(camera) */
      hasJoinedRoom: false,
      activeRoom: null, // Track the current active room
      twillioToken: props.currentUser.twillioToken,
      showCounter: false,
   };
   this.joinRoom = this.joinRoom.bind(this);
   this.roomJoined = this.roomJoined.bind(this);
   this.finishCallButton = this.endCallButton.bind(this);
   this.detachTracks = this.detachTracks.bind(this);
   this.detachParticipantTracks =this.detachParticipantTracks.bind(this);
   this.startSession =this.startSession.bind(this);
   this.connectInterpreter =this.connectInterpreter.bind(this);
 }

 componentDidMount() {
   this.props.onRef(this);
   this.joinRoom();
   if (this.props.callData && this.props.callData.state === "started") {this.setState({showCounter: true});}
 }

 componentWillUnmount() {
   if (this.props.currentUser.userType === 'deaf') {
     this.props.onRef(null);
   }
   if (this.state.activeRoom) { this.state.activeRoom.disconnect();}
   this.setState({ activeRoom: null, hasJoinedRoom: false, localMediaAvailable: false });
 }

 async setupLocalAudioAndVideoTracks() {
   try {
     const audioAndVideoTrack = await createLocalTracks();
     return audioAndVideoTrack;
   } catch(error) {
     console.log(error);
     return [];
   }
 }

 async joinRoom() {
   // This is enabled by setting `roomNameErr` to true
   if (!this.state.roomName.trim()) {
      this.setState({ roomNameErr: true });
      return;
    }
    // For testing the webrtc https://test.webrtc.org/
    const audioAndVideoTrack = await this.setupLocalAudioAndVideoTracks();
    const tracks = audioAndVideoTrack.concat(localDataTrack);

    console.log("Joining room '" + this.state.roomName + "'...");
    let connectOptions = {
      name: this.state.roomName,
      tracks: tracks,
    };

    if (this.state.previewTracks) {
      console.log("There is a preview Tracks");
      connectOptions.tracks = this.state.previewTracks;
    }
    /* Connect to a room by providing the token and connection options
     that include the room name and tracks. We also show an alert if an
     error occurs while connecting to the room.  */
    // console.log("Try to connect with token:" + this.state.twillioToken + " options:" + connectOptions);

    // Connect to a room
    const room = await Video.connect(this.state.twillioToken, connectOptions);

    // Save a reference to `room` on `window` for debugging.
    window.room = room;
    this.roomJoined(room);

 }

 connectInterpreter() {
   localDataTrack.send('CONNECT_INTERPRETER');
 }

 async endCallButton() {
   // Call action for finish the call
   if (this.props.currentUser.userType === 'deaf') {
    await this.props.dispatch(actions.callsFinish({callId: this.state.roomName}));
    this.props.openEndOfCallDialog();
   }
   if (this.props.currentUser.userType === 'interpreter') {
     this.props.endCallInterpreter();
   }
   this.setState({ hasJoinedRoom: false, localMediaAvailable: false });
   if (this.state.activeRoom) {this.state.activeRoom.disconnect();}
   localDataTrack.send('END_CALL');
 }

 async startSession() {
   localDataTrack.send('START_SESSION')
 }

 async switchInterpreter() {
   localDataTrack.send('SWITCH_INTERPRETER')
 }

 stopSession() {
   localDataTrack.send('STOP_SESSION')
 }

 // Attach the Tracks to the DOM.
 attachTracks(tracks, container) {
   tracks.forEach(track => {
     if (track.kind === 'audio' || track.kind === 'video') {
       container.appendChild(track.attach());
     }
   });
 }

 // Attach the Participant's Tracks to the DOM.
 attachParticipantTracks(participant, container) {
   var tracks = Array.from(participant.tracks.values());
   this.attachTracks(tracks, container);
 }

 detachTracks(tracks) {
    tracks.forEach(track => {
      if (track.kind === 'audio' || track.kind === 'video' ) {
        track.detach().forEach(detachedElement => {
          detachedElement.remove();
        });
        // track.stop();
      }
    });
  }

 detachParticipantTracks(participant) {
   var tracks = Array.from(participant.tracks.values());
   this.detachTracks(tracks);
 }

 roomJoined(room) {
   // Called when a participant joins a room
   console.log("Joined as '" + this.state.identity + "'");
   this.setState({
     activeRoom: room,
     localMediaAvailable: true,
     hasJoinedRoom: true  // Removes ‘Join Room’ button and shows ‘Leave Room’
   });

    // Attach LocalParticipant's tracks to the DOM, if not already attached.
    var previewContainer = this.refs.localMedia;

    if (previewContainer && !previewContainer.querySelector('video')) {
      this.attachParticipantTracks(room.localParticipant, previewContainer);
    }

    // Attach the Tracks of the room's participants.
    room.participants.forEach(participant => {
      console.log("Already in Room: '" + participant.identity + "'");
      var previewContainer = this.refs.remoteMedia;
      console.log(previewContainer);
      this.attachParticipantTracks(participant, previewContainer);
    });

    // Participant joining room
    room.on('participantConnected', participant => {
      if (this.props.currentUser && this.props.currentUser.userType === 'deaf') {
        this.props.interpreterJoin();
      }
      console.log("Joining: '" + participant.identity + "' (interpreter connected)");
    });

    // Attach participant’s tracks to DOM when they add a track
    room.on('trackAdded', (track, participant) => {
      console.log(participant.identity + ' added track: ' + track.kind);
      var previewContainer = this.refs.remoteMedia;
      this.attachTracks([track], previewContainer);

      if (track.kind === 'data') {
        this.props.dispatch(actions.callsGet({id: this.state.roomName}));
        track.on('message', data => {
          switch (data) {
            case "START_SESSION":
              console.log("START_SESSION");
              this.props.dispatch(actions.callsGet({id: this.state.roomName}));
              break;
            case "END_CALL":
              console.log("END_CALL");
              this.props.dispatch(actions.callsGet({id: this.state.roomName}));
              break;
            case "SWITCH_INTERPRETER":
              console.log("SWITCH_INTERPRETER");
              if (this.props.currentUser.userType === 'interpreter') {
                this.setState({ hasJoinedRoom: false, localMediaAvailable: false });
                if (this.state.activeRoom) {this.state.activeRoom.disconnect();}
              }
              this.props.dispatch(actions.callsGet({id: this.state.roomName}));
              break;
            case "STOP_SESSION":
              console.log("STOP_SESSION");
              this.props.dispatch(actions.callsGet({id: this.state.roomName}));
              break;
            case "CONNECT_INTERPRETER":
              console.log("CONNECT_INTERPRETER");
              this.props.dispatch(actions.callsGet({id: this.state.roomName}));
              break;
            default:
              break;
          }
        });
      }

    });

    // Detach participant’s track from DOM when they remove a track.
    room.on('trackRemoved', (track, participant) => {
      console.log(participant.identity + ' removed track: ' + track.kind);
      this.detachTracks([track]);
      this.props.dispatch(actions.callsGet({id: this.state.roomName}));
    });

    // Detach all participant’s track when they leave a room.
    room.on('participantDisconnected', participant => {
      console.log("Participant '" + participant.identity + "' left the room");
      this.detachParticipantTracks(participant);
      if (this.props.callData && (this.props.callData.state === "started")) {
        this.props.dispatch(actions.callsPause({callId: this.state.roomName}));
      }
      if (this.props.callData.activeInterpreter && participant.identity === this.props.callData.activeInterpreter.uid && this.props.callData.state === "connected") {
        this.switchInterpreter();
      }
      this.setState({hasJoinedRoom: false});
    });

    // Once the local participant leaves the room, detach the Tracks
    // of all other participants, including that of the LocalParticipant.
    room.on('disconnected', () => {
      console.log("In room disconnected");
      if (this.state.previewTracks) {
        this.state.previewTracks.forEach(track => {
          track.stop();
        });
      }
      this.detachParticipantTracks(room.localParticipant);
      room.participants.forEach(this.detachParticipantTracks);
      this.setState({ activeRoom: null, hasJoinedRoom: false, localMediaAvailable: false });
      this.props.dispatch(actions.callsGet({id: this.state.roomName}));
    });
 }

 render() {
   const callData = this.props.callData ? this.props.callData : null;
   /* Controls showing of the local track Only show video track after user has joined a room else show nothing*/
   let showLocalTrack = this.state.localMediaAvailable ? (<div id="localMedia" className="flex-item"><div ref="localMedia" /> </div>) : '';

   let endCallButton = (callData.state === 'created' || callData.state === "started" || callData.state === "paused" || callData.state === "connected") ?
    (<IconButton className="endCallButton" hoveredStyle={{background: 'rgb(191, 13, 69)'}} style={style.endCallButton} onClick={() => this.endCallButton()} iconStyle={style.endCallIcon}><CallEnd/></IconButton>) : null

   return (
      <div className="flex-container">
         {showLocalTrack} {/* Show local track if available */}
         <div>
          {endCallButton}
        </div>
         {/* The following div element shows all remote media (other participant’s tracks) */}
         <div className="flex-item" ref="remoteMedia" id="remoteMedia" />
       </div>

   )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoComponent);
