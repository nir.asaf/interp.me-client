import React, { Component } from 'react';
import { connect } from 'react-redux';
import actions from '../../actions';
import Video from 'twilio-video';
import EndCallButton from '../Call/EndCallButton';
import StartCallButton from '../Call/StartCallButton';
import MicButton from '../Call/MicButton';
import VideoButton from '../Call/VideoButton';
// import SwitchInterpreterButton from '../Call/SwitchInterpreterButton';
// import PauseCallButton from '../Call/PauseCallButton';

import Draggable from 'react-draggable';
import Timer from '../Common/Timer';
import * as VideoUtil from './VideoUtil';
// import { LocalDataTrack, createLocalTracks }  from 'twilio-video';
// import CameraAlt from '@material-ui/icons/CameraAlt';
// import Mic from '@material-ui/icons/Mic';
// import Headset from '@material-ui/icons/Headset';

import './Video.css';

var dataTrack = new Video.LocalDataTrack({maxRetransmits: 1000});
// const localDataTrack = new Video.LocalDataTrack();

const mapStateToProps = state => ({
  callData: state.call.data,
  callState: state.call.callState,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class VideoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: null,
      previewTrack: null,
      identity: props.currentUser.uid,
      participantIdentity: null,
      roomName: props.callData.id,
      twillioToken: props.currentUser.twillioToken,
      currentUser: props.currentUser,
      twilioReady: false,
    }
    this.roomJoined = this.roomJoined.bind(this);
  }

  async setupLocalAudioAndVideoTracks() {
    try {
      const audioAndVideoTrack = await Video.createLocalTracks();
      const tracks = audioAndVideoTrack.concat(dataTrack);
      return tracks;
    } catch(error) {
      console.log(error);
      return [];
    }
  }

 componentDidMount() {
      var localTracksPromise = this.state.previewTracks ? Promise.resolve(this.state.previewTracks) : this.setupLocalAudioAndVideoTracks();

      localTracksPromise.then((tracks)=> {
          window.previewTracks = tracks;
          window.dataTrack = dataTrack;
          this.setState({previewTracks: tracks})
          var previewContainer = document.getElementById("localMedia");
          if (!previewContainer.querySelector("video")) {
            VideoUtil.attachTracks(tracks, previewContainer);
          }
          // If all tracks in place join room
          this.joinRoom()
        },(error)=> {
            VideoUtil.log("Unable to access Camera and Microphon");
        }
      );
  }

 async joinRoom() {
    VideoUtil.log("Joining room '" + this.state.roomName + "'...");
    var connectOptions = {
      name: this.state.roomName,
      tracks: [dataTrack],
      preferredVideoCodecs: ['H264'],
    };
    if (this.state.previewTracks) {
      connectOptions.tracks = this.state.previewTracks;
    }
    // Join the Room with the token from the server and the LocalParticipant's Tracks.
    Video.connect(this.state.twillioToken, connectOptions).then(this.roomJoined, (error)=> {
      console.log("======= Error - Video.connect ============");
      console.log(error);
      if (error.code === 20104) {
        // AccessTokenExpiredError
        this.props.unmountVideo();
      } else {
        VideoUtil.log("Could not connect to Twilio: " + error.message);
        this.state.previewTracks.forEach((track)=> {
          if (track.kind !== 'data') {
            track.stop();
          }
        });
        VideoUtil.detachTracks(this.state.previewTracks);
        this.setState({previewTracks: null});
      }
    });
  }

 roomJoined(room){
    this.setState({
      activeRoom: room,
      twilioReady: true,
    })
     window.roomName = room.name;
     window.room = room;
     VideoUtil.log("Joined as '" + this.state.identity + "'");
     if  (this.props.currentUser.userType === 'interpreter') this.props.interpreterJoinRoom();
     // Attach LocalParticipant's Tracks, if not already attached.
     var previewContainer = document.getElementById("localMedia");
     if (!previewContainer.querySelector("video")) {
       VideoUtil.attachParticipantTracks(room.localParticipant, previewContainer);
     }

     room.localParticipant.on('trackPublicationFailed', (error, localTrack) => {
       console.warn('Failed to publish LocalTrack "%s": %s', localTrack.name, error.message);
     });

     room.on('trackSubscriptionFailed', (error, remoteTrackPublication, remoteParticipant) => {
       console.warn('Failed to subscribe to RemoteTrack "%s" from RemoteParticipant "%s": %s"', remoteTrackPublication.trackName, remoteParticipant.identity, error.message);
     });

     // Attach the Tracks of the Room's Participants.
     room.participants.forEach((participant)=> {
       VideoUtil.log("Already in Room: '" + participant.identity + "'");
       var previewContainer = document.getElementById("remote-media");
       VideoUtil.attachParticipantTracks(participant, previewContainer);
     });

     // When a Participant joins the Room, log the event.
     room.on("participantConnected", (participant) => {
       VideoUtil.log("Joining: '" + participant.identity + "'");
       this.setState({participantIdentity: participant});
       if (this.props.currentUser.userType === 'deaf') {
         this.props.interpreterJoin();
       }
       participant.on('trackSubscribed', track => {
         console.log(`Subscribed to a RemoteTrack: ${track}`);
        });
     });

     // When a Participant adds a Track, attach it to the DOM.
     room.on("trackSubscribed", (track, participant) => {
       console.log(`Participant "${participant.identity}" added ${track.kind} Track ${track.sid}`);
       var previewContainer = document.getElementById("remote-media");
       if (track.kind === 'audio' || track.kind === 'video') {
         previewContainer.appendChild(track.attach());
       }
       // VideoUtil.attachTracks([track], previewContainer);
       // Communication between both sides with track data
       if (track.kind === 'data') {
         // After communication establish connect interpreter to the call
         if (this.props.currentUser && this.props.currentUser.userType === 'interpreter' && this.props.callData.state !== 'connected') {
           this.props.dispatch(actions.callsConnectInterpreter({callId: room.name}));
         }
      }
       track.on('message', (message) => {
         console.log("trackSubscribed");
         console.log(message);
         if (message === 'SWITCH_CALL' || message === 'CONNECT_INTERPRETER' || message === 'PAUSE_CALL' || message === 'START_CALL') {
           console.log("CallGet");
           this.props.dispatch(actions.callsGet({id: room.name}));
         }
         if (message === 'START_CALL') {
           this.props.dispatch(actions.startTimer());
         }
         if (message === 'SWITCH_CALL' && this.props.currentUser.userType === 'interpreter') {
           this.state.activeRoom.disconnect();
           this.openPopup();
         }
         if (message === 'PAUSE_CALL' && this.props.currentUser.userType === 'interpreter') {
           this.props.dispatch(actions.stopTimer());
         }
         if (message === 'FINISH_CALL') {
           this.props.dispatch(actions.callsGet({id: room.name}));
           this.openPopup();
           this.props.dispatch(actions.stopTimer());
           if (this.state.activeRoom) this.state.activeRoom.disconnect();
         }
       })
     })

     // When a Participant removes a Track, detach it from the DOM.
     room.on("trackUnsubscribed", (track, participant)=> {
         VideoUtil.log(participant.identity + " removed track: " + track.kind);
         VideoUtil.detachTracks([track]);
     });

     // When a Participant leaves the Room, detach its Tracks.
     room.on("participantDisconnected", (participant)=> {
         VideoUtil.log("Participant '" + participant.identity + "' left the room");
         VideoUtil.detachParticipantTracks(participant);
         if (this.state.currentUser.userType === 'interpreter') {
           this.state.activeRoom.disconnect();
           this.openPopup();
         }
         if (this.state.currentUser.userType === 'deaf') {
           this.props.interpreterJoin();
           const date = {callId: this.props.callData.id, interpreterMatch: this.props.callData.interpreterMatch}
           this.props.dispatch(actions.callsNewMatch(date));
         }

     });

     // Once the LocalParticipant leaves the room, detach the Tracks
     // of all Participants, including that of the LocalParticipant.
     room.on("disconnected", ()=> {
       VideoUtil.log("Left");
       if (this.state.previewTracks) {
         this.state.previewTracks.forEach((track)=> {
           if (track.kind !== 'data') {
             track.stop();
           }
         });
       }
       VideoUtil.detachParticipantTracks(room.localParticipant);
       room.participants.forEach(VideoUtil.detachParticipantTracks);
       this.setState({activeRoom: null});
     });

  }

 componentWillReceiveProps(nextProps) {
   console.log(nextProps.callState, this.props.callState, 'testssss');
   if (nextProps.callState !== this.props.callState) {
      switch (nextProps.callState) {
        case "connected":
          console.log("CONNECT_INTERPRETER");
          dataTrack.send('CONNECT_INTERPRETER');
          break;
        case "pause":
          dataTrack.send('PAUSE_CALL');
          break;
        case "start":
          dataTrack.send('START_CALL');
          break;
        case "switch":
          dataTrack.send('SWITCH_CALL');
          break;
        case "finish":
          console.log('Deaf call FINISH_CALL');
          dataTrack.send('FINISH_CALL');
          this.state.activeRoom.disconnect();
          break;
        default:
          break;
      }
    }
    if (nextProps.currentUser.twillioToken !== this.props.currentUser.twillioToken) {
      this.joinRoom();
    }
  }

 endCallButton = () => {
   this.setState({roomName: null});
    if (this.props.currentUser.userType === 'deaf') {
      const date = {callId: this.props.callData.id}
      this.props.dispatch(actions.stopTimer());
      this.openPopup();
      // dataTrack.send('FINISH_CALL');
      this.props.dispatch(actions.callsFinish(date));
    }else if (this.props.currentUser.userType === 'interpreter') {
      this.props.dispatch(actions.stopTimer());
      this.props.dispatch(actions.callsGet({id: this.props.callData.id}));
      this.openPopup();
      this.state.activeRoom.disconnect();
    }
 }

 openPopup = () => {
  this.props.openReviewDialog();
 }

 componentWillUnmount() {}

 render() {
   const { activeRoom } = this.state;
   const { currentUser, callData } = this.props;
   const startCallTIme = callData && callData.sessions[callData.activeSessionId] ? Date(callData.sessions[callData.activeSessionId].startTime) : Date.now();

   return (
    <div className="videoWrap">

      { callData && callData.state !== 'created' &&
        <div className="timer">
          <Timer withAmountOfBilled={currentUser.userType === 'interpreter'} startedAt={startCallTIme}/>
        </div>}
      {/* callData.activeInterpreter && currentUser.userType === 'deaf' &&
        <div className="activeInterpreter">
          <div className="name">{callData.activeInterpreter.name}</div><span style={{padding: '0 4px 0px 4px'}}>-</span>
          <div className="rate"><UserRating rating={callData.activeInterpreter.rating} /></div>
        </div>*/}
      {/* callData.caller && currentUser.userType === 'interpreter' &&
        <div className="deafUser">
          <div className="name">{callData.caller.name}</div>
        </div>*/}
      { this.state.activeRoom ? <div id="remote-media"></div> : null }
      <div id="controls">
        {/*<div id="setting">
          <CameraAlt/>
          <Mic/>
          <Headset/>
        </div>*/}
        <div id="preview">
          <Draggable bounds="body">
            <div ref="localMedia" id="localMedia"></div>
          </Draggable>
        </div>

        { activeRoom && (
          <div>
            <div ref="roomControls " id="roomControls">
              <VideoButton room={activeRoom}/>
              <EndCallButton room={activeRoom} endCallButton={this.endCallButton}/>
              <MicButton room={activeRoom}/>
            </div>

            <div className="userControls">
              { currentUser.userType === 'deaf' && (callData.state === 'connected' || callData.state === 'paused') && (<StartCallButton callData={callData} room={activeRoom}/>)}
              {/*callData.state === 'started' || callData.state === 'paused' || callData.state ===  'connected' ? <SwitchInterpreterButton callData={callData} room={activeRoom}/> : null*/}
              {/*callData.state === 'started' ? <PauseCallButton room={activeRoom}/> : null */}
            </div>
          </div>
        )}

      </div>
     </div>
   )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoComponent);
