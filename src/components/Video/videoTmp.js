// When a Participant adds a Track, attach it to the DOM.
// room.on("trackAdded", (track, participant)=> {
//     VideoUtil.log(participant.identity + " added track: " + track.kind);
//     var previewContainer = document.getElementById("remote-media");
//     VideoUtil.attachTracks([track], previewContainer);
//     // Communication between both sides with track data
//     if (track.kind === 'data') {
//       // After communication establish connect interpreter to the call
//       if (this.props.currentUser && this.props.currentUser.userType === 'interpreter' && this.props.callData.state !== 'connected') {
//         this.props.dispatch(actions.callsConnectInterpreter({callId: room.name}));
//       }
//       track.on('message', data => {
//         console.log("trackAdded");
//         console.log(data);
//         if (data === 'SWITCH_CALL' || data === 'CONNECT_INTERPRETER' || data === 'PAUSE_CALL' || data === 'START_CALL') {
//           this.props.dispatch(actions.callsGet({id: room.name}));
//         }
//         if (data === 'START_CALL') {
//           this.props.dispatch(actions.startTimer());
//         }
//         if (data === 'SWITCH_CALL' && this.props.currentUser.userType === 'interpreter') {
//           this.state.activeRoom.disconnect();
//           this.setState({openDialog: true});
//         }
//         if (data === 'PAUSE_CALL' && this.props.currentUser.userType === 'interpreter') {
//           this.props.dispatch(actions.stopTimer());
//         }
//         if (data === 'FINISH_CALL') {
//           this.setState({openDialog: true});
//           this.props.dispatch(actions.stopTimer());
//           this.state.activeRoom.disconnect();
//         }
//       })
//     }
// });
