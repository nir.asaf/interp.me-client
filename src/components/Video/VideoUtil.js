const attachTracks = (tracks, container) => {
    tracks.forEach((track)=> {
      if (track.kind !== 'data') {
        container.appendChild(track.attach());
      }
    });

}

const attachParticipantTracks = (participant, container) => {
    var tracks = Array.from(participant.tracks.values());
    attachTracks(tracks, container);
}

const detachTracks = (tracks) => {
    tracks.forEach((track)=> {
      if (track.kind !== 'data') {
        track.detach().forEach((detachedElement)=> {
            detachedElement.remove();
        });
      }
    });
}

const detachParticipantTracks= (participant) => {
    var tracks = Array.from(participant.tracks.values());
    detachTracks(tracks);
}

const log = (message) => {
  console.log(message);
}

export {
  attachTracks,
  attachParticipantTracks,
  detachTracks,
  detachParticipantTracks,
  log,
};
