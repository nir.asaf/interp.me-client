import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SelectComponent from '../Common/SelectComponent';

const styles = theme => ({
  interpretersSearch: {
    width: '100%',
  },
  gridPadding : {
    padding: 10
  },
  gridMorePaddig: {
    padding: 10,
    paddingBottom: 40
  }
})

class InterpreterSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signLanguages: '',
      spokenLanguages: '',
      specialized: '',
      experience: '',
      // reviewCount: 0,
    };
    this.filterOptions = this.filterOptions.bind(this);
  }

  filterOptions (key, value) {
    this.setState({ [key]: value, offset: 0, limit: 20});
    let { handleFilter } = this.props;

    setTimeout (()=> {
      let filters = '';
      let filterOptions = ['spokenLanguages', 'signLanguages', 'experience', 'specialized', 'reviewCount'];

      filterOptions.forEach(filter => {
        console.log(filter, this.state, this.state[filter]);
        if (this.state[filter] && this.state[filter].length) {
          filters += (filters && filters.length ? ' AND ' : '') + (filter + '_' + this.state[filter] + '>0');
        }
      })
      handleFilter(filters);
    }, 200);
  }

  render() {
    const { classes, config } = this.props;
    const { signLanguages, spokenLanguages, experience, specialized } = this.state;
    let spokenLanguagesList = [];
    if (config && config.spokenLanguages) {
      spokenLanguagesList = config.spokenLanguages.map(language => {
        return {
          label: language.name,
          value: language.id
        }
      });
    }

    let signLanguagesList = [];
    if (config && config.signLanguages) {
      signLanguagesList = config.signLanguages.map(language => {
        return {
          label: language.name,
          value: language.id
        }
      });
    }

    let experienceList = [];
    if (config && config.experienceOptions) {
      experienceList = config.experienceOptions.map(experience => {
        return {
          label: experience.name,
          value: experience.id
        }
      });
    }

    // let reviewCountList = ['1', '2', '3', '4', '5'].map(review => {
    //   return {
    //     label: review + ' Review(s)',
    //     value: review
    //   }
    // });

    let specializationList = [];
    if (config && config.specializationOptions) {
      specializationList = config.specializationOptions.map(specialization => {
        return {
          label: specialization.name,
          value: specialization.id
        }
      });
    }

    return (
      <div className={classes.interpretersSearch}>
        <Grid container spacing={16}>

          <Grid item xs={12} sm={12}>
            <SelectComponent
              label={'Spoken Languages'}
              suggestions={spokenLanguagesList}
              placeholder={'Spoken Languages'}
              onChange={(obj)=> {this.filterOptions('spokenLanguages', obj.value)}}
              value={spokenLanguages}
              selectType="single"/>
          </Grid>

          <Grid item xs={12} sm={12}>
            <SelectComponent
              label={'Sign Languages'}
              suggestions={signLanguagesList}
              placeholder={'Sign Languages'}
              onChange={(obj)=> {this.filterOptions('signLanguages', obj.value)}}
              value={signLanguages}
              selectType="single"/>
          </Grid>

          <Grid item xs={12} sm={12}>
            <SelectComponent
              label={'Experience'}
              suggestions={experienceList}
              placeholder={'Experience'}
              selectType={'single'}
              value={experience}
              onChange={(obj)=> {this.filterOptions('experience', obj.value)}}/>
          </Grid>

          <Grid item xs={12} sm={12}>
            <SelectComponent
              label={'Specialization'}
              suggestions={specializationList}
              placeholder={'Specialization'}
              selectType={'single'}
              value={specialized}
              onChange={(obj)=> {this.filterOptions('specialized', obj.value)}}/>
          </Grid>

        {/* <Grid item xs={12} sm={4} key={5}>
          <SelectComponent
            label="Review Count"
            suggestions={reviewCountList}
            placeholder={'Review Count'}
            selectType={'single'}
            value={reviewCount}
            onChange={(obj)=> {this.filterOptions('reviewCount', obj.value)}}/>
        </Grid> */}
        </Grid>
      </div>
    );
  }
}

InterpreterSearch.propTypes = {
  classes: PropTypes.object.isRequired,
  handleFilter: PropTypes.func.isRequired,
};

export default withStyles(styles, { withTheme: true })(InterpreterSearch);
