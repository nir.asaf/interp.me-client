import React from 'react';
import { connect } from 'react-redux';
import { Navbar, NavbarBrand, NavbarNav, NavbarToggler, Collapse, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu, DropdownItem  } from 'mdbreact';
import CurrentBalance from '../User/CurrentBalance'
import HomeIcon from '@material-ui/icons/Home';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import './Header.css';


const mapStateToProps = state => ({
  ...state,
  authUser: state.session.authUser,
  completeDetails: state.common.completeDetails
});

const CallView = prps => {
    return (
      <div></div>
    )
}

const LoggedOutView = props => {
  if (!props.authUser && !props.currentUser) {
    return (

      <NavbarNav right>
          <NavItem>
            <a href="http://interp.me" className="nav-link">
              Home
            </a>
          </NavItem>

          <NavItem>
            <NavLink to="/login" className="nav-link">
              Sign in
            </NavLink>
          </NavItem>

          <NavItem>
            <NavLink to="/signup" className="nav-link">
              Sign up
            </NavLink>
          </NavItem>
      </NavbarNav>

    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser && props.authUser && !(props.currentUser.registered)) {
    return (
      <NavbarNav right>
        <NavItem onClick={props.onLogout}>
            <NavLink to="#" className="nav-link mr-2">Logout</NavLink>
        </NavItem>
    </NavbarNav>
    )
  } else if (props.currentUser && props.authUser && props.currentUser.registered) {
    return (

      <NavbarNav right>
        { props.currentUser.userType === 'interpreter'?
          <NavItem>
            <NavLink to="#" className="nav-link mr-2">
              <i className="fa fa-circle fa-blink" style={{color: '#14a43b'}}></i>&nbsp; Online
            </NavLink>
          </NavItem>
          :
          null
        }
        <NavItem>
          <NavLink to="/dashboard" className="nav-link mr-2">
            <HomeIcon/> Dashboard
          </NavLink>
        </NavItem>
        {/*<NavItem className="mr-4">
          <Badge badgeContent={10} secondary={true} badgeStyle={{}} style={{padding: '0'}}>
            <IconButton tooltip="Notifications">
              <NotificationsIcon />
            </IconButton>
          </Badge>
        </NavItem>*/}
        <NavItem className="mr-2">
          <h6 className="pt-2">
            <div className="blue"
              style={{borderRadius: '2px', padding: '.25em .4em', color: '#fff', fontSize: '75%', boxShadow: '0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12)', marginTop: '2px'}}>
              <CurrentBalance currentUser={props.currentUser}/>
            </div>
          </h6>
        </NavItem>
        <NavItem>
          <Dropdown isOpen = { props.dropdownOpen } toggle = { props.toggle }>
            <DropdownToggle nav caret><AccountCircleIcon/></DropdownToggle>
            <DropdownMenu>
              <DropdownItem>
                <span>
                  <span style={{display: 'block', width: '100%', height: 'initial', paddingTop: '0', marginBottom: '-2px', paddingBottom: '0'}}>{props.currentUser.name}</span>
                  <span style={{display: 'block', width: '100%', color: '#999', height: 'initial', fontSize: '12px', padding: '0', marginTop: '-2px'}}>{props.currentUser.email}</span>
                </span>
              </DropdownItem>
              <DropdownItem divider/>
              <DropdownItem href="/settings"><SettingsIcon/>Settings</DropdownItem>
              <DropdownItem href="#" onClick={props.onLogout}><ExitToAppIcon/>Logout</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </NavItem>

        <NavItem>
          <NavLink
            to={`/@${props.authUser.uid}`}
            className="nav-link">
            {props.authUser.name}
          </NavLink>
        </NavItem>

      </NavbarNav>


    );
  }

  return null;
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false,
      dropdownOpen: false
    };
    this.onClick = this.onClick.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  onClick(){
    this.setState({
      collapse: !this.state.collapse,
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  componentWillUnmount() {
    this.setState({
      collapse: false,
    });
  }

  render() {
    const authUser = this.props.authUser;
    const currentUser = this.props.currentUser;
    const isHome = this.props.router.pathname === '/';
    const isCall = window.location.pathname.split('/')[1] === 'call';

    return (
      <header>
        {
          isCall ?
            <CallView currentUser={currentUser} authUser={authUser} />
          :
            <Navbar color={`${isHome ? 'transparent' : 'white'}`} expand="md">

              <NavbarBrand href={authUser ? '/dashboard' : '/signup'}>interp.<span style={{color: '#0f7f9c'}}>me</span> </NavbarBrand>
                { !this.state.isWideEnough && <NavbarToggler onClick = { this.onClick } />}
                <Collapse isOpen = { this.state.collapse } navbar>
                  <LoggedOutView currentUser={currentUser} authUser={authUser} />
                  <LoggedInView
                    currentUser={currentUser}
                    authUser={authUser}
                    dropdownOpen={this.state.dropdownOpen}
                    toggle={this.toggle}
                    onLogout={this.props.onLogout}
                  />
                </Collapse>
              </Navbar>
        }
      </header>
    );
  }
}

export default connect(mapStateToProps)(Header);
