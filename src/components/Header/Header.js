import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
// import CurrentBalance from '../User/CurrentBalance'
import { Link } from 'react-router-dom'
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import AccountCircle from '@material-ui/icons/AccountCircle';
import DashboardIcon from '@material-ui/icons/Dashboard';
import GroupIcon from '@material-ui/icons/Group';
import ListItem from '@material-ui/core/ListItem';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Drawer from '@material-ui/core/Drawer';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    width: '100%',
    boxShadow: '0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12)',
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  iconWithText: {
    marginRight: theme.spacing.unit,
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
  },
});


class ListItemLink extends React.Component {
  renderLink = itemProps => <Link to={this.props.to} {...itemProps} />;
  render() {
    const { icon, primary, style={} } = this.props;
    return (
      <ListItem button component={this.renderLink} style={style}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText primary={primary} />
      </ListItem>
    );
  }
}

const LinkTo = props => <Link to={props.to} {...props} />

const LoggedOutDrawer = () => (
  <List component="nav">
    <ListItem button component="a" href="http://interp.me">Home</ListItem>
    <ListItemLink to="/signup" primary="Sign up"/>
    <ListItemLink to="/login" primary="Sign in"/>
  </List>
)

const LogInDrawer = props => (
  <div>
      <ListItem>
        <Avatar><ImageIcon /></Avatar>
        <ListItemText primary={props.currentUser.name} secondary={props.currentUser.email} />
      </ListItem>
      <Divider />
      <List component="nav">
        <ListItemLink to="/dashboard" primary="Dashboard"/>
        {props.currentUser.userType === 'deaf' && <ListItemLink to="/interpreters" icon={<GroupIcon />} primary="Interpreters"/>}
        <ListItemLink to="/settings" icon={<SettingsIcon />} primary="Settings"/>
        <ListItem button onClick={props.onLogout}>
          <ListItemIcon><ExitToAppIcon /></ListItemIcon>
          <ListItemText primary="Logout" />
        </ListItem>
      </List>
  </div>
);

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      mobileOpenDrawer: false,
    };
  }

  componentDidMount() {
    this.setState({ anchorEl: null, mobileOpenDrawer: false, });
  }

  componentWillUnmount() {
    this.setState({ anchorEl: null });
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  toggleDrawer = () => {
    this.setState({
      mobileOpenDrawer: !this.state.mobileOpenDrawer,
    });
  };

  render() {
    const { classes, authUser, currentUser } = this.props;
    const { anchorEl, mobileOpenDrawer } = this.state;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="inherit">
            <Toolbar>
              {authUser && currentUser && currentUser.registered && (<IconButton color="inherit" aria-label="Open drawer" onClick={this.toggleDrawer} className={classes.navIconHide}>
                <MenuIcon />
              </IconButton>)}
              <Typography variant="title" color="inherit" className={classes.flex}>
                <span style={{color: '#999191'}}>interp.</span><span style={{color: '#0f7f9c'}}>me</span>
              </Typography>

              <Hidden smDown>
                <div>
                  {authUser && currentUser && currentUser.registered && (
                    <div>
                      <Button color="inherit" variant="text" component={LinkTo} to="/dashboard"><DashboardIcon className={classes.iconWithText}/>Dashboard</Button>
                      {currentUser.userType === 'deaf' && <Button color="inherit" variant="text" component={LinkTo} to="/interpreters"><GroupIcon className={classes.iconWithText}/>Interpreters</Button>}
                      <IconButton aria-owns={Boolean(anchorEl) ? 'menu-appbar' : null} aria-haspopup="true" onClick={this.handleMenu} color="inherit">
                        <AccountCircle />
                      </IconButton>
                      <Menu id="menu-appbar" anchorEl={anchorEl} anchorOrigin={{ vertical: 'top', horizontal: 'right',}} transformOrigin={{vertical: 'top', horizontal: 'right',}} open={Boolean(anchorEl)} onClose={this.handleClose}>
                        <MenuItem disabled>
                          <Avatar>
                            <ImageIcon />
                          </Avatar>
                          <ListItemText classes={{ primary: classes.primary }} inset primary={currentUser.name} secondary={currentUser.email}/>
                        </MenuItem>
                        <Divider style={{marginTop: '0.8rem'}}/>
                        <MenuItem onClick={this.handleClose} component={LinkTo} to="/settings">
                          <ListItemIcon>
                            <SettingsIcon />
                          </ListItemIcon>
                          <ListItemText inset primary="Settings" />
                        </MenuItem>
                        <MenuItem onClick={() => {this.handleClose(); this.props.onLogout();}}>
                          <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                          <ListItemText classes={{ primary: classes.primary }} inset primary="Logout" />
                        </MenuItem>
                      </Menu>
                    </div>
                  )}
                  {!authUser && !currentUser && (
                    <div>
                      <Button color="inherit" variant="text" href="http://interp.me">Home</Button>
                      <Button color="inherit" variant="text" component={LinkTo} to="signup">Sign up</Button>
                      <Button color="inherit" variant="text" component={LinkTo} to="/login">Sign in</Button>
                    </div>
                  )}
                </div>
              </Hidden>
              { currentUser && authUser && !(currentUser.registered) && (<div><Button onClick={this.props.onLogout} color="inherit">Logout</Button></div>)}
              <Hidden mdUp>
                <div>{authUser && currentUser && currentUser.registered && (
                    <div>
                      <IconButton component={LinkTo} to="/dashboard"><DashboardIcon className={classes.iconWithText}/></IconButton>
                      {currentUser.userType === 'deaf' && <IconButton component={LinkTo} to="/interpreters"><GroupIcon className={classes.iconWithText}/></IconButton>}
                      <IconButton component={LinkTo} to="/settings"><SettingsIcon/></IconButton>
                    </div>
                  )}
                </div>
              </Hidden>
            </Toolbar>
          </AppBar>

          <Hidden xsUp implementation="css">
            <div>
              <Drawer open={mobileOpenDrawer} onClose={this.toggleDrawer} classes={{paper: classes.drawerPaper, }} ModalProps={{keepMounted: true,}}>
                <div tabIndex={0} role="button" onClick={this.toggleDrawer} onKeyDown={this.toggleDrawer}>
                  {authUser && currentUser && currentUser.registered && <LogInDrawer currentUser={currentUser} authUser={authUser} onLogout={this.props.onLogout}/> }
                  {!authUser && !currentUser && <LoggedOutDrawer/>}
                </div>
              </Drawer>
            </div>
          </Hidden>

      </div>
    )
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Header);
