import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Payment from '../Payment/Payment';

const styles = theme => ({
	formControl: {
		margin: 0,
		width: '100%',
	},
	text: {
		display: 'flex',
		alignItems: 'flex-end',
	},
});

class PaymentForm extends React.Component {

	render() {
		const { classes, selectedCard, handlePaymentType } = this.props;

		return (
			<Grid container>
				<Grid item xs={12} className={classes.text}>
					<FormControl component="fieldset" className={classes.formControl}>
						<FormLabel style={{ color: '#000' }}>How will you pay for the call?</FormLabel>
						<Payment selectedCard={selectedCard} handlePaymentType={handlePaymentType} selectable/>
					</FormControl>
				</Grid>
			</Grid>
		);
	}
}

PaymentForm.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles))(PaymentForm);
