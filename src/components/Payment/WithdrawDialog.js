import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import NumberFormat from 'react-number-format';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import PropTypes from 'prop-types';
import actions from '../../actions';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Slider from '@material-ui/lab/Slider';
import { compose } from 'recompose';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  model: state.common.model,
  currentBalance: state.settings.credit,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      ref={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
    />
  );
}

NumberFormatCustom.propTypes = {
  onChange: PropTypes.func.isRequired,
};

class WithdrawDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'paypal',
      paypalEmail: '',
      paypalFirstName: '',
      paypalLastName: '',
      err: '',
      amount: props.currentBalance ? props.currentBalance : props.currentUser.payoutCredit / 100,
      currentBalance: props.currentBalance ? props.currentBalance : props.currentUser.payoutCredit / 100,
      finish: false,
    };
  }

  onChange = (event) => {
    const { name, value } = event.target;
    this.setState({[name]: value, err: ''});
  }

	handleClose = () => {
    this.setState({finish: false});
  	this.props.dispatch(actions.closeModal())
  };

	componentDidMount() {
		if (this.props.model === 'WithdrawDialog') {
      this.props.dispatch(actions.interpretersGetPayoutCredit());
    }
	}

  changePaymentType() {
    this.setState((oldState) => {
      return { paymentType: !oldState.paymentType};
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentBalance && nextProps.currentBalance !== this.props.currentUser.payoutCredit) {
      this.setState({currentBalance: nextProps.currentBalance})
    }
  }

  validate = (details) => {
    const {paypalEmail, paypalLastName, paypalFirstName} = details
    if(!paypalEmail || !paypalEmail.trim()) {
      this.setState({err: 'Email is Required'});
      return false;
    }
    if(!paypalLastName || !paypalLastName.trim()) {
      this.setState({err: 'Last name is Required'});
      return false;
    }
    if(!paypalFirstName || !paypalFirstName.trim()) {
      this.setState({err: 'First name is Required'});
      return false;
    }
    if(paypalEmail) {
      let emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      let res = emailReg.test(paypalEmail);
      if(!res){
        this.setState({err: 'Please enter correct email'});
      	return false;
      }
    }
    return true;
  }

  submitWithdraw = (details) => ev => {
    ev.preventDefault();
    if(this.validate(details)) {
      this.props.dispatch(actions.interpretersWithdraw(details));
      this.setState({finish: true});
    }
  }

  handleSliderChange = (event, amount) => {
    this.setState({ amount });
  };

  render () {
    const { paypalFirstName, paypalLastName, paypalEmail, err, amount, currentBalance, type, finish} = this.state;
    const { fullScreen } = this.props;
    const details = { paypalFirstName, paypalLastName, paypalEmail, amount, type};
    let disabledWithdraw = currentBalance === 0 ? true : false;
    return (
				<Dialog fullScreen={fullScreen} fullWidth open={this.props.model === 'WithdrawDialog'}>
          <DialogContent>
            { finish ?
              <Grid container spacing={16}>
                <Grid item xs={12} sm={12}>
                  <h6>We sent you email with instructions</h6>
                </Grid>
              </Grid>
            :
            <div>
              <Grid container spacing={16}>
                <Grid item xs={12} sm={12}>
                  <h3 className="mt-3 mb-3">Withdraw</h3>
                  {disabledWithdraw ? <p className="text-center">You don't have found to withdraw</p> : null}
                  <h6 className="mt-3 mb-3">Withdraw amount: {currentBalance ? "$" + currentBalance : 0}</h6>
                </Grid>
              </Grid>
              <Grid container spacing={16}>
                <Grid item xs={12} sm={4}>
                  <h5>Payment Type</h5>
                  <RadioGroup aria-label="Payment Type" name="type" value={type} onChange={this.changePaymentType}>
                    <FormControlLabel value="paypal" control={<Radio />} label="PayPal" />
                    <FormControlLabel disabled value="check" control={<Radio />} label="Bank Check (Coming Soon)" />
                  </RadioGroup>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <h5>Payment Info</h5>
                  {err ? <div className="col-sm-12 alert alert-danger" role="alert" style={{marginTop: '3px'}}>{err}</div> : null}
                  <Grid container spacing={16} style={{border: '1px solid #c3c3c3', padding: '12px', margin: '1rem 0rem 1rem 0rem', maxWidth: '350px'}}>
                    <p>Select the amount you want to withdraw</p>
                    <Grid item xs={12} sm={12}>
                      <Slider step={1} min={0} max={currentBalance} value={amount} aria-labelledby="label" onChange={this.handleSliderChange} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField style={{width: '100%'}} label="Amount" name="Amount" value={amount} onChange={(res)=> this.setState({amount: Number(res.target.value)})} InputProps={{inputComponent: NumberFormatCustom}}/>
                    </Grid>
                  </Grid>
                  <Grid container spacing={16}>
                    <Grid item xs={12} sm={12}>
                      <TextField label="Paypal Email" name="paypalEmail" helperText="The email address you use for your Paypal account" onChange={this.onChange} value={paypalEmail} disabled={disabledWithdraw}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField label="First Name" name="paypalFirstName" helperText="First name on the account" onChange={this.onChange} value={paypalFirstName} disabled={disabledWithdraw}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField label="Last Name" name="paypalLastName" helperText="Last name on the account" onChange={this.onChange} value={paypalLastName}disabled={disabledWithdraw}/>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
            }
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}>{ finish ? "Close" : "Cancel"}</Button>
            { !finish && <Button disabled={!paypalEmail || amount === 0 || !paypalFirstName || !paypalLastName} primary="true" onClick={this.submitWithdraw(details)} style={{marginRight: '1rem'}}>Submit</Button>}
          </DialogActions>
	      </Dialog>
    )
  }
}

export default compose(connect(mapStateToProps, mapDispatchToProps), withMobileDialog())(WithdrawDialog);
