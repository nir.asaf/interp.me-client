import React from 'react';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import LoadingButton from '../Common/LoadingButton';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import actions from '../../actions';
import LockIcon from '@material-ui/icons/Lock';
import GA from '../../utils/GoogleAnalytics'
import {CardElement} from 'react-stripe-elements';

const styles = theme => ({
  cardFormElement: {
    border: '1px solid #D5D5D5',
    borderColor: 'transparent',
    padding: '15px',
    borderRadius: '5px',
    boxShadow: '0 2px 4px rgba(0, 0, 0, 0.09), 0 0 0 1px rgba(0, 0, 0, 0.08)',
  }
})

const mapStateToProps = state => ({
    inProgress: state.common.inProgress,
    success: state.payment.success
})

const mapDispatchToProps = dispatch => ({
    dispatch,
});

const createOptions = () => {
  return {
    style: {
      base: {
        fontSize: '16px',
        color: '#424770',
        letterSpacing: '0.025em',
        border: 'solid 1px',
        '::placeholder': {
          color: '#aab7c4',
        },
      },
      invalid: {
        color: '#9e2146',
      },
    },
  };
};

class CardForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      err: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.success) {
      this.props.dispatch(actions.deafGetPaymentIds());
      if (this.element) {
        this.element.clear();
      }
    }
  }

  submit = (ev) => {
    ev.preventDefault();
    GA.logEvent('Payment', 'Pay button pressed', {});
    // Create Token using stripe API
    this.props.dispatch(actions.asyncStart());
    this.props.stripe.createToken().then(payload => {
      if (payload.error) {
        this.setState({err: payload.error.message});
        this.props.dispatch(actions.asyncEnd());
        return;
      }
      var data = {
        metadata: {
          last4: payload.token.card.last4,
          country: payload.token.card.country,
          brand: payload.token.card.brand,
          name: payload.token.card.name,
          exp_month: payload.token.card.exp_month,
          exp_year: payload.token.card.exp_year,
        },
        token: payload.token.id,
        provider: "stripe",
      }
      this.props.dispatch(actions.deafAddPayment(data));
    });
  }

  onReady = (el) => {
    el.focus();
  }

  render() {
    const {classes} = this.props;
    const {err = ''} = this.state;
    return (
      <React.Fragment>
        <Typography variant="subheading" gutterBottom style={{marginBottom: '1rem' ,color: '#999'}}>CREDIT OR DEBIT CARD</Typography>
        <CardElement elementRef={(c) => this.element = c} className={classes.cardFormElement} onReady={this.onReady} {...createOptions(this.props.fontSize)}/>
        {err && <div style={{color: '#b94a48'}}>{err}</div>}
        <div style={{color: '#999', fontSize: 14, marginTop: '1rem'}}>
          <span style={{position: 'relative', top: 4, paddingRight: 10}}><LockIcon style={{color: "#5ea5d7"}}/></span>
          <span>Secured by Stripe with bank-level encryption</span>
        </div>
			  <div style={{marginTop: '1.5rem'}}>
          <LoadingButton style={{width: '100%'}} onClick={this.submit} primary={true} loading={this.props.inProgress} label='Add payment'/>
        </div>
      </React.Fragment>
    );
  }
}

CardForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(CardForm);
