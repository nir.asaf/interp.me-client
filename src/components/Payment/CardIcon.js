import React from 'react';
import CreditCardIcon from '@material-ui/icons/CreditCard';

const cardBrandToName = {
	'visa': {
    name: 'visa',
    image: require("../../images/payment/visa.png"),
  },
  'mastercard': 'mastercard',
  'amex': 'american-express',
  'discover': 'discover',
  'diners': 'diners',
  'jcb': 'jcb',
  'unknown': <CreditCardIcon/>,
}

const CardIcon = (props) => {
  const {brand} = props;
  return(
    <React.Fragment>
     {brand ?
       <img src={cardBrandToName[brand.toLowerCase()].image} style={{width: 30}} alt={brand}/>
       :
       <CreditCardIcon/>
     }
    </React.Fragment>
  )
};

export default CardIcon;
