import React from 'react';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Card from './Card'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
})

class CardsDetails extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cardsDetails: props.cardsDetails
    }
  }

  removeCard = (card) => {
    this.setState(prevState => ({
      cardsDetails: prevState.cardsDetails.filter(el => el.id !== card.id )
    }));
  }

  render() {
    const {cardsDetails} = this.state;
    const {selectable, selectedCard} = this.props;
    return(
      <React.Fragment>
        { cardsDetails && cardsDetails.length ?
          <div>
            <Typography variant="headline" style={{marginBottom: '1.5rem'}}>Your credit and debit cards</Typography>
            {cardsDetails.map((card, index) => {
              return(
                <Card key={index} card={card} selectedCard={selectedCard} selectable={selectable} onRemove={this.removeCard}/>
              )
            })}
         </div>
         :
         <Typography variant="headline" style={{marginBottom: '1.5rem'}}>Set a new payment</Typography>
        }
      </React.Fragment>
    )
 }
};

CardsDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CardsDetails);
