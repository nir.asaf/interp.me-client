import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import actions from '../../actions';
import { connect } from 'react-redux';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import CardForm from './CardForm';

import {
  StripeProvider,
  Elements,
  injectStripe,
} from 'react-stripe-elements';

const styles = theme => ({
  dialog: {
    width: '100%',
  }
})

const mapStateToProps = state => ({
  model: state.common.model,
  credit: state.payment.credit,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const CF = injectStripe(CardForm);

class PaymentDialog extends React.Component {

	handleClose = () => {
  	this.props.dispatch(actions.closeModal())
  };

	componentDidMount() {
    if (this.props.model === 'PaymentDialog') this.props.dispatch(actions.deafGetChargeTypes());
	}

  componentWillUnmount() {
    if (this.props.model === 'PaymentDialog') this.props.dispatch(actions.resetCredit());
  }

  render () {
    const { fullScreen, model, credit } = this.props;
    return (
				<Dialog open={model === 'PaymentDialog'} fullWidth={true} fullScreen={fullScreen} onClose={this.handleClose} aria-labelledby="payment-dialog">
          <DialogTitle id="form-dialog-title">Buy More Minutes</DialogTitle>
          { !this.props.credit ?
						<DialogContent>
							<StripeProvider apiKey="pk_test_pHJA7WyTEBRif6JoTCSTB2V5" >
					        <Elements>
										<div>
				        			<CF handleClose={this.handleClose} handleFinish={this.handleFinish}/>
										</div>
									</Elements>
							</StripeProvider>
            </DialogContent>
            :
            <DialogContent>
              <h2 className="mt-3 mb-3">Thank you</h2>
              <h6>Your new balance is: {credit.newBalance}</h6>
              <Button className="mt-5" onClick={this.handleClose}>Close</Button>
            </DialogContent>
          }
	      </Dialog>
    )
  }
}

export default compose(withMobileDialog(), withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(PaymentDialog);
