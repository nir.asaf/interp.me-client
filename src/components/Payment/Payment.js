import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardForm from './CardForm';
import Divider from '@material-ui/core/Divider';
import CardsDetails from './CardsDetails';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import { compose } from 'recompose';
import {Elements, StripeProvider, injectStripe} from 'react-stripe-elements';
import actions from '../../actions';

const styles = theme => ({
  payment: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  divider: {
    marginTop: '2rem',
    marginBottom: '1rem',
  },
  formControl: {
		margin: 0,
		width: '100%',
	},
	text: {
		display: 'flex',
		alignItems: 'flex-end',
	},
})

const mapStateToProps = state => ({
  currentUser: state.common.currentUser ? state.common.currentUser : null,
  cardsDetails: state.payment.cardsDetails ? state.payment.cardsDetails : null,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const CF = injectStripe(CardForm);

class Payment extends React.Component {
  constructor(props) {
    super();
    this.state = { 
      isAddCardView: false,
      couponCode: '',
      couponCodeNotValid: false,
      payment: 'card'
    };
  }

  componentDidMount() {
    this.props.dispatch(actions.deafGetPaymentIds())
  }

  handleCardToggle = () => {
		this.setState({ isAddCardView: !this.state.isAddCardView })
  }
  
  checkCouponCode = () => {
		this.setState({ couponCodeNotValid: true })
	}

	setPaymentType = ev => {
    this.setState({payment: ev.target.value});
    if (this.props.handlePaymentType) {
      this.props.handlePaymentType(ev.target.value);
    }
  }
  
  render() {
    const {classes, cardsDetails, selectedCard, selectable } = this.props;
    const { isAddCardView, payment, couponCode, couponCodeNotValid } = this.state;
    return (
      <div className={classes.payment}>
        {cardsDetails && <CardsDetails selectedCard={selectedCard} selectable={selectable} cardsDetails={cardsDetails}/>}
        <Divider className={classes.divider}/>

        {!isAddCardView ?
          <Button onClick={this.handleCardToggle} size="small" variant="contained">{cardsDetails && cardsDetails.length ? 'Add/Change Payment' : 'Add a Payment'}</Button>
          : <div>
            <Typography variant="headline" style={{marginBottom: '1.5rem'}}>Add a New Payment Method<Button onClick={this.handleCardToggle} style={{float: 'right'}} size="small" variant="contained">Close</Button></Typography>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormControlLabel onChange={this.setPaymentType} value="card" control={<Radio checked={payment === 'card'} color="primary" />} label="Credit / Debit Card" />
              {payment === 'card'
                ? <StripeProvider apiKey="pk_test_pHJA7WyTEBRif6JoTCSTB2V5" >
                  <Elements>
                    <CF/>
                  </Elements>
                </StripeProvider>
                : null
              }
              <FormControlLabel onChange={this.setPaymentType} value="coupon" control={<Radio checked={payment === 'coupon'} color="primary" />} label="Use Coupon Code" />
              {payment === 'coupon'
                ? <div>
                  <TextField helperText={couponCodeNotValid ? 'Enter valid code' : ''} disabled={payment !== 'coupon'} onChange={e => this.setState({ couponCode: e.target.value })} error={couponCodeNotValid} value={couponCode} style={{ marginRight: 12 }} />
                  <Button disabled={payment !== 'coupon'} size="small" variant="contained" onClick={this.checkCouponCode}>Check Code</Button>
                </div>
                : null
              }
              <FormControlLabel disabled value="business" control={<Radio color="primary" />} label="Bill Business (Coming Soon)" />
            </FormControl>
          </div>
        }
      </div>
    );
  }
}

Payment.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Payment);
