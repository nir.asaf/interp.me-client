import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CardIcon from './CardIcon';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import actions from '../../actions';
import ConfirmDialog from '../Common/ConfirmDialog';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing.unit * 1,
    fontSize: 14,
    marginBottom: '1rem',
  },
  align: {
    display: 'flex',
    alignItems: 'end',
  },
  button: {
    width: 'auto',
    height: 'auto',
  },
  selectedTrashButton: {
    width: 'auto',
    height: 'auto',
    marginRight: 8
  },
  cardPointer: {
    cursor: 'pointer'
  },
  expireText: {
    margin: 'auto'
  },
  selectedCard: {
    background: '#1c8fea45',
    border: '1px solid #2196f3',
    borderRadius: '4px'
  }
})

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class Card extends React.Component {
  constructor(props) {
    super();
    this.state = { confirmOpen: false, };
  }

  deleteCard = card => ev => {
    ev.preventDefault()
    this.setState({ confirmOpen: true });
  }

  deleteConfirmCard = confirm => {
    this.setState({ confirmOpen: false });
    if (confirm) {
      this.props.onRemove(this.props.card);
      this.props.dispatch(actions.deafRemovePayment({ paymentId: this.props.card.id }));
    }
  }

  selectCard = ev => {
    ev.preventDefault()
    this.props.dispatch(actions.deafSelectPaymentCard(this.props.card));
  }

  render() {
    const { classes, card, onRemove, selectable, selectedCard } = this.props;
    const { confirmOpen } = this.state;
    return (
      <Paper className={classes.root}>
        {selectable ?
          <div className={classes.cardPointer} onClick={this.selectCard}>
            <Grid container spacing={16} className={selectedCard && selectedCard.id === card.id ? classes.selectedCard : ''}>
              <Grid item xs={6} className={classes.align}>
                <div style={{ paddingRight: 8 }}><CardIcon brand={card.metadata.brand} /></div>
                <span>{card.metadata.brand} ending in {card.metadata.last4}</span>
              </Grid>
              <Grid item xs style={{ textAlign: 'center' }}>
                {`${card.metadata.exp_month}/${card.metadata.exp_year}`}
              </Grid>
              {onRemove ?
                <Grid item xs style={{ textAlign: 'right ' }}>
                  <IconButton onClick={this.deleteCard(card)} className={classes.button} aria-label="Delete">
                    <DeleteIcon />
                  </IconButton>
                </Grid>
                : null
              }
            </Grid>
          </div>
          : <Grid container spacing={16}>
            <Grid item xs={6} className={classes.align}>
              <div style={{ paddingRight: 8 }}><CardIcon brand={card.metadata.brand} /></div>
              <span>{card.metadata.brand} ending in {card.metadata.last4}</span>
            </Grid>
            <Grid item xs style={{ textAlign: 'center' }}>
              {`${card.metadata.exp_month}/${card.metadata.exp_year}`}
            </Grid>
            {onRemove ?
              <Grid item xs style={{ textAlign: 'right ' }}>
                <IconButton onClick={this.deleteCard(card)} className={classes.button} aria-label="Delete">
                  <DeleteIcon />
                </IconButton>
              </Grid>
              : null
            }
          </Grid>
        }
        <ConfirmDialog text={"Are you sure you want to delete this card?"} open={confirmOpen} closeCallback={this.deleteConfirmCard} />
      </Paper>
    );
  }
}

Card.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(Card);
