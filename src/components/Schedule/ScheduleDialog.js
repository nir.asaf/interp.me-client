import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import actions from '../../actions';
import { connect } from 'react-redux';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { compose } from 'recompose';
import Schedule from './Schedule';

const mapStateToProps = state => ({
  model: state.common.model,
  credit: state.payment.credit,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class ScheduleDialog extends React.Component {

  handleClose = () => {
    this.props.dispatch(actions.closeModal())
  };

  render() {
    const { fullScreen, model } = this.props;
    return (
      <Dialog open={model === 'ScheduleDialog'} fullScreen={fullScreen} onClose={this.handleClose} aria-labelledby="payment-dialog">
         <DialogContent>
          <Schedule />
         </DialogContent>
         <DialogActions>
           <Button variant="contained" onClick={this.handleClose} color="primary">
             Close
           </Button>
         </DialogActions>
      </Dialog>
    )
  }
}

export default compose(withMobileDialog(), connect(mapStateToProps, mapDispatchToProps))(ScheduleDialog);
