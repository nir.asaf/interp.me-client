import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LanguageIcon from '@material-ui/icons/Language';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import InputAdornment from '@material-ui/core/InputAdornment';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import GenderSelect from '../User/GenderSelect';
import { compose } from 'recompose';
import Button from '@material-ui/core/Button';
import DateInput from '../Schedule/DateInput';
import Duration from '../Schedule/Duration';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SelectComponent from '../Common/SelectComponent';
import LoadingButton from '../Common/LoadingButton';
import TimeInput from '../Schedule/TimeInput';
import SelectTimezone from '../../utils/SelectTimezone';
import {findObjectByValue} from '../../utils/Helpers';
import moment from 'moment';

const styles = theme => ({
  detailsWrap: {
    [theme.breakpoints.down('sm')]: {
      order: 2,
    },
  },
  paper: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  disabled: {
    color: "#333",
  },
});


class ScheduleInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      enableEdit: false,
    }
  }

  handleChange = name => event => {
    let value = event.target && event.target.value ? event.target.value : event.value
    let newSchedule = Object.assign({}, this.props.schedule);
    if (name === 'gender') {
      if (newSchedule.interpreterMatch && newSchedule.interpreterMatch.filters) {
        newSchedule.interpreterMatch.filters.find(obj => {
          if (obj.type === name) return obj.value = value ? value : "";
          return obj;
        });
      } else {
        newSchedule.interpreterMatch.filters= [{ type: "gender", "value": value ? value : "" }]
      }
    }else {
      newSchedule[name] = value;
    }
    this.props.handleSchedule(newSchedule);
  };

  handleChangeDateAndTime = name => val => {
    let newSchedule = Object.assign({}, this.props.schedule);
    let date = this.state.date;
    let time = this.state.time;
    if (name === "time") {
      time = val;
      newSchedule.startTime.timestamp = this.state.date + " " + val
    }else if (name === "date") {
      date = val;
      newSchedule.startTime.timestamp = this.state.time + " " + val
    }
    this.props.handleChangeDateAndTime({schedule: newSchedule, date: date, time: time });
  }

  handleChangeLanguages = obj => {
    let newSchedule = Object.assign({}, this.props.schedule);
    newSchedule.interpreterMatch.spokenLanguage = obj.value
    this.props.handleSchedule(newSchedule);
  }

  enableEdit = event => {
    if (this.props.schedule && this.props.schedule.caller.id === this.props.currentUser.uid && !this.props.datePassed) {
      this.setState({enableEdit: true})
    }
  }

  updateScheduleCall = schedule => event => {
    let data = {
      scheduleId: schedule.scheduleId,
      resendInvitations: true,
      signLanguage: schedule.signLanguage,
      interpreterMatch: schedule.interpreterMatch,
      startTime: {timestamp: schedule.startTime.timestamp ,timezone: schedule.startTime.timezone},
      durationMins: schedule.durationMins,
      paymentId: schedule.paymentId,
      paymentType: 'user_payment_id',
      callType: "video",
    };
    this.props.updateScheduleCall(data);
  }

  render() {
    const { classes, spokenLanguages, signLanguages, currentUser, schedule, time, spokenLanguage, inProgress } = this.props;
    const { enableEdit } = this.state;
    
    return (
      <Grid item xs={12} sm={8} className={classes.detailsWrap}>
        <Paper elevation={1} className={classes.paper}>
          <Grid container spacing={24}>
            {!enableEdit && schedule.callerId === currentUser.uid && <Grid item xs={12} sm={12}><Button style={{float: 'right'}} onClick={this.enableEdit} variant="contained">Edit</Button></Grid>}
            <Grid item xs={12} sm={4}>
              <TimeInput enable={enableEdit} value={time} icon={true} onChange={this.handleChangeDateAndTime("time")}/>
            </Grid>
            <Grid item xs={12} sm={4}>
              <DateInput enable={enableEdit} value={moment(schedule.startTime.timestamp).format("Y-MM-D")} icon={true} onChange={this.handleChangeDateAndTime("date")}/>
            </Grid>
            <Grid item xs={12} sm={2}>
              <Duration enable={enableEdit} value={schedule.durationMins} onSelect={this.handleChange("durationMins")} durationView="text"/>
            </Grid>
            <Grid item xs={12} sm={5}>
              { !enableEdit ?
                <TextField fullWidth label="Time Zone" value={schedule.startTime.timezone} disabled={!enableEdit}
                  InputProps={{
                    disableUnderline: !enableEdit,
                    classes: {
                      disabled: classes.disabled,
                    },
                    startAdornment: <InputAdornment position="start"><LocationOnIcon/></InputAdornment>
                    }}
                  />
                :
                <SelectTimezone label="Time Zone" icon={<LocationOnIcon/>} value={schedule.startTime.timezone} onChange={this.handleChange("timezone") } selectType={'single'}/>
              }

            </Grid>
            <Grid item xs={12} sm={5}>
              { !enableEdit ?
                <TextField fullWidth label="Spoken Languages"
                  value={spokenLanguages && spokenLanguage ? findObjectByValue(spokenLanguages, spokenLanguage).label : ''}
                  disabled={!enableEdit}
                  InputProps={{
                    disableUnderline: !enableEdit,
                    classes: {
                      disabled: classes.disabled,
                    },
                    startAdornment: <InputAdornment position="start"><LanguageIcon/></InputAdornment>
                    }}
                  />
                :
                <SelectComponent icon={<LanguageIcon/>} label="Spoken Languages" suggestions={spokenLanguages} onChange={this.handleChangeLanguages} value={schedule.interpreterMatch.spokenLanguage} selectType="single"/>
              }
            </Grid>
            <Grid item xs={12} sm={8}>
              { !enableEdit ?
                <TextField fullWidth label="Sign Languages" value={signLanguages && schedule.signLanguage ? findObjectByValue(signLanguages, schedule.signLanguage).label : ''}
                  disabled={!enableEdit}
                  InputProps={{
                    disableUnderline: !enableEdit,
                    classes: {
                      disabled: classes.disabled,
                    },
                    startAdornment: <InputAdornment position="start"><Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/></InputAdornment>
                    }}
                  />
                :
                <SelectComponent icon={<Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/>} label="Sign Languages" suggestions={signLanguages} onChange={this.handleChange("signLanguage")} value={schedule.signLanguage} selectType="single"/>
              }
            </Grid>
            <Grid item xs={12} sm={12} className={classes.gender}>
              {enableEdit && <GenderSelect label="Gender" onChange={this.handleChange("gender") } value={schedule.interpreterMatch.filters ? schedule.interpreterMatch.filters.find(obj => { return obj.type === 'gender'}).value : ''}/>}
            </Grid>

            {enableEdit && <Grid item xs={12} sm={12} style={{textAlign: 'left', marginTop: '3rem'}}>
              <LoadingButton loading={inProgress} variant="contained" onClick={this.updateScheduleCall(schedule)} label="Update"/>
            </Grid> }
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

ScheduleInfo.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default compose(withStyles(styles))(ScheduleInfo);
