import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import TimerIcon from '@material-ui/icons/Timer';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  durationMinsButton: {
    borderRadius: 0,
  },
  disabled: {
    color: "#333",
  },
})

class Duration extends React.Component {
  state = {
    durationMins: this.props.value,
  }

  handleChange = event => {
   this.setState({ [event.target.name]: event.target.value });
   this.props.onSelect(event)
  };

  handleClick = val => event => {
   this.setState({ durationMins: val });
   this.props.onChange(val)
  };

  render() {
    const {classes, enable=false, durationView = 'button'} = this.props;
    const {durationMins} = this.state;

    if (durationView === 'button') {
      return (<div>
                <Button className={classes.durationMinsButton} variant="contained" disabled={durationMins === 15} onClick={this.handleClick(15)}>15 min</Button>
                <Button className={classes.durationMinsButton} variant="contained" disabled={durationMins === 30} onClick={this.handleClick(30)}>30 min</Button>
                <Button className={classes.durationMinsButton} variant="contained" disabled={durationMins === 60} onClick={this.handleClick(60)}>60 min</Button>
                <Button className={classes.durationMinsButton} variant="contained" disabled={durationMins === 120} onClick={this.handleClick(120)}>2hrs+</Button>
              </div>);
    } else if (durationView === 'select') {
      return (<div>
                <Select fullWidth value={durationMins} onChange={this.handleChange} inputProps={{name: 'durationMins',}}>
                  <MenuItem value={15}>15 min</MenuItem>
                  <MenuItem value={30}>30 min</MenuItem>
                  <MenuItem value={60}>60 min</MenuItem>
                  <MenuItem value={120}>120hrs+</MenuItem>
                </Select>
              </div>)
    } else {
      return (<div>
                <TextField label="Duration"
                           fullWidth
                           value={durationMins}
                           disabled={!enable}
                           type="number"
                           InputProps={{
                             disableUnderline: !enable,
                             classes: {
                               disabled: classes.disabled,
                             },
                             startAdornment: (<InputAdornment position="start"><TimerIcon/></InputAdornment>),
                             max: 300
                           }}
                           name="durationMins"
                           onChange={this.handleChange}/>
              </div>)
    }
  }
}

Duration.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Duration);
