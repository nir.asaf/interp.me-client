import React from 'react';
import PropTypes from 'prop-types';
import InputAdornment from '@material-ui/core/InputAdornment';
import DateRangeIcon from '@material-ui/icons/DateRange';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import FormHelperText from '@material-ui/core/FormHelperText';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  disabled: {
    color: "#333",
  },
})

class TimeInput extends React.Component {
  state = {
    date: this.props.value,
    dateError: null,
  }

  handleTimeChange = val => event => {

  };

  render() {
    const {icon=false, enable=false, classes} = this.props;
    const {date, dateError} = this.state;
    return (
      <div>
        <TextField fullWidth
                   label="Date"
                   error={dateError}
                   type="date"
                   value={date}
                   disabled={!enable}
                   InputLabelProps={{shrink: true,}}
                   InputProps={{
                     classes: {
                       disabled: classes.disabled,
                     },
                     disableUnderline: !enable,
                     startAdornment: icon && (<InputAdornment position="start"><DateRangeIcon/></InputAdornment>),
                   }}
                   onChange={(event) => {
                     if (moment(event.target.value).isAfter(moment().format())){
                       this.setState({date: event.target.value, err: '', dateError: false});
                       this.props.onChange(event.target.value)
                       return;
                     }
                     this.setState({date: '', dateError: true});
                   }}
                   />
         {dateError && (<FormHelperText error>Please select a valid date</FormHelperText>)}
      </div>
    )
  }
}

TimeInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimeInput);
