import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';
import { Link } from 'react-router-dom'
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import DateRangeIcon from '@material-ui/icons/DateRange';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import SpokenLanguageSelect from '../User/SpokenLanguageSelect';
import Icon from '@material-ui/core/Icon';
import SelectComponent from '../Common/SelectComponent';
import Duration from '../Schedule/Duration';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Typography from '@material-ui/core/Typography';
import SelectTimezone from '../../utils/SelectTimezone';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PaymentForm from '../Payment/PaymentForm';
import LoadingButton from '../Common/LoadingButton';
import actions from '../../actions';
import moment from 'moment';
import GA from '../../utils/GoogleAnalytics'

const styles = theme => ({
  textField: {
    paddingRight: theme.spacing.unit,
    marginBottom: '2rem',
  },
  wrapper: {
    maxWidth: 550,
    marginTop: '2rem',
  },
  buttonWrap: {
    margin: '2rem 0px 2rem',
    textAlign: 'center',
    fontSize: '0.8rem',
  },
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing.unit * 2,
    },
  },
  text: {
    marginBottom: '2rem'
  },
  pra: {
    paddingTop: '0.5rem',
  },
  gridWrap: {
    marginBottom: '1rem',
    marginTop: '1rem',
  },
  button: {
    margin: theme.spacing.unit,
  },
  gender: {
    marginTop: '1rem',
  },
  timeZone: {
    paddingTop: '14px',
    marginRight: '14px'
  },
  durationMinsButton: {
    borderRadius: 0,
  },
});

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  signLanguages: state.common.config.signLanguages,
  authUser: state.session.authUser,
  inProgress: state.common.inProgress,
  cardsDetails: state.payment.cardsDetails ? state.payment.cardsDetails : null,
  selectedCard: state.payment.selectedCard ? state.payment.selectedCard : null,
  signLanguageList: state.common.config && !state.common.config.signLanguages ? [] : state.common.config.signLanguages,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const LinkTo = props => <Link to={props.to} {...props} />

class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: '',
      date: '',
      spokenLanguage: '',
      signLanguage: this.props.currentUser && this.props.currentUser.signLanguages[0] ? this.props.currentUser.signLanguages[0] : [],
      dateError: false,
      gender: '',
      durationMins: '',
      timezone: null,
      callType: 'scheduled_video',
      context: '',
      type: null,
      err: '',
      payment: 'card'
    }
  }

  componentDidMount() {
    if (!(this.props.cardsDetails && this.props.cardsDetails.length)) {
      this.props.dispatch(actions.deafGetPaymentIds())
    } else {
      this.props.dispatch(actions.deafSelectPaymentCard(this.props.cardsDetails[0]));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cardsDetails && nextProps.cardsDetails.length && !nextProps.selectedCard) {
      this.props.dispatch(actions.deafSelectPaymentCard(nextProps.cardsDetails[0]));
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handlePaymentType = (payment) => {
    this.setState({ payment: payment })
  }

  handleSignLanguageSelection = (selection) => {
    // send event to GA
    var languageLabel = selection ? selection.label : 'none';
    GA.logEvent('Start Call', 'Sign language selected', {
      label: languageLabel
    });
    selection = selection.length === 0 ? "" : selection;
    console.log(selection);
    this.setState({ signLanguage: selection });
  };

  createScheduleCreate = call => event => {
    const { selectedCard } = this.props;
    const { gender} = this.state;
    GA.logEvent('Schedule Call', '"Book A Call" button pressed');
    if (gender === '' && call.interpreterMatch.filters) {
      var filters = call.interpreterMatch.filters;
      filters.some((item, index) => {
        return filters[index]['type'] === 'gender' ? !!(filters.splice(index, 1)) : false;
      })
      call.interpreterMatch.filters = filters;
    }
    call.paymentType= 'user_payment_id';

    if (selectedCard) {
      call.paymentId = selectedCard.id;
    }

    console.log(call);
    this.props.dispatch(actions.scheduleCreate(call))
  }

  validateBookCall = () => {
    let { selectedCard } = this.props;
    let { spokenLanguage, time, date, durationMins, timezone } = this.state;

    if (!(spokenLanguage && time && date && durationMins && timezone)) return false;

    if (!selectedCard) return false;

    return true;
  }

  render() {
    const { classes, selectedCard, currentUser, signLanguageList } = this.props;
    const {err, dateError, time, date, gender, durationMins, timezone, callType, payment, spokenLanguage, signLanguage, context} = this.state;
    const interpreterMatch = {
      spokenLanguage: spokenLanguage,
        filters: [{ type: "gender", "value": gender }]
    }
    const call = {
      signLanguage: signLanguage,
      durationMins,
      callType,
      context,
      startTime: {
        timestamp: date + ' ' + time,
        timezone: timezone
      },
      interpreterMatch: interpreterMatch
    };

    const signLanguagesFilterList = [];
    currentUser.signLanguages.forEach(signLang => {
      let language = signLanguageList.find((lang) => lang.id === signLang);
      if (language) {
        signLanguagesFilterList.push({ label: language.name, value: language.id });
      }
    });

    return (
        <div className={classes.wrapper}>
          <Typography variant='headline' align="center" className={classes.text}>
            Schedule A Call
          </Typography>

            <Grid container>
              {err && (
                <Grid container justify="center" className={classes.buttonWrap}>
                  <div className={classes.error}>{err}</div>
                </Grid>)}

              <Grid item xs={12} sm={4} style={{alignItems: 'flex-end', display: 'flex'}} className={classes.gridWrap}>
                <h6>Sign language</h6>
              </Grid>
              <Grid item xs={12} sm={8} className={classes.gridWrap}>
                <SelectComponent icon={<Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/>} label="" suggestions={signLanguagesFilterList} onChange={this.handleSignLanguageSelection} value={signLanguage} selectType="single"/>
              </Grid>
              <Grid item xs={12} sm={4} className={classes.gridWrap} style={{alignItems: 'flex-end', display: 'flex'}}>
                <h6>Spoken language</h6>
              </Grid>
              <Grid item xs={12} sm={8} className={classes.gridWrap}>
                <SpokenLanguageSelect label="" type="single" onChange={val => {this.setState({spokenLanguage: val})}}/>
              </Grid>

              <Grid item xs={12} className={classes.gridWrap}>
                <Divider/>
              </Grid>

              {/*<Grid container justify="center" spacing={8} className={classes.buttonWrap}>
                <Grid item>
                  <Button variant="contained" disabled={type === 'volunteer'} className={classes.button} onClick={() => this.setState({type: "volunteer"})}>Volunteer</Button>
                  <p className={classes.pra}>(Free)</p>
                </Grid>
                <Grid item>
                  <Button variant="contained" disabled={type === 'certified'} className={classes.button} onClick={() => this.setState({type: "certified"})}>Certified</Button>
                  <p className={classes.pra}>($1 a minute)</p>
                </Grid>
              </Grid>*/}
              <Grid container justify="center" className={classes.gridWrap} spacing={8} alignItems="center">
                <Grid item xs={12} sm={5} className={classes.gridWrap}>How long is the session</Grid>
                <Grid item xs={12} sm={7} className={classes.gridWrap}>
                  <Duration onChange={val => this.setState({durationMins: val})}/>
                </Grid>
              </Grid>

              <Grid item xs={12} className={classes.gridWrap}>
                <Divider/>
              </Grid>

              <Grid container spacing={16} justify="center" className={classes.gridWrap}>
                <Grid item xs={12} sm={12} className={classes.gridWrap}>Time & Date</Grid>
                <Grid item xs={12} sm={3}>
                  <TextField fullWidth label="Time" type="time" value={time} InputProps={{step: 300, startAdornment: (<InputAdornment position="start"><AccessTimeIcon/></InputAdornment>),}}
                    onChange={event => this.setState({time: event.target.value}) } InputLabelProps={{shrink: true,}}/>
                </Grid>
                <Grid item xs={12} sm={4}>
                  <TextField fullWidth label="Date" error={dateError} type="date" value={date} InputLabelProps={{shrink: true,}}
                    InputProps={{startAdornment: (<InputAdornment position="start"><DateRangeIcon/></InputAdornment>),}}
                    onChange={(event) => {
                      if (moment(event.target.value).isAfter(moment().format())){
                        this.setState({date: event.target.value, err: '', dateError: false});
                        return;
                      }
                      this.setState({date: '', err: "Please select a valid date", dateError: true});
                    }}/>
                  {dateError && (<FormHelperText error>Please select a valid date</FormHelperText>)}
                </Grid>
                <Grid item xs={12} sm={4}>
                  <SelectTimezone value={timezone} onChange={(res) => this.setState({timezone: res.value }) } selectType={'single'} label="Time Zone" placeholder=""/>
                </Grid>
              </Grid>

              <Grid item xs={12} className={classes.gridWrap}>
                <Divider/>
              </Grid>

              <Grid container>
                <Grid item xs={12} sm={12} className={classes.gender}>
                  <Grid item xs={12} sm={5} className={classes.gridWrap}>Gender</Grid>
                  <RadioGroup aria-label="gender" name="gender" value={gender} onChange={this.handleChange('gender')} row>
                    <FormControlLabel style={{marginBottom: '0'}} value="f" control={<Radio color="primary"/>} label="Female only" />
                    <FormControlLabel style={{marginBottom: '0'}} value="m" control={<Radio color="primary"/>} label="Male only" />
                    <FormControlLabel style={{marginBottom: '0'}} value="" control={<Radio color="primary"/>} label="Any" />
                  </RadioGroup>
                </Grid>
              </Grid>

              <Grid item xs={12} className={classes.gridWrap}>
                <Divider/>
              </Grid>

              <Grid container className={classes.gridWrap}>
                <Grid item xs={12} sm={12}>
                  <h6>What is the context?</h6>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <TextField fullWidth value={context} multiline rows="4" margin="normal"
                    onChange={event => {
                        if (event.target.value.length <= 200) this.setState({context: event.target.value});
                      }
                    }/>
                  <FormHelperText style={{textAlign: 'right'}}>{this.state.context.length}/200</FormHelperText>
                </Grid>
              </Grid>

              <Grid container className={classes.gridWrap}>
                <Grid item xs={12} sm={5} className={classes.gridWrap}>Payment</Grid>
                <Grid item xs={12} sm={12}>
                  <PaymentForm payment={payment} selectedCard={selectedCard} handlePaymentType={this.handlePaymentType} />
                </Grid>
              </Grid>

              <Grid container justify="center" className={classes.gridWrap} style={{textAlign: 'center'}}>
                <LoadingButton variant="contained" color="primary" onClick={this.createScheduleCreate(call)} loading={this.props.inProgress}
                  label="Book A Call" disabled={!this.validateBookCall()}/>
              </Grid>
              <Grid container justify="flex-end" className={classes.gridWrap}>
                <Grid item>
                  <Button size="small" color="primary" component={LinkTo} to="/dashboard">I need an interpreter NOW</Button>
                </Grid>
              </Grid>
            </Grid>

      </div>
    );
  }
}

Schedule.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(Schedule);
