import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LoadingButton from '../Common/LoadingButton';

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class ScheduleViewInterpreter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: props.schedule,
      interpreterDecision: null,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedule && !nextProps.schedule.error) {
      this.setState({ schedule: nextProps.schedule });
    }
  }

  reject = event => {
    if (this.props.currentUser.userType === 'interpreter' && this.props.schedule) {
      this.setState({ interpreterDecision: 'reject' })
      this.props.scheduleReject({ scheduleId: this.props.schedule.scheduleId });
    }
  }

  accept = event => {
    if (this.props.currentUser.userType === 'interpreter' && this.props.schedule) {
      this.setState({ interpreterDecision: 'accept' })
      this.props.scheduleAccept({ scheduleId: this.props.schedule.scheduleId });
    }
  }

  render() {
    const { classes, inProgress } = this.props;
    const { schedule, interpreterDecision } = this.state;

    return (
      <Paper elevation={1} className={classes.paper} style={{ borderTop: '1px solid #1976d2', paddingRight: 12, paddingLeft: 12 }}>
        {schedule.invitationState && schedule.invitationState === "accepted" ?
          <div>
            <h4>Call was accepted, please wait for the call to start</h4>
            <LoadingButton size="small" loading={inProgress} onClick={this.reject} label="Reject" />
          </div>
          :
          <div>
            <h6>{schedule.caller.name} would like to invite you to the call</h6>
            <Grid container spacing={16} style={{ paddingTop: '1rem' }}>
              <Grid item xs={6}>
                {interpreterDecision !== 'reject' && <LoadingButton size="small" color="primary" style={{ width: '100%' }} loading={inProgress} onClick={this.accept} label="Accept" />}
              </Grid>
              <Grid item xs={6}>
                {interpreterDecision !== 'accept' && <LoadingButton size="small" style={{ width: '100%' }} loading={inProgress} onClick={this.reject} label="Reject" />}
              </Grid>
            </Grid>
          </div>
        }
      </Paper>
    );
  }
}

ScheduleViewInterpreter.propTypes = {
  classes: PropTypes.object.isRequired,
  schedule: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
};

export default compose(withRouter, withStyles(styles))(ScheduleViewInterpreter);
