import React from 'react';
import PropTypes from 'prop-types';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  disabled: {
    color: "#333",
  },
})

class TimeInput extends React.Component {
  state = {
    time: this.props.value,
  }

  handleTimeChange = val => event => {

  };

  render() {
    const {icon=false, enable=false, classes} = this.props;
    const {time} = this.state;
    return (
      <div>
        <TextField fullWidth
                   label="Time"
                   type="time"
                   value={time}
                   disabled={!enable}
                   onChange={event => {
                      this.props.onChange(event.target.value)
                      this.setState({time: event.target.value});
                    }
                   }
                   InputProps={{
                     classes: {
                       disabled: classes.disabled,
                     },
                     disableUnderline: !enable,
                     step: 300,
                     startAdornment: icon && (<InputAdornment position="start"><AccessTimeIcon/></InputAdornment>),
                   }}
                   InputLabelProps={{shrink: true,}}
        />
      </div>
    )
  }
}

TimeInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimeInput);
