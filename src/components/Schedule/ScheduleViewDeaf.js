import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import CardContent from '@material-ui/core/CardContent';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class ScheduleViewDeaf extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schedule: props.schedule,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.schedule && !nextProps.schedule.error) {
      this.setState({ schedule: nextProps.schedule });
    }
  }

  render() {
    const { classes } = this.props;
    const { schedule } = this.state;

    if (!schedule) return null;
    
    return (
      <Paper elevation={1} className={classes.paper} style={{ borderTop: '1px solid #1976d2', paddingRight: 0, paddingLeft: 0, paddingBottom: 0, }}>
        <h3>Status</h3>
        {schedule.activeInterpreter ?
          <CardContent className={classes.content}>
            <h4>Interpreter</h4>
            <Typography variant="headline" component={Link} to={`/profile/${schedule.activeInterpreter.id}`}>{schedule.activeInterpreter.name}</Typography>
            <Typography variant="subheading" color="textSecondary"></Typography>
          </CardContent>
          :
          <CardContent><h5>No interpreter accepted the call yet, return soon to get an update</h5></CardContent>
        }
      </Paper>
    );
  }
}

ScheduleViewDeaf.propTypes = {
  classes: PropTypes.object.isRequired,
  schedule: PropTypes.object.isRequired,
};

export default compose(withRouter, withStyles(styles))(ScheduleViewDeaf);
