import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  wrapper: {
    position: 'relative',
    width: '100%',
    display: 'inline',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

class LoadingButton extends React.Component {
    render () {

      const {
            classes,
            label='',
            className='',
            size='large',
            loading=false,
            color="primary",
            onClick,
            type='',
            style={},
            variant='contained',
            disabled=false,
            fullWidth = false,
        } = this.props;

    return (
      <div className={classes.wrapper}>
        <Button fullWidth={fullWidth} type={type} size={size} variant={variant} className={className} style={style} disabled={disabled || loading} onClick={onClick} color={color}>
          {label}
        </Button>
        {loading && <CircularProgress size={25} thickness={3} className={classes.buttonProgress}/>}
      </div>
    )
  }
}

LoadingButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoadingButton);
