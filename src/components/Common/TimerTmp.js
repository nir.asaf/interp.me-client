import React from 'react';

const style = {
  timer: {
    display: 'block',
    right: '0',
    textAlign: 'center',
    color: '#fff',
  }
}

class Timer extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        elapsed: 0
      }
      this.tick = this.tick.bind(this);
    }

    componentDidMount() {
      this.timer = setInterval(this.tick, 50);
    }

    componentWillUnmount() {
      clearInterval(this.timer);
    }

    tick() {
      this.setState({elapsed: new Date() - Date.parse(this.props.start)});
    }

    render() {
      var elapsed = Math.round(this.state.elapsed / 100);
      var seconds = (elapsed / 10).toFixed(1);
      var minutes = Math.floor(seconds / 60)+':'+Math.floor(seconds % 60);

      return (
        <div style={style.timer}>
          <span className = "timer">{minutes} minutes</span>
        </div>
      );
    }
}

export default Timer;
