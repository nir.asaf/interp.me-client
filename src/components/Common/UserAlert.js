import React from 'react';
import InfoIcon from '@material-ui/icons/Info';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import actions from '../../actions';

const styles = theme => ({
  snackbar: {
    width: '100%',
    maxWidth: '100%',
    position: 'relative',
    backgroundColor: theme.palette.primary.dark,
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
    marginRight: theme.spacing.unit,
    [theme.breakpoints.down('sm')]: {
      position: 'absolute',
      left: '4px',
    },
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
  messageWrap: {
    width: '100%',
    padding: '4px',
  },
  action: {
    right: 0,
    top: 0,
  },
  close: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  button: {
    padding: '9px',
    fontSize: '12px',
    marginLeft: '10px',
    [theme.breakpoints.down('sm')]: {
      marginTop: '4px',
      padding: 0,
    }
  },
});

const mapStateToProps = state => ({
  ...state.auth,
  authUser: state.session.authUser,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class UserAlert extends React.Component {
  constructor(props) {
    super(props);
    this.resendVerificationLink = this.resendVerificationLink.bind(this);
    this.state = {
      verificationSent: false,
      openMessage: true,
    }
  }

  resendVerificationLink(){
    this.setState({verificationSent: true});
    this.props.dispatch(actions.sendEmailVerification());
  }

  handleClose = (event) => {
    this.setState({ openMessage: false });
  }

  render() {
    const {authUser, classes} = this.props;
    const {verificationSent, openMessage} = this.state;

    return (
      <React.Fragment>
      { authUser
        && authUser.providerData
        && authUser.providerData[0]
        && authUser.providerData[0].providerId === 'password'
        && !authUser.emailVerified ?
        <div>{ openMessage && (
            <SnackbarContent
              aria-describedby="client-snackbar"
              className={classes.snackbar}
              classes= {{action: classes.action, message: classes.messageWrap}}
              message={
                <span id="client-snackbar" className={classes.message}>
                  <InfoIcon className={classes.icon} />
                  <span className={classes.text}>Please check your inbox and verify your email address at {authUser.email}</span>
                  {!verificationSent && (<Button className={classes.button} color="inherit" onClick = { this.resendVerificationLink }>Resend verification link</Button>) }
                </span>
              }
              action={[
                <IconButton key="close" aria-label="Close" color="inherit" className={classes.close} onClick={this.handleClose}>
                  <CloseIcon className={classes.icon} />
                </IconButton>,
              ]}
            />)}
          </div>
      :
        null
      }
      </React.Fragment>
    )

  }
}

UserAlert.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(UserAlert);
