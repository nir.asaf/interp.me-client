/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import {findObjectByValue, findObjectsByValue} from '../../utils/Helpers';
import { emphasize } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    height: 'auto',
    touchAction: 'none',
  },
  chipLabel: {
    whiteSpace: 'initial',
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    fontSize: 16,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  placeholderWithIcon: {
    position: 'absolute',
    left: 34,
    fontSize: 16,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  fotter: {
    width: '110px',
    height: '20px',
    zIndex: 3,
    margin: 10,
    float: 'right',
    textAlign: 'right',
    fontSize: 0,
    lineHeight: 0,
  },
  image: {
    float: 'right',
    padding: 10,
    textAlign: 'right',
  },
  menu: {
    maxHeight: 165,
    overflowY: 'scroll',
    display: 'block',
    border: '1px solid #ccc',
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      fullWidth
      autoFocus={true}
      onKeyPress={(e) => props.selectProps.onKeyPress && props.selectProps.onKeyPress(e.target.value)}
      InputProps={{
        inputComponent,
        startAdornment: props.selectProps.icon && (
          <InputAdornment position="start">
            {props.selectProps.icon}
          </InputAdornment>
        ),
        inputProps: {
          className: props.selectProps.classes.input,
          ref: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
    />

  );
}

function Option(props) {
  return (
    <MenuItem
      key={props.children}
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.icon ? props.selectProps.classes.placeholderWithIcon : props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      key={props.data.value ? props.data.value : Math.floor(Math.random() * 100)}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      classes={{
        label: props.selectProps.classes.chipLabel
      }}
      clickable={true}
      onDelete={event => {
        if (event.target.tagName.toLowerCase() === 'svg'){
          if (props.selectProps.restriction && props.selectProps.value.length < 2){
            props.selectProps.callback("delete");
            return;
          }
          props.removeProps.onClick();
          props.removeProps.onMouseDown(event);
        }
      }}
      onTouchStart={event => {
        if (event.target.tagName.toLowerCase() === 'svg'){
          if (props.selectProps.restriction && props.selectProps.value.length < 2){
            props.selectProps.callback("delete");
            return;
          }
          props.removeProps.onClick();
          props.removeProps.onMouseDown(event);
        }
      }}
    />
  );
}

function MenuList(props) {
  return (
      <div className={props.selectProps.classes.menu}>
        {props.children}
        {props.selectProps.withFooter && <div className={props.selectProps.classes.footer}><img className={props.selectProps.classes.image} src={props.selectProps.withFooter} alt="Algolia"/></div>}
      </div>
  );
}

const components = {
  Option,
  Control,
  NoOptionsMessage,
  Placeholder,
  SingleValue,
  MultiValue,
  ValueContainer,
  MenuList
};

class SelectComponent extends React.Component {
  state = {
    single: this.props.selectType === 'single' && this.props.value ? findObjectByValue(this.props.suggestions, this.props.value) : null,
    multi:  this.props.selectType === 'multi' && this.props.value ? findObjectsByValue(this.props.suggestions, this.props.value) : null,
  };

  handleChange = name => value => {
    this.setState({
      [name]: value,
    });
    this.props.onChange(value);
  };

  render() {
    const { classes, callback, withFooter=null, key="", restriction=false, suggestions, isLoading, selectType,  placeholder, fullWidth=true, label=null, icon=null, onKeyPress=null } = this.props;
    return (
      <div className={classes.root}>
        { label && label.length ? <InputLabel style={{transform: 'translate(0, 1.5px) scale(0.75)', transformOrigin: 'top left', float: 'left', margin: 0}} htmlFor="selectComponent">{label}</InputLabel> : null }
        <Select
          fullWidth={fullWidth}
          classes={classes}
          clearable={false}
          isLoading={isLoading}
          options={suggestions}
          components={components}
          restriction={restriction}
          callback={callback}
          key={key}
          value={this.state[selectType]}
          onChange={this.handleChange(selectType)}
          placeholder={placeholder}
          icon={icon}
          withFooter={withFooter}
          onKeyPress={onKeyPress}
          isMulti={selectType === "multi"}
          inputProps={{id: 'selectComponent'}}
        />

      </div>
    );
  }
}

SelectComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SelectComponent);
