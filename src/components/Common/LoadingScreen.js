import React from 'react';

const style = {
  background: {
    position: 'fixed',
    opacity: '0.8',
    height: '100%',
    width: '100%',
    background: 'white',
    zIndex: '9999',
    touchAction: 'none',
  },
  text: {
    height: '100%',
    width: '100%',
    display: 'flex',
    position: 'fixed',
    alignItems: 'center',
    justifyContent: 'center',
  }
}

export default class LoadingScreen extends React.Component {
  render () {
      const {
        text = '',
      } = this.props;
      return (
        <div style={style.background}><h1 style={style.text}>{text}</h1></div>
      )
  }
}
