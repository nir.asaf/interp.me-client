import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

const styles = theme => ({
  root: {
    // display: 'flex',
  },
  reviewsWrap: {
    paddingLeft: '2px',
    verticalAlign: 'top',
  },
});

class UserRating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: props.rating ? props.rating : 0,
    };
  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({rating: nextValue});
    if (this.props.onStarsRate) this.props.onStarsRate(nextValue);
  }

  render () {
    const {
          classes,
          style,
          starsSize=12,
          name="UserRating",
          editing=false,
          showReview= false,
          reviewCount=0,
      } = this.props;
    return (
      <div className={classes.root} style={{fontSize:  `${starsSize}px`}}>
        <StarRatingComponent
          name={name}
          emptyStarColor="#b1aeae"
          editing={editing}
          style={style}
          value={this.state.rating}
          onStarClick={this.onStarClick.bind(this)}/>
        {showReview ? <Link to="/settings#reviews" className={classes.reviewsWrap} >({reviewCount} reviews)</Link> : null}
      </div>
    )
  }
}

UserRating.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserRating);
