import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import AmountOfBilled from '../Call/AmountOfBilled';
import TimerIcon from '@material-ui/icons/Timer';

const style = {
  timer: {
    display: 'block',
    right: '0',
    textAlign: 'center',
    color: '#fff',
  }
}

const mapStateToProps = state => ({
  baseTime: state.call.baseTime,
  startedAt: state.call.startedAt,
  stoppedAt: state.call.stoppedAt
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class Timer extends React.Component {

  componentDidMount() {
    this.interval = setInterval(this.forceUpdate.bind(this), this.props.updateInterval);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  // Helper function that takes store state
  // and returns the current elapsed time
  getElapsedTime(baseTime, startedAt, stoppedAt = new Date().getTime()) {
    if (!startedAt) {
      return 0;
    } else {
      return stoppedAt - startedAt + baseTime;
    }
  }

  // humanizeDuration(input, units ) => {
  //   // units is a string with possible values of y, M, w, d, h, m, s, ms
  //   var duration = moment().startOf('day').add(units, input),
  //     format = "";
  //
  //   if(duration.hour() > 0){ format += "H [hours] "; }
  //
  //   if(duration.minute() > 0){ format += "m [minutes] "; }
  //
  //   format += " s [seconds]";
  //
  //   return duration.format(format);
  // }

  render() {
    const { baseTime, startedAt, stoppedAt, showIcon = true, withAmountOfBilled, withOutTimer } = this.props;
    const elapsed = this.getElapsedTime(baseTime, startedAt, stoppedAt);

    return (
      <div>
        {!withOutTimer ? <div style={style.timer}>
          { showIcon && <TimerIcon style={{verticalAlign: 'bottom', paddingRight: '4px'}}/>}
          {moment.utc(elapsed).format('HH:mm:ss')}
        </div>
        : null 
        }
        {withAmountOfBilled ? <AmountOfBilled time={elapsed}/> : null}
      </div>
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
