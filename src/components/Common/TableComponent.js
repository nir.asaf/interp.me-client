import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import moment from 'moment';
import actions from '../../actions';
import Paper from '@material-ui/core/Paper';
import { compose } from 'recompose';
import { connect } from 'react-redux';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

const mapStateToProps = state => ({
  callHistory: state.settings.callHistory,
  scheduleList: state.schedule.scheduleList,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

// Schedule List Table
const ScheduleList = props => {
  const { rowsPerPage, page, getSorting, order, orderBy } = props;
  let schedules = []
  if (props.data)  {
    schedules = props.data.map((item) => ({
      startTime: new Date(item.startTime.timestamp),
      timestamp: item.startTime.timestamp,
      duration: parseFloat(item.durationMins),
      durationMins: item.durationMins,
      scheduleId: item.scheduleId
    }));
  }
  return (
    schedules && schedules.sort(getSorting(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map( (data, index) => {
      return (<TableRow key={index}>
        <TableCell>{data.timestamp}</TableCell>
        <TableCell>{data.durationMins}</TableCell>
        <TableCell><Link to={`/schedule/${data.scheduleId}`}>View</Link></TableCell>
      </TableRow>)
    })
  )
}

// Call History Table
const CallHistory = props => {
  const { rowsPerPage, page, getSorting, order, orderBy } = props;
  let calls = []
  if (props.data)  {
    calls = props.data.map((item) => ({
      startTime: item.startTime,
      time: new Date(item.startTime),
      interpreter: item.interpreters.map((interp) => interp.name).join(','),
      interpreters: item.interpreters,
      duration: parseFloat(item.duration),
      durationMins: item.duration,
    }));
  }
  return (
    calls.sort(getSorting(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map( (data, index) => {
      return (<TableRow key={index}>
        <TableCell>{moment(new Date(data.startTime)).format('MM/DD/YY, h:mm a')}</TableCell>
        <TableCell>{data.interpreters.map((interpreter, index) => {
            return <Link key={index} to={`/profile/${interpreter.id}`}>{interpreter.name},</Link>})}
        </TableCell>
        <TableCell>{data.durationMins}</TableCell>
      </TableRow>)
    })
  )
}

class TableComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      rowsPerPage: 5,
      orderBy: '',
      order: 'desc'
    }
  }

  componentDidMount() {
    if (this.props.currentUser && this.props.currentUser.userType === 'deaf') {
      if (this.props.type === "scheduleList") this.props.dispatch(actions.scheduleListDeaf());
      if (this.props.type === "callHistory") this.props.dispatch(actions.deafGetCallHistory());
    } else if (this.props.currentUser && this.props.currentUser.userType === 'interpreter') {
      if (this.props.type === "scheduleList") this.props.dispatch(actions.scheduleListInterpreter());
    }
    this.getDefaultOrderBy();
  }

  getDefaultOrderBy = () => {
    if (this.props.cells && this.props.cells.length) {
      this.setState({orderBy: this.props.cells[0].id});
    }
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  getCountByType = type => {
    if (this.props[type] && this.props[type].schedules && type === "scheduleList") {
      return this.props[type].schedules.length
    } else if (this.props[type] && this.props[type].calls && type === "callHistory") {
      return this.props[type].calls.length
    }
    return 0;
  }

  createSortHandler = property => event => {
    this.handleRequestSort(event, property);
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  desc = (a, b, orderBy) => {
    return b[orderBy] < a[orderBy] ? -1 : b[orderBy] > a[orderBy] ? 1 : 0;
  };

  getSorting = (order, orderBy) => {
    return order === 'desc' ? (a, b) => this.desc(a, b, orderBy) : (a, b) => -this.desc(a, b, orderBy);
  };

  render () {
    const { classes, cells, type, scheduleList = null, callHistory = null, currentUser } = this.props;
    const {rowsPerPage, orderBy, order, page} = this.state;

    return (
      <React.Fragment>
        {type === "scheduleList" &&
          <div>
            <h4>Scheduled Calls</h4>
            {this.getCountByType("scheduleList") === 0 && currentUser.userType === 'deaf' && <p>Here you will see all your scheduled calls, <Link to="/schedule">Click here</Link> to create your first call</p>}
          </div>
        }
        {type === "callHistory"  &&
          <div>
            <h6>Call History</h6>
            {this.getCountByType("callHistory") === 0 && <p>Here you will see your calls history</p>}
          </div>
        }
        <Paper className={classes.root}>
          <Table>
            <TableHead>
             <TableRow>
               {cells.map( (cell, index) => {
                return (
                  <TableCell
                    key={index}
                    sortDirection={orderBy === cell.id ? order : false}
                  >

                      <TableSortLabel
                        active={orderBy === cell.id}
                        direction={order}
                        onClick={this.createSortHandler(cell.id)}
                      >
                        {cell.label}
                      </TableSortLabel>
                  </TableCell>
                );
                  //return <TableCell key={index}>{name}</TableCell>
                })}
             </TableRow>
           </TableHead>
           <TableBody>
             {type === 'scheduleList' && scheduleList ?  <ScheduleList getSorting={this.getSorting} orderBy={orderBy} order={order} data={scheduleList.schedules} rowsPerPage={rowsPerPage} page={page}/> : null}
             {type === 'callHistory' && callHistory ? <CallHistory getSorting={this.getSorting} orderBy={orderBy} order={order} data={callHistory.calls} rowsPerPage={rowsPerPage} page={page}/> : null }
           </TableBody>
           <TableFooter>
              <TableRow>
                <TablePagination
                  colSpan={3}
                  count={this.getCountByType(type)}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActionsWrapped}
                />
              </TableRow>
          </TableFooter>
         </Table>
       </Paper>
     </React.Fragment>
    )
  }
}

TableComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(TableComponent);
