import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { compose } from 'recompose';

const styles = theme => ({
  text: {
    lineHeight: '1.5'
  }
})

class MessageDialog extends React.Component {
  state = {
    open: this.props.open,
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render () {
    const { classes, fullScreen } = this.props;
    return (
      <Dialog open={this.state.open} onClose={this.handleClose} fullScreen={fullScreen}>
         <DialogContent>
           <h2>Thanks for signing up!<br/>Welcome to our testing version</h2>
           <h4 className={classes.text}>We will be launching interp.me on the 15th of September. <br/>in the meantime please share it with your friends and stay updated via our <a href="https://www.facebook.com/interp.mee/">Facebook page</a></h4>
         </DialogContent>
         <DialogActions>
           <Button variant="contained" onClick={this.handleClose} color="primary">
             Continue
           </Button>
         </DialogActions>
       </Dialog>
    )
  }
}

MessageDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  fullScreen: PropTypes.bool.isRequired,
};

export default compose(withMobileDialog(), withStyles(styles))(MessageDialog);
