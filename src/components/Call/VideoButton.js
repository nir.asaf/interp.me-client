import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import VideocamIcon from '@material-ui/icons/Videocam';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    outline: 'none',
    width: '40px',
    height: '40px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class VideoButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      videoStatus: true,
    }
  }

  handleClick = () => {
    const {videoStatus} = this.state;
    this.setState(prevState => ({
      videoStatus: !prevState.videoStatus
    }))
    this.state.activeRoom.localParticipant.videoTracks.forEach(function (videoTrack) {
       if (videoStatus) {
         videoTrack.disable();
       }else {
         videoTrack.enable();
       }
    });
  }

  render () {
    const { classes } = this.props;
    const { videoStatus } = this.state;

    return (
      <Button variant="fab"
              aria-label="end"
              onClick={this.handleClick}
              className={classes.button}>
        {videoStatus ? <VideocamIcon/> : <VideocamOffIcon/>}
      </Button>
    )
  }
}

VideoButton.propTypes = {
  room: PropTypes.object.isRequired,
};

export default withStyles(styles)(VideoButton);
