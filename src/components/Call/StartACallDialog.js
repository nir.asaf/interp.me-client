import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { compose } from 'recompose';
import { Link } from 'react-router-dom';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import helpers from '../../utils/Helpers';
import Grid from '@material-ui/core/Grid';
import actions from '../../actions';
import { withStyles } from '@material-ui/core/styles';
import CallIcon from '@material-ui/icons/Call';
import SpokenLanguageSelect from '../User/SpokenLanguageSelect';
import { connect } from 'react-redux';
import GA from '../../utils/GoogleAnalytics';
import PaymentForm from '../Payment/PaymentForm';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Icon from '@material-ui/core/Icon';
import SelectComponent from '../Common/SelectComponent';
import { sendMixpanelEvent } from '../../utils/MixpanelUtil';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    minHeight: 150,
    zIndex: 9,
  },
  formControl: {
    margin: theme.spacing.unit,
    width: '100%',
  },
  text: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  buttonWrap: {
    margin: '1rem 0px 1rem',
    textAlign: 'center',
    fontSize: '0.8rem',
  },
  pra: {
    paddingTop: '0.5rem',
    marginBottom: '1rem',
  },
  disabled: {
    opacity: 0.1,
    pointerEvents: 'none',
  },
  title: {
    textAlign: 'center',
    backgroundColor: '#fafafa',
    borderBottom: '1px solid #e5e5e5'
  }
});

const LinkTo = props => <Link to={props.to} {...props} />

const mapStateToProps = state => ({
  model: state.common.model,
  browserInfo: state.common.browserInfo,
  inProgress: state.common.inProgress,
  cardsDetails: state.payment.cardsDetails ? state.payment.cardsDetails : null,
  selectedCard: state.payment.selectedCard ? state.payment.selectedCard : null,
  signLanguageList: state.common.config && !state.common.config.signLanguages ? [] : state.common.config.signLanguages,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

class StartACallDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      activeStep: 0,
      spokenLanguage: '',
      gender: '',
      searchText: '',
      signLanguage: props.currentUser.signLanguages[0],
      callType: 'instant_fixed',
      selectedInterpreters: [],
      context: '',
      disabled: false,
      payment: 'card',
    }
  }

  componentDidMount() {
    if (!helpers.twilioBrowserSupport()) {
      this.setState({ disabled: true });
    }
    if (!(this.props.cardsDetails && this.props.cardsDetails.length)) {
      this.props.dispatch(actions.deafGetPaymentIds())
    } else {
      this.props.dispatch(actions.deafSelectPaymentCard(this.props.cardsDetails[0]));
    }
    this.props.dispatch(sendMixpanelEvent('Open Start A Call Dialog'));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cardsDetails && nextProps.cardsDetails.length && !nextProps.selectedCard) {
      this.props.dispatch(actions.deafSelectPaymentCard(nextProps.cardsDetails[0]));
    }
  }

  componentWillUnmount() {
    this.props.dispatch(actions.closeModal())
  }

  handleClose = () => {
    GA.logEvent('Start Call', '"Cancel" button pressed');
    this.props.dispatch(actions.closeModal())
    this.props.dispatch(sendMixpanelEvent('Close Start A Call Dialog'));
  };

  checkCouponCode = () => {
    this.setState({ couponCodeNotValid: true })
  }

  handleSpokenLanguageSelection = (selection) => {
    // send event to GA
    var languageLabel = selection ? selection.label : 'none';
    GA.logEvent('Start Call', 'Spoken language selected', {
      label: languageLabel
    });
    this.setState({ spokenLanguage: selection });
  };

  handleSignLanguageSelection = (selection) => {
    // send event to GA
    var languageLabel = selection ? selection.label : 'none';
    GA.logEvent('Start Call', 'Sign language selected', {
      label: languageLabel
    });
    selection = selection.length === 0 ? "" : selection.value;
    this.setState({ signLanguage: selection });
  };

  createCall = () => {
    this.props.dispatch(sendMixpanelEvent('Press Stary A Call Button in StartACallDialog'));
    const { selectedCard } = this.props;
    const { spokenLanguage, signLanguage, selectedInterpreters, context ,callType} = this.state;
    GA.logEvent('Start Call', '"Start A Call" button pressed');
    // const gender = this.state.gender ? { "type" : "gender", "value" : this.state.gender} : null;
    const data = {
      signLanguage: signLanguage,
      "interpreterMatch": {
        spokenLanguage: spokenLanguage,
        list: selectedInterpreters.length ? selectedInterpreters : [],
      },
      context: context,
      callType: callType,
      paymentType: 'user_payment_id'
      //paymentType: payment
    }

    if (selectedCard) {
      data.paymentId = selectedCard.id;
    }
    this.props.dispatch(actions.createCall(data))
  }

  handlePaymentType = (payment) => {
    this.setState({ payment: payment })
  }

  validateStartCall = () => {
    let { selectedCard } = this.props;
    let { spokenLanguage, signLanguage, callType } = this.state;

    if (spokenLanguage === "") return false;

    if (signLanguage === "") return false;

    if (!selectedCard && callType !== 'instant_free') return false;

    return true;
  }

  handleNext = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep + 1,
    });
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep - 1,
    });
  };

  getStepContent(stepIndex) {
    const { classes, selectedCard, signLanguageList, currentUser } = this.props;
    const { callType, context, payment, signLanguage, spokenLanguage } = this.state;

    const signLanguagesFilterList = [];
    currentUser.signLanguages.forEach(signLang => {
      let language = signLanguageList.find((lang) => lang.id === signLang);
      if (language) {
        signLanguagesFilterList.push({ label: language.name, value: language.id });
      }
      return [];
    });

    switch (stepIndex) {
        case 0:
          return (
            <React.Fragment>

              <Grid container justify="center" className={classes.pra}>
                <Grid item xs={12} sm={7} style={{ zIndex: 10 }}>
                  <SelectComponent icon={<Icon style={{ fontSize: 24 }} className={'fa fa-sign-language'}/>} label="Sign Language" suggestions={signLanguagesFilterList} onChange={this.handleSignLanguageSelection} value={signLanguage} selectType="single"/>
                </Grid>
              </Grid>

              <Grid container justify="center">
                <Grid item xs={12} sm={7} style={{ zIndex: 9 }}>
                  <SpokenLanguageSelect value={spokenLanguage} label="Spoken Language" placeholder="Select Spoken Language" type="single" onChange={this.handleSpokenLanguageSelection} />
                </Grid>
              </Grid>
,
              <Grid container justify="center" spacing={8} className={classes.buttonWrap}>
                <Grid item xs={12}><h4>Select the type of the call</h4></Grid>
                <Grid item>
                  <Button variant="contained" disabled={true} className={classes.button} onClick={() => this.setState({ callType: 'instant_free' })}>Volunteer</Button>
                  <Typography color={callType === 'instant_free' ? 'primary' : 'default'} variant="caption" gutterBottom align="center">(Free - Coming Soon)</Typography>
                </Grid>
                <Grid item>
                  <Button variant="contained" className={classes.button} >Certified</Button>
                  <Typography color={callType === 'instant_fixed' ? 'primary' : 'default'} variant="caption" gutterBottom align="center">($1 a minute)</Typography>
                </Grid>
              </Grid>

              <Grid container justify="center">
                <Grid item xs={12} sm={7}>
                  <TextField placeholder="What is the context?" fullWidth value={context} multiline rows="3" onChange={(event) => { if (event.target.value.length <= 100) this.setState({ context: event.target.value }) }} />
                  <FormHelperText style={{ textAlign: 'right' }}>{context.length}/100 letters</FormHelperText>
                </Grid>
              </Grid>
            </React.Fragment>
          )
        case 1:
          return (
            <PaymentForm payment={payment} selectedCard={selectedCard} handlePaymentType={this.handlePaymentType} />
          )
        default:
        return;
    }
  }

  render() {
    const { disabled, activeStep, spokenLanguage, signLanguage, callType } = this.state;
    const { fullScreen, model, classes } = this.props;
    return (
      <Dialog open={model === 'StartACallDialog'} fullScreen={fullScreen} fullWidth onClose={this.handleClose}>
        <DialogTitle className={classes.title}>
          Start a new Call
        </DialogTitle>
        {!disabled ?
          <DialogContent style={{ minHeight: '340px' }}>
            <Grid container>
              <Grid item xs={12}>
                <Stepper activeStep={activeStep} alternativeLabel>
                  <Step key="setting"><StepLabel>Call Setting</StepLabel></Step>
                  {callType !== 'instant_free' ? <Step key="payment"><StepLabel>Payment</StepLabel></Step> : null }
                </Stepper>
              </Grid>
            </Grid>

            {this.getStepContent(activeStep)}

            <Grid container justify="flex-end" className={classes.buttonWrap}>
              <Grid item xs={12}>
                <Button disabled={activeStep === 0} onClick={this.handleBack} className={classes.button}>Back</Button>
                {activeStep === 1 || callType === 'instant_free' ?
                  <Button size="large" variant="contained" color="primary" disabled={!this.validateStartCall()} onClick={this.createCall}>
                    <CallIcon style={{ paddingRight: 10 }} />
                    Start a Call
                  </Button>
                  :
                  <Button variant="contained" disabled={spokenLanguage && signLanguage ? false : true} color="primary" onClick={this.handleNext} className={classes.button}>Next</Button>
                }
              </Grid>
            </Grid>
            <Divider/>
            <Grid container justify="flex-end">
              <Grid item>
                <Button size="small" onClick={this.handleClose}>Cancel</Button>
                <Button size="small" color="primary" component={LinkTo} to="/schedule">Schedule for later</Button>
              </Grid>
            </Grid>
          </DialogContent>
          :
          <DialogContent style={{ minHeight: '340px', textAlign: 'center' }}>
            <h5>Sorry, at the moment we don't support {this.props.browserInfo.name} {this.props.browserInfo.mobile} for video call</h5>
            <Button size="small" variant="contained" onClick={this.handleClose}>Back to dashboard</Button>
          </DialogContent>
        }
      </Dialog>

    );
  }
}

StartACallDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withMobileDialog(), withStyles(styles))(StartACallDialog);
