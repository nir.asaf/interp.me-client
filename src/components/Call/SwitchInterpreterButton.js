import React from 'react';
import actions from '../../actions';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Shuffle from '@material-ui/icons/Shuffle';
import purple from '@material-ui/core/colors/purple';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    fontSize: '12px',
    backgroundColor: purple[500],
    '&:hover': {
      backgroundColor: purple[700],
    },
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class SwitchInterpreterButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      disabled: false,
    }
  }

  componentWillUnmount() {
    this.setState({disabled: false})
  }

  clickCallsNewMatch = () => {
    this.setState({disabled: true})
    const date = {
      callId: this.props.callData.id,
      interpreterMatch: this.props.callData.interpreterMatch
    }
    this.props.dispatch(actions.callsNewMatch(date));
  }

  render () {
    const { classes } = this.props;
    return (
      <Button variant="contained"
              color="secondary"
              onClick={this.clickCallsNewMatch}
              disabled={this.state.disabled}
              className={classes.button}>
      Switch
      <Shuffle className={classes.rightIcon} />
    </Button>
    )
  }
}

SwitchInterpreterButton.propTypes = {
  classes: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(SwitchInterpreterButton);
