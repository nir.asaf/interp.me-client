// "sessions" - map of id (of the session) to {<br/>
// "id" - id of the session<br/>
// "startTime" - server-time when the session got started<br/>
// "interpreterId" - id of the interpreter used in this session<br/>
// "finishTime" - server-time when the session got finished (null if still active)<br/>
// "durationMins" - total amount of minutes the call took (null if still active)<br/>

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import UserRating from '../Common/UserRating';
import TextField from '@material-ui/core/TextField';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

import moment from 'moment';
import { IsEmptyObject } from '../../utils/Helpers';
import actions from '../../actions';

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(14),
    flexBasis: '33%',
    flexShrink: 0,
  },
  card: {
    border: '1px solid',
    marginBottom: '10px'
  },
  rateText: {
    fontSize: theme.typography.pxToRem(14),
    color: '#2a96f3',
  },
  sessions: {
    marginTop: '1.5rem',
  },
  rateTextRotate: {
    transform: 'rotate(180deg)',
  },
  timeLable: {
    fontSize: '12px',
    textAlign: 'right'
  },
  title: {
    marginLeft: '30px',
    background: 'white',
    position: 'absolute',
    top: '7px',
    textAlign: 'CENTER',
    padding: '5px'
  },
  starRating: { letterSpacing: '16px', lineHeight: '0.5' }
})

class EndOfCallDeafDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: null,
      numberOfSessions: 0,
      reviews: [],
      checks: [],
      rateRotate: true,
      rateNumber: 3,
      reviewRateNumber: 3,
      text: '',
      isSubmittedFeedBack: false,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleFeedback = this.handleFeedback.bind(this);
    this.handleReview = this.handleReview.bind(this);
    this.submitReview = this.submitReview.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.callData && !IsEmptyObject(nextProps.callData.sessions)) {
      const reviews = [];
      var numberOfSessions = 0;
      for (var i = 0; i < Object.keys(nextProps.callData.sessions).length; i++) {
        var sessionKey = Object.keys(nextProps.callData.sessions)[i]
        var session = nextProps.callData.sessions[sessionKey];
        let obj = { targetUserId: session.interpreter.id, ratingStars: 3, reviewText: '' };
        reviews[i] = obj;
        numberOfSessions = numberOfSessions + 1;
      }
      this.setState({ reviews: reviews, numberOfSessions: numberOfSessions });
    }
  }

  submitReview(index) {
    let newReviews = Object.assign([], this.state.reviews);
    if (!this.state.reviews[index].ratingStars) {
      newReviews[index]['ratingError'] = true;
      newReviews[index]['reviewErrorText'] = "Please choose rating.";
    } else if (!this.state.reviews[index].reviewText) {
      newReviews[index]['ratingError'] = true;
      newReviews[index]['reviewErrorText'] = "Please add review text.";
    } else {
      this.props.dispatch(actions.reviewsCreate(this.state.reviews[index]));
      newReviews[index]['close'] = true;
    }
    this.setState({ reviews: newReviews });
  };

  handleFeedback() {
    var data = {
      subject: 'Call quality',
      text: this.state.text ? this.state.text : 'Call quality',
      context: {
        callId: this.props.callData ? this.props.callData.id : '',
        qualityRate: this.state.rateNumber.toString()
      }
    }
    this.props.dispatch(actions.configSendFeedback(data));
    this.setState({ isSubmittedFeedBack: true })
  };

  handleClose() {
    this.resetCall();
    this.props.dispatch(actions.redirectTo('/dashboard'));
  };

  resetCall = () => {
    this.props.dispatch(actions.resetTimer());
    this.props.dispatch(actions.callDataReset());
  }

  handleChangePanel = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
      rateRotate: expanded ? false : true,
    });
  };

  handleReview(name, value, index) {
    let newReviews = Object.assign([], this.state.reviews);
    newReviews[index]['ratingError'] = false;
    newReviews[index]['reviewErrorText'] = "";
    newReviews[index][name] = value;
    this.setState({ reviews: newReviews });
  }

  onRate = (rating) => {
    this.setState({ rateNumber: rating })
  }

  onTextChange = (text) => {
    this.setState({ text: text })
  }

  getTimeDifference(session) {
    if (!session.finishTime)
      return 0;

    const duration = moment.duration(moment(session.finishTime).diff(moment(session.startTime)));
    return parseInt(duration.asMinutes(), 10);
  }

  render() {
    const { isSubmittedFeedBack, text, reviews } = this.state;
    const { open, callData, classes, fullScreen } = this.props;

    console.log(callData.sessions)
    return (
      <Dialog disableBackdropClick disableEscapeKeyDown title="End Call" fullScreen={fullScreen} fullWidth open={open} onClose={this.handleClose}>
        <DialogContent>
          {IsEmptyObject(callData.sessions) ?
            <h5>There wasn't active sessions</h5>
            :
            <div>
              <div className={classes.sessions}>
                <h3>How was your experience</h3>
                {Object.keys(callData.sessions).map((sessionId, index) => {
                  const session = callData.sessions[sessionId];
                  const review = reviews[index];
                  console.log(review);
                  return (<div key={session.id} >
                    {!(review && review.close) ?
                      <Card className={classes.card} style={{ paddingBottom: 10 }}>
                        <Grid container spacing={24}>
                          <Grid item xs={6}>
                            <h4 style={{ padding: '5px' }}>{session.interpreter.name}</h4>
                          </Grid>
                          <Grid item xs={6} style={{ textAlign: 'end' }}>
                            <h4 style={{ padding: '5px' }}>{this.getTimeDifference(session) + ' minute(s)'}</h4>
                          </Grid>
                        </Grid>
                        {(review && review.ratingError) ?
                          <Grid container spacing={24}>
                            <Grid item xs={3}></Grid>
                            <Grid item xs={9}>
                              <h4 style={{ padding: '5px', color: 'red' }}>{review.reviewErrorText}</h4>
                            </Grid>
                          </Grid>
                          : null
                        }
                        <CardHeader className={classes.timeLable}>
                        </CardHeader>
                        <CardContent style={{ paddingTop: '0', paddingBottom: 0 }}>
                          <Grid container spacing={16}>
                            <Grid item xs={3}>
                              Rate:
                            </Grid>
                            <Grid item xs={9}>
                              <div id="starRating" className={classes.starRating} ><UserRating value={this.state.reviewRateNumber} starsSize="30" editing={true}
                                onStarsRate={(val) => this.handleReview('ratingStars', val, index)} /></div>
                            </Grid>
                          </Grid>
                          <Grid container spacing={16}>
                            <Grid item xs={3}>
                              Review:
                            </Grid>
                            <Grid item xs={9}>
                              <TextField onChange={(event) => {
                                this.handleReview('reviewText', event.target.value, index);
                                let newChecks = Object.assign([], this.state.checks);
                                newChecks[index] = newChecks[index] ? true : true;
                                this.setState({ checks: newChecks });
                              }} rows="3" label="Add a general feedback" multiline fullWidth margin="normal" style={{ marginTop: 0 }} />
                            </Grid>
                          </Grid>
                        </CardContent>
                        <CardActions style={{ justifyContent: "flex-end" }}>
                          <Button variant="contained" color="primary" onClick={() => { this.submitReview(index) }}>Submit</Button>
                        </CardActions>
                      </Card>
                      : null
                    }
                  </div>)
                })}
                {!isSubmittedFeedBack ?
                  <Card className={classes.card}>
                    <Grid container spacing={16}>
                      <Grid item xs={9}>
                        <h4 style={{ padding: '5px' }}>FeedBack</h4>
                      </Grid>
                    </Grid>

                    <CardHeader style={{ textAlign: "right", fontSize: '14px', marginTop: 10 }} className={classes.timeLable}>
                    </CardHeader>
                    <CardContent style={{ paddingTop: '0', paddingBottom: 0 }}>
                      <Grid container spacing={16}>
                        <Grid item xs={3}>
                          Call Quality:
                      </Grid>
                        <Grid item xs={9}>
                          <div id="starRating" className={classes.starRating}><UserRating value={this.state.rateNumber} starsSize="30" editing={true}
                            onStarsRate={(val) => this.onRate(val)} /></div>
                        </Grid>
                      </Grid>
                      <Grid container spacing={16}>
                        <Grid item xs={3}>
                          FeedBack:
                      </Grid>
                        <Grid item xs={9}>
                          <TextField fullWidth value={text} variant="outlined" id="outlined-bare" margin="normal" multiline rows="2" onChange={event => { if (event.target.value.length <= 200) this.onTextChange(event.target.value); }} />
                        </Grid>
                      </Grid>
                    </CardContent>
                    <CardActions style={{ justifyContent: "flex-end" }}>
                      <Button variant="contained" color="primary" onClick={this.handleFeedback}>Submit</Button>
                    </CardActions>
                  </Card>
                  : null
                }
              </div>
            </div>
          }
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={this.handleClose}>Go back to dashboard</Button>

        </DialogActions>

      </Dialog>
    )
    // return (
    //   <Dialog disableBackdropClick disableEscapeKeyDown title="End Call" fullScreen open={open} onClose={this.handleClose}>
    //     <DialogContent>
    //       { IsEmptyObject(callData.sessions) ?
    //         <h5>There wasn't active sessions</h5>
    //         :
    //         <div>
    //         <h5>We hope you have a great call and find our service helpful</h5>
    //         { callData.totalBilledMinutes ? <h6>The total minutes you will be billed for is: {callData.totalBilledMinutes} Minutes</h6> : null }
    //         <div className={classes.sessions}>
    //           <h6>Please rate the active sessions</h6>

    //           { Object.keys(callData.sessions).map((sessionId, index) => {
    //               const session = callData.sessions[sessionId];
    //               return (<ExpansionPanel key={session.id} expanded={expanded === session.id} onChange={this.handleChangePanel(session.id)}>
    //                 <ExpansionPanelSummary expandIcon={<div style={{display: 'inline-flex'}}><span className={classNames(classes.rateText, (!this.state.rateRotate) ? classes.rateTextRotate : '')}>Rate</span><ExpandMoreIcon /></div>}>
    //                   <Grid container>
    //                     <Grid item xs={12} sm={6}>
    //                       <Typography className={classes.heading}>Start time: {moment(session.startTime).format('MMMM Do YYYY, h:mm a')}</Typography>
    //                     </Grid>
    //                     <Grid item xs={12} sm={6}>
    //                       <Typography className={classes.heading}>Interpreter Name: {session.interpreter.name}</Typography>
    //                     </Grid>
    //                   </Grid>
    //                 </ExpansionPanelSummary>
    //                 <ExpansionPanelDetails>
    //                   <Grid container>
    //                     <Grid item xs={12} sm={12}>
    //                       <TextField onChange={(event)=> {
    //                           this.handleReview('reviewText', event.target.value, index);
    //                           let newChecks = Object.assign([], this.state.checks);
    //                           newChecks[index] = newChecks[index] ? true : true;
    //                           this.setState({checks: newChecks});
    //                         }} rows="4" label="Write a review about the interpreter" multiline fullWidth margin="normal" style={{marginTop: 0}}/>
    //                     </Grid>
    //                     <Grid item xs={12} sm={12}>
    //                       <div style={{letterSpacing: '16px'}}><UserRating value={this.state.rateNumber} starsSize="30" editing={true}
    //                         onStarsRate={(val)=> this.handleReview('ratingStars', val, index)}/></div>
    //                     </Grid>
    //                   </Grid>
    //                 </ExpansionPanelDetails>
    //                 {/*<ExpansionPanelActions>
    //                   <Button size="small" color="primary" onClick={this.reviewsCreate()}>Rate {session.interpreter.name}</Button>
    //                 </ExpansionPanelActions>*/}
    //               </ExpansionPanel>)
    //             })
    //           }
    //         </div>
    //       </div>
    //       }
    //     </DialogContent>
    //     <DialogActions>
    //       <Button variant="contained" color="primary"
    //         disabled={!(this.state.numberOfSessions === this.state.checks.length)}
    //         onClick={this.handleClose}>{IsEmptyObject(callData.sessions) ? 'Back to dashboard' : 'Rate'}</Button>
    //     </DialogActions>
    //   </Dialog>
    // )
  }
}

EndOfCallDeafDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  callData: PropTypes.object.isRequired,
};

export default compose(withMobileDialog(), withStyles(styles))(EndOfCallDeafDialog);
