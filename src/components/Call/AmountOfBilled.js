import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    position: 'absolute',
    right: '4px',
    top: '30px',
    color: '#fff',
  }
});

const mapStateToProps = state => ({
  startedAt: state.call.startedAt,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class AmountOfBilled extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0,
      amount: "0",
    }
  }

  componentWillUnmount() {

  }

  getAmount(time) {
    var hours = Math.floor(time / 3600000);
    var minutes = Math.floor((time - (hours * 3600000)) / 60000);
    return hours * 60 + minutes;
  }

  render () {
    const { classes } = this.props;
    const amount = this.getAmount(this.props.time)
    return (
      <div className={classes.root}>${amount}</div>
    )
  }
}

AmountOfBilled.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(AmountOfBilled);
