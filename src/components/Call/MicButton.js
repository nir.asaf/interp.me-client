import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MicIcon from '@material-ui/icons/Mic';
import MicOffIcon from '@material-ui/icons/MicOff';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    outline: 'none',
    width: '40px',
    height: '40px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class MicButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      micStatus: true,
    }
  }

  handleClick = () => {
    const {micStatus} = this.state;
    this.setState(prevState => ({
      micStatus: !prevState.micStatus
    }))
    this.state.activeRoom.localParticipant.audioTracks.forEach(function (audioTrack) {
       if (micStatus) {
         audioTrack.disable();
       }else {
         audioTrack.enable();
       }
    });
  }

  render () {
    const { classes } = this.props;
    const { micStatus } = this.state;

    return (
      <Button variant="fab"
              aria-label="end"
              onClick={this.handleClick}
              className={classes.button}>
        {micStatus ? <MicIcon/> : <MicOffIcon/>}
      </Button>
    )
  }
}

MicButton.propTypes = {
  room: PropTypes.object.isRequired,
};

export default withStyles(styles)(MicButton);
