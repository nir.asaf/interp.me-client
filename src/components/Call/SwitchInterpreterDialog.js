import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import { Input } from 'mdbreact';


export default class SwitchInterpreterDialog extends React.Component {

  handleOpen = () => {
    this.setState({open: true});
  };

  render() {
    const actions = [
      <Button
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.props.close}
      />,
    ];

    return (
      <div>
        <Dialog
          title="Reason for switching"
          actions={actions}
          modal={false}
          open={this.props.open}
          onClose={this.props.close}
        >
          <div className="col-6">
            <Input type="textarea" label="Your Reason for switching" icon="pencil"/>
          </div>
        </Dialog>
      </div>
    );
  }
}
