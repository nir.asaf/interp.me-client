import React from 'react';
import VideoComponent from '../Video/VideoComponent';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import actions from '../../actions';
import EndOfcallInterpreterDialog from '../Call/EndOfcallInterpreterDialog';

const mapStateToProps = state => ({
  callData: state.call.data,
  model: state.common.model,
  callState: state.call.callState,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const LinkTo = props => <Link to={props.to} {...props} />;

class CallViewInterpreter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false,
      readyToStartaCAll: false,
      joinRoom: false,
      unmountVideo: false,
      callFinish: false,
      openReviewDialog: false,
    };
  }

  componentDidMount() {
    const activeInterpreter = this.props.callData && this.props.callData.activeInterpreter && this.props.callData.activeInterpreter.uid === this.props.currentUser.uid
    activeInterpreter ? this.setState({readyToStartaCAll: true, openDialog: false}) : this.handleOpen();
  }

  componentWillUnmount() {
      this.setState({openDialog: true, readyToStartaCAll: false});
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.currentUser.twillioToken !== this.props.currentUser.twillioToken) {
      this.setState({unmountVideo: false});
    }
    if (nextProps.callData.state === 'finished') {
      this.setState({callFinish: true});
    }
  }

  unmountVideo = () => {
    this.setState({unmountVideo: true});
    this.props.dispatch(actions.usersGetCurrent());
  }

  handleOpen = () => {
    this.setState({openDialog: true});
  };

  handleJoinRoom = () => {
    this.setState({joinRoom: true});
  }

  // Start a call trigger calls Connect Interpreter to connect the interpreter to the call
  startCall = () => {
    this.setState({readyToStartaCAll: true, openDialog: false});
  }

  openReviewDialog = () => {
    this.setState({openReviewDialog: true});
  }

  // Show a Dialog for interpreter before start the call
  BeforeStartCallDialog() {
      return (
        <Dialog fullScreen open={this.state.openDialog} onClose={this.handleClose}>
          <DialogContent>
            <h5>Before you start your interpretation session - please make sure:</h5>
            <List>
              <ListItem>You have a strong and reliable internet connection</ListItem>
              <ListItem>You are connected via a device with a high-quality camera</ListItem>
              <ListItem>You are in a quiet environment without distractions (e.g. barking dogs, crying babies).</ListItem>
              <ListItem>Wear a headset to have the best audio quality.</ListItem>
              <ListItem>You have a clean background behind you.</ListItem>
            </List>
          </DialogContent>
          <DialogActions>
            <Button size="small" variant="contained" onClick={this.handleClose}>No, I'll reconnect once I'm in a proper environment</Button>
            <Button size="small" variant="contained" color="primary" onClick={this.startCall}>Yes, connect to call</Button>
          </DialogActions>
        </Dialog>
      )
  }


  render() {
    const {currentUser, callData=null} = this.props;
    const {joinRoom, readyToStartaCAll, unmountVideo, callFinish, openReviewDialog} = this.state;
    let activeInterpreter = callData && callData.activeInterpreter && callData.activeInterpreter.uid === currentUser.uid;

    return (
      <div className="interpreter-call-page">
        { (!callData.activeInterpreter || activeInterpreter) && (callData.state !== "finished") ?
            <div>
              {this.BeforeStartCallDialog()}
              {readyToStartaCAll && !unmountVideo && (
                <div>
                  {!joinRoom && <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', color: '#fff', marginTop: '6rem'}}><CircularProgress /><h1>Joining</h1></div>}
                  {joinRoom && unmountVideo && <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', color: '#fff', marginTop: '6rem'}}><CircularProgress /><h1>Loading configuration ...</h1></div>}
                  <VideoComponent unmountVideo={this.unmountVideo} currentUser={currentUser} callData={callData} interpreterJoinRoom={this.handleJoinRoom} openReviewDialog={this.openReviewDialog}/>
                </div>
              )}
            </div>
          :
          !activeInterpreter && callData.state !== "finished" ?
            <div className="callCenterElements">
              <h2 style={{color: '#fff'}}>This call is already picked up by another interpreter.</h2>
              <Button component={LinkTo} variant="contained" to="/dashboard">Go To Dashboard</Button>
            </div>
          :
            <div className="callCenterElements">
              { !callFinish && (
                <div>
                  <h2 className="mb-1" style={{color: '#fff'}}>The call you are trying to access have already finished by the caller.</h2>
                  <Button component={LinkTo} variant="contained" to="/dashboard">Go To Dashboard</Button>
                </div>)}
            </div>
        }
        {currentUser.userType === 'interpreter' && callData ? <EndOfcallInterpreterDialog  currentUser={currentUser} callData={callData} open={openReviewDialog} close={(action) => {this.setState({openReviewDialog: false})}} dispatch={this.props.dispatch} /> : null}
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CallViewInterpreter);
