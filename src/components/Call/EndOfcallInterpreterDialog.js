import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import TextField from '@material-ui/core/TextField';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import CurrentBalance from '../User/CurrentBalance';
import AttachMoney from '@material-ui/icons/AttachMoney';
import Update from '@material-ui/icons/Update';

import actions from '../../actions';
import UserRating from '../Common/UserRating';

const styles = theme => ({
  timeLable: {
    fontSize: '12px',
    textAlign: 'right'
  },
  title: {
    marginLeft: '30px',
    background: 'white',
    position: 'absolute',
    top: '7px',
    textAlign: 'CENTER',
    padding: '5px'
  },
  card: {
    border: '1px solid',
    marginBottom: '10px'
  },
  iconMoney: {
    position: 'relative',
    bottom: '-8px'
  },
  starRating: { letterSpacing: '16px', lineHeight: '0.5' }
})

class EndOfcallInterpreterDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rateNumber: 3,
      text: '',
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleCloseAndSubmit = this.handleCloseAndSubmit.bind(this);
  }

  handleCloseAndSubmit() {
    var data = {
      subject: 'Call quality',
      text: this.state.text ? this.state.text : 'Call quality',
      context: {
        callId: this.props.callData ? this.props.callData.id : '',
        qualityRate: this.state.rateNumber.toString()
      }
    }
    this.props.dispatch(actions.configSendFeedback(data));
    this.resetCall();
    this.props.dispatch(actions.redirectTo('/dashboard'));
  };

  handleClose() {
    this.resetCall();
    this.props.dispatch(actions.redirectTo('/dashboard'));
  };

  resetCall = () => {
    this.props.dispatch(actions.resetTimer());
    this.props.dispatch(actions.callDataReset());
  }

  onRate = (rating) => {
    this.setState({ rateNumber: rating })
  }

  onTextChange = (text) => {
    this.setState({ text: text })
  }

  componentDidMount() {
    if (this.props.open) this.props.dispatch(actions.callsGet({ id: this.props.callData.id }));
  }

  render() {
    const { classes, open, callData, currentUser, fullScreen } = this.props;
    const { text } = this.state;

    return (
      <div>
        <Dialog disableBackdropClick disableEscapeKeyDown title="End Call" fullScreen={fullScreen} fullWidth open={open}>
          <DialogContent>
            <h3>End of Call</h3>
            {callData && callData.totalBilledMinutes ?
              <Card className={classes.card}>

                <Grid container spacing={16}>
                  <Grid item xs={9}>
                    <h4 style={{ padding: '5px' }}>Summary</h4>
                  </Grid>
                </Grid>

                {/* <CardHeader className={classes.timeLable}>
                </CardHeader> */}
                <CardContent style={{ paddingTop: '0', paddingBottom: 0 }}>

                  <Grid container spacing={16}>
                    <Grid item xs={12}>
                      <AttachMoney className={classes.iconMoney} /> <span>You earned {callData.totalBilledMinutes}$ in this call</span>
                      <br></br>
                      <span style={{ marginLeft: '28px' }}>Your new balance is now <CurrentBalance currentUser={currentUser} />$.</span>
                    </Grid>
                  </Grid>
                  {/* <Grid container spacing={16}>
                    <Grid item xs={12}>
                      Your new balance is now <CurrentBalance currentUser={currentUser} />$.
                    </Grid>
                  </Grid> */}
                  <Grid container spacing={16} style={{ marginBottom: '5px' }}>
                    <Grid item xs={12} style={{ display: 'inline-flex' }}>
                      <Update />  {callData.totalBilledMinutes ? <h6 style={{ marginLeft: '5px' }}>The call duration was {callData.totalBilledMinutes} minutes.</h6> : null}
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              : null
            }

            <Card className={classes.card}>

              <Grid container spacing={16}>
                <Grid item xs={9}>
                  <h4 style={{ padding: '5px' }}>FeedBack</h4>
                </Grid>
              </Grid>

              <CardHeader className={classes.timeLable}>
              </CardHeader>
              <CardContent style={{ paddingTop: '0', paddingBottom: 0 }}>
                <Grid container spacing={16}>
                  <Grid item xs={3}>
                    Call Quality:
                  </Grid>
                  <Grid item xs={9}>

                    <div id="starRating" className={classes.starRating}><UserRating value={this.state.rateNumber} starsSize="30" editing={true}
                      onStarsRate={(val) => this.onRate(val)} /></div>
                  </Grid>
                </Grid>
                <Grid container spacing={16}>
                  <Grid item xs={3}>
                    FeedBack:
                  </Grid>
                  <Grid item xs={9}>
                    <TextField fullWidth value={text} variant="outlined" id="outlined-bare" multiline rows="2" margin="normal" onChange={event => { if (event.target.value.length <= 200) this.onTextChange(event.target.value); }} />
                  </Grid>
                </Grid>
              </CardContent>
              <CardActions style={{ justifyContent: "flex-end" }}>
                <Button variant="contained" color="primary" onClick={this.handleCloseAndSubmit}>Submit</Button>
              </CardActions>
            </Card>
          </DialogContent>
          <DialogActions>
            <Button size="small" color="primary" onClick={this.handleClose}>Close</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
    // return (
    //   <div>
    //     <Dialog disableBackdropClick disableEscapeKeyDown title="End Call" fullWidth={true} open={open}>
    //       <DialogContent>
    //         <h5>Please select call quality:</h5>
    //         <div style={{fontSize: '2rem'}}><StarRatingComponent name="quality-rate" value={this.state.rateNumber} onStarClick={(nextValue, prevValue, name) => this.onRate(nextValue)}/></div>
    //       </DialogContent>
    //       <DialogActions>
    //         <Button variant="contained" color="primary" onClick={this.handleClose}>Rate</Button>
    //       </DialogActions>
    //     </Dialog>
    //   </div>
    // );
  }
}

EndOfcallInterpreterDialog.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(withMobileDialog(), withStyles(styles))(EndOfcallInterpreterDialog);
