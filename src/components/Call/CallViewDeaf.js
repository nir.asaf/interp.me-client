import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import VideoComponent from '../Video/VideoComponent';
import {Link} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import actions from '../../actions';
import EndOfCallDeafDialog from '../Call/EndOfCallDeafDialog';

const styles = theme => ({
  searchingForInterpreters: {
    overflow: 'hidden',
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  callData: state.call.data,
  callState: state.call.callState,
})

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const LinkTo = props => <Link to={props.to} {...props} />;

class CallViewDeaf extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openSwitchInterpreterDialog: false,
      interpreterJoin: false,
      unmountVideo: false,
      callFinish: false,
      openDialog: false,
    };
  }

  unmountVideo = () => {
    this.setState({unmountVideo: true});
    this.props.dispatch(actions.usersGetCurrent());
  }

  componentDidMount() {}

  componentWillUnmount() {
    this.setState({interpreterJoin: false, openSwitchInterpreterDialog: false});
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.currentUser.twillioToken !== this.props.currentUser.twillioToken) {
      this.setState({unmountVideo: false});
    }
    if (nextProps.callData.state === 'finished') {
      this.setState({callFinish: true});
    }
  }

  handleInterpreterJoin = () => {
    this.setState(prevState => ({
      interpreterJoin: !prevState.interpreterJoin
    }));
  }

  openReviewDialog = () => {
    this.setState({openDialog: true});
  }

  render() {
    const {currentUser, callData=null, classes} = this.props;
    const {interpreterJoin, callFinish, unmountVideo, openDialog} = this.state;
    return (
      <div className="deaf-call-page">
        {
          callData.callerId === currentUser.uid ?
            callData.state === "finished" && !callFinish ?
              <div className="callCenterElements">
                <h2 style={{color: '#fff'}}>The call you are trying to access have already finished. Please go back to dashboard and start a new call</h2>
                <Button component={LinkTo}  variant="contained" to="/dashboard">Go To Dashboard</Button>
              </div>
            :
              <div>
                  { /* Check if interpreter Join or not*/
                    (interpreterJoin || callData.activeInterpreter) || (callData.state === 'paused' || callData.state === 'connected' || callData.state === 'started' || callData.state === 'finished') ? null : <div className={classes.searchingForInterpreters}><CircularProgress /><h1 style={{color: '#fff', marginTop: '1.5rem'}}>Searching for available interpreters, hang on…</h1></div>
                  }
                  { !unmountVideo ?
                    <VideoComponent
                      currentUser={currentUser}
                      interpreterJoin={this.handleInterpreterJoin}
                      callData={callData}
                      unmountVideo={this.unmountVideo}
                      openReviewDialog={this.openReviewDialog}/>
                    :
                    <div className="callCenterElements">
                      <h2 style={{color: '#fff'}}>Loading needed data</h2>
                    </div>
                  }
                  {currentUser.userType === 'deaf' && callData ? <EndOfCallDeafDialog callData={callData} open={openDialog} close={(action) => {this.setState({openDialog: false})}} dispatch={this.props.dispatch}/> : null}
              </div>
          :
            <div className="callCenterElements">
              <h2>You are not permitted to participate in this call, please go back to dashboard and start a new call</h2>
              <Button component={LinkTo}  variant="contained" to="/dashboard">Go To Dashboard</Button>
            </div>
         }
      </div>
    )
  }
}

CallViewDeaf.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(CallViewDeaf);
