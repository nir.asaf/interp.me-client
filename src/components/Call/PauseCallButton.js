import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import actions from '../../actions';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import PhonePaused from '@material-ui/icons/PhonePaused';
import green from '@material-ui/core/colors/green';


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    fontSize: '12px',
    backgroundColor: green[500],
    '&:hover': {
     backgroundColor: green[700],
    },
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class PauseCallButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      disabled: false,
    }
  }

  componentWillUnmount() {
    this.setState({disabled: false})
  }

  clickPauseCall = () => {
    this.setState({disabled: true})
    this.props.dispatch(actions.callsPause({callId: this.state.activeRoom.name}));
  }

  render () {
    const { classes } = this.props;

    return (
      <Button variant="contained"
              color="secondary"
              onClick={this.clickPauseCall}
              disabled={this.state.disabled}
              className={classes.button}>Pause
      <PhonePaused className={classes.rightIcon} />
    </Button>
    )
  }
}

PauseCallButton.propTypes = {
  classes: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(PauseCallButton);
