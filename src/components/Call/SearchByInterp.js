{/*
  // import FormControlLabel from '@material-ui/core/FormControlLabel';
  // import SearchIcon from '@material-ui/icons/Search';
  // import AlgoliaIcon from '../../images/search-by-algolia.svg';
  
  listOfInterpreters: !state.call.listOfInterpreters ? [] :
    state.call.listOfInterpreters.map(interpreter => {
      return {
        value: interpreter.objectID,
        label: interpreter.name,
      }
    }),

  const {searchInterpreterBy, selectedInterpreters, searchForInterpreters} = this.state;

  if (nextProps.listOfInterpreters !== this.props.listOfInterpreters) {
    this.setState({ searchForInterpreters: false })
  }

  interpreterSelection = (event) => {
    this.setState({ searchInterpreterBy: event.target.value });
  }

  handleInputInterpreterSearch = (val) => {
    this.setState({ searchForInterpreters: true })
    const data = { query: val }
    this.props.dispatch(actions.interpretersSearch(data));
  }

  <Grid item xs={12}>
    <h5>Interpreter selection</h5>
    <RadioGroup name="searchInterpreterBy" aria-label="search-interpreter-by-name" value={searchInterpreterBy} onChange={this.interpreterSelection}>
      <FormControlLabel value="match" control={<Radio color="primary"/>} label="Find a matching interpreter" />
      <FormControlLabel value="searchForInterpreters" control={<Radio color="primary"/>} label="Search for interpreters by name" />
    </RadioGroup>
  </Grid>

  { searchInterpreterBy === 'searchForInterpreters' ?
    <Grid item xs={12}>
      <h5 className="mt-2">Search for interpreter by name:</h5>
    <Grid item xs={12} sm={8} className={classes.container}>
      <FormControl className={classes.formControl}>
        <SelectComponent
          suggestions={listOfInterpreters}
          placeholder="Interpreter name"
          isLoading={searchForInterpreters}
          selectType={'multi'}
          icon={<SearchIcon/>}
          onKeyPress={this.handleInputInterpreterSearch}
          withFooter={AlgoliaIcon}
          onChange={(obj) => {
            let newSelectedInterpreters = Object.assign([], selectedInterpreters);
            newSelectedInterpreters = obj.map(interpreter => {return interpreter.value});
            this.setState({selectedInterpreters: newSelectedInterpreters});
          }}

          />
        </FormControl>
      </Grid>
    </Grid>
    :
    null
   }
  {/*
  <h5 className="mt-2 mb-4">Interpreter preferences</h5>
  <RadioButtonGroup name="gender" labelPosition="right" onChange={(ev) => { this.setState({gender: ev.target.value})}} valueSelected={gender}>
    <RadioButton value="any" label="Any" style={{display: 'inline-block', width: '100px'}}/>
    <RadioButton value="f" label="Female Only" style={{display: 'inline-block', width: '150px'}}/>
    <RadioButton value="m" label="Male Only" style={{display: 'inline-block', width: '150px'}}/>
  </RadioButtonGroup>
  */}
