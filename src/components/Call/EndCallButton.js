import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CallEnd from '@material-ui/icons/CallEnd';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class EndCallButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      disabled: false,
    }
  }

  clickEndCall = () => {
    // this.props.room.disconnect();
    this.props.endCallButton();
  }

  render () {
    const { classes } = this.props;
    return (
      <Button variant="fab"
              color="secondary"
              aria-label="end"
              onClick={this.clickEndCall}
              className={classes.button}>
        <CallEnd/>
      </Button>
    )
  }
}

EndCallButton.propTypes = {
  classes: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired,
};

export default withStyles(styles)(EndCallButton);
