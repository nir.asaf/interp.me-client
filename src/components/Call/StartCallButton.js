import React from 'react';
import { connect } from 'react-redux';
import actions from '../../actions';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Phone from '@material-ui/icons/Phone';
import green from '@material-ui/core/colors/green';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    fontSize: '12px',
    backgroundColor: green[500],
    '&:hover': {
     backgroundColor: green[700],
    },
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  dispatch
});

class StartCallButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRoom: props.room,
      disabled: false,
    }
  }

  componentWillUnmount() {
    this.setState({disabled: false})
  }

  clickStartCall = () => {
    this.setState({disabled: true})
    const data = {
       callId: this.props.callData.id,
       interpreterId: this.props.callData.activeInterpreter.uid
    }
    this.props.dispatch(actions.callsStartSession(data));
  }

  render () {
    const { classes, callData } = this.props;

    return (
      <Button onClick={this.clickStartCall}
              variant="contained"
              color="secondary"
              disabled={ this.state.disabled || !callData.activeInterpreter }
              className={classes.button}>
      Start
      <Phone className={classes.rightIcon} />
    </Button>
    )
  }
}

StartCallButton.propTypes = {
  classes: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired,
};

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(StartCallButton);
