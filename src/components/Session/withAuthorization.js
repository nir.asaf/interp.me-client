import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { firebase } from '../../firebase';

const withAuthorization = (condition) => (Component) => {
  class WithAuthorization extends React.Component {
    

    componentWillMount() {
      let pathnameFromUser = this.props.location.pathname
      if (pathnameFromUser === '/login' || pathnameFromUser === '/signup') {
          pathnameFromUser = '/';
      }
      const location = {
        pathname: this.props.currentUser && !this.props.currentUser.registered  ? '/complete_details' : '/login',
        state: { from: {pathname: pathnameFromUser} }
      }

      firebase.auth.onAuthStateChanged(authUser => {
        if(location.pathname !== location.state.from.pathname){
          if (!condition(authUser, this.props.currentUser)) {
            this.props.history.push(location);
          }
        }
      });
    }

    render() {
      return this.props.authUser ? <Component /> : null;
    }
  }

  const mapStateToProps = (state) => ({
    authUser: state.session.authUser,
    currentUser: state.common.currentUser,
  });

  return compose(
    withRouter,
    connect(mapStateToProps),
  )(WithAuthorization);
}

export default withAuthorization;
