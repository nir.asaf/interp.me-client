import auth from './reducers/auth';
import signUp from './reducers/signUp';
import { combineReducers } from 'redux';
import common from './reducers/common';
import home from './reducers/home';
import profile from './reducers/profile';
import settings from './reducers/settings';
import video from './reducers/video';
import session from './reducers/session'
import call from './reducers/call'
import payment from './reducers/payment'
import verification from './reducers/verification'
import config from './reducers/config'
import schedule from './reducers/schedule'
import review from './reducers/review'
import { routerReducer } from 'react-router-redux';
import { snackbarReducer } from 'material-ui-snackbar-redux';

export default combineReducers({
  auth,
  signUp,
  common,
  home,
  profile,
  settings,
  video,
  session,
  call,
  payment,
  verification,
  config,
  schedule,
  review,
  router: routerReducer,
  snackbar: snackbarReducer
});
